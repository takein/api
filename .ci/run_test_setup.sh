#!/usr/bin/env bash

## The TakeInAPI project is within the API directory
cd ~/clone/api && pwd

## Grab the dotenv file from S3 then replace the MySQL creds, replace with Codeship env vars
curl -o ".env" https://s3-ap-southeast-2.amazonaws.com/takein-deployments/public/ci-testing-laravel.env
sed -i.bak "s/\(DB_USERNAME=\).*$/\1${MYSQL_USER}/" .env && rm .env.bak
sed -i.bak "s/\(DB_PASSWORD=\).*$/\1${MYSQL_PASSWORD}/" .env && rm .env.bak

if [ "$CI_BRANCH" = "master" ]; then
    ## Option 1: Install dependencies through Composer INCLUDING dev dependencies
    echo Installing Composer deps from remote server
    composer install --prefer-source --no-interaction
else
    ## Option 2: Install deps by copying down the folder from s3
    echo Installing Composer deps via folder copy
    curl -o "vendor.tar.gz" https://s3-ap-southeast-2.amazonaws.com/takein-deployments/public/lumen-composer-vendor.tar.gz
    tar -xzf "vendor.tar.gz"
fi

php artisan migrate --force
php artisan db:seed


## Start php server to test against
# cd ~/clone/api/public && pwd
# nohup bash -c "php -S 127.0.0.1:8090 2>&1 &" && sleep 1; cat nohup.out


## Another alternative: Build and install an apache instance
## Install httpd into ubuntu locally
cd && pwd
curl -o "httpd-2.4.18.tar.gz" https://s3-ap-southeast-2.amazonaws.com/takein-deployments/public/lamp/httpd-2.4.18.tar.gz
curl -o "httpd-2.4.18.tar.gz.md5" https://s3-ap-southeast-2.amazonaws.com/takein-deployments/public/lamp/httpd-2.4.18.tar.gz.md5
## Integrity check
if [ "$(awk '{print $1;}' httpd-2.4.18.tar.gz.md5)" != "$(md5sum httpd-2.4.18.tar.gz | awk '{print $1;}')" ]; then 
    exit 1
fi

tar -xzf httpd-2.4.18.tar.gz && cd httpd-2.4.18
./configure --prefix=/home/rof/apache
make
make install

## Install mod_php5
cd ~/apache/modules
curl -o "libphp-5.6.so" https://s3-ap-southeast-2.amazonaws.com/takein-deployments/public/lamp/modules/libphp-5.6.so
curl -o "libphp-zts-5.6.so" https://s3-ap-southeast-2.amazonaws.com/takein-deployments/public/lamp/modules/libphp-zts-5.6.so


cd ~
## Apache config modding
## Disable the main port listener which would be set as port 80
sed -i.bak "s/^Listen.*/#\0/" apache/conf/httpd.conf && rm apache/conf/httpd.conf.bak
sed -i.bak "s/^\(Group\).*/\1 rof/g" apache/conf/httpd.conf && rm apache/conf/httpd.conf.bak
sed -i.bak "s/^\(User\).*/\1 rof/g" apache/conf/httpd.conf && rm apache/conf/httpd.conf.bak
## Enable some modules
sed -i.bak "s/#\(LoadModule actions_module.*\)$/\1/" apache/conf/httpd.conf && rm apache/conf/httpd.conf.bak
sed -i.bak "s/#\(LoadModule rewrite_module.*\)$/\1/" apache/conf/httpd.conf && rm apache/conf/httpd.conf.bak
sed -i.bak "s/#\(LoadModule ssl_module.*\)$/\1/" apache/conf/httpd.conf && rm apache/conf/httpd.conf.bak
sed -i.bak "s/#\(LoadModule cgid_module.*\)$/\1/" apache/conf/httpd.conf && rm apache/conf/httpd.conf.bak
echo >> apache/conf/httpd.conf
echo "Include conf/extra/takein.conf" >> apache/conf/httpd.conf
## Deal with PHP5
# echo >> apache/conf/httpd.conf
# echo "<IfModule prefork.c>" >> apache/conf/httpd.conf
# echo "  LoadModule php5_module modules/libphp-5.6.so" >> apache/conf/httpd.conf
# echo "</IfModule>" >> apache/conf/httpd.conf

# echo "<IfModule "'!'"prefork.c>" >> apache/conf/httpd.conf
# echo "  LoadModule php5_module modules/libphp-zts-5.6.so" >> apache/conf/httpd.conf
# echo "</IfModule>" >> apache/conf/httpd.conf
## Move dropin confs
if [ -e ~/clone/.ci/takein.conf ]; then cp ~/clone/.ci/takein.conf apache/conf/extra/; fi;
# curl -o "apache/conf/extra/php.conf" https://s3-ap-southeast-2.amazonaws.com/takein-deployments/public/lamp/conf.d/php.conf

## Start your engines
cd ~
apache/bin/apachectl start

## Why does Basic auth not work: http://stackoverflow.com/questions/7884687/http-authentication-with-php-running-as-fcgi

exit 0