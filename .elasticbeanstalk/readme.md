## Using Saved Configurations

_To apply a saved configuration when you create an environment with eb create, use the --cfg option:_

    ~/workspace/my-app$ eb create --cfg savedconfig

## Tags

_If you use the EB CLI to create environments, use the --tags option with eb create to add tags:_

    ~/workspace/my-app$ eb create --tags mytag1=value1,mytag2=value2
