'use strict';

//=======================================
//
// Test file for Logins
//
// Test last updated for:
// + commit 3cae8cb98447620d5862cb68059503a4ed6e8cfb
//
//=======================================

// var path = require('path');
var frisby = require('frisby');
var ti = require('../takein');

// Grab some user data
var requester = ti.provideTestData('testdata/users-existing.json').shift();


//===================
// Login Correct password
    
var correctBasicAuthHeaderValue = 'Basic ' + new Buffer(requester.username + ':' + requester.password).toString('base64');
 
frisby.create('Login with Basic auth')
    .post(ti.endpoint('login'))
    .addHeader('Authorization', correctBasicAuthHeaderValue)
    .expectStatus(200)
    .expectJSONTypes({
        api_token: String,
        expires_at: String,
        disabled: Boolean,
        needs_reset: Boolean
    })
    // .inspectJSON()
    .toss();

//===================
// Login wrong password

var wrongBasicAuthHeaderValue = 'Basic ' + new Buffer(requester.username + ':' + requester.password + 'aaa').toString('base64');
    
frisby.create('Ensure incorrect logins dont work')
    .post(ti.endpoint('login'))
    .addHeader('Authorization', wrongBasicAuthHeaderValue)
    .expectStatus(401)
    .toss();
    
    
//===================
// Handshake

frisby.create('Ensure handshake works')
    .get(ti.endpoint('user/handshake') + '?name=' + requester.email)
    .expectStatus(200)
    .toss();
    
    
//===================
// Forgot password

// Todo

    
//===================
// Logouts
// Testing logouts is hard without destroying tokens...

// frisby.create('Ensure Logout works')
    // .get(ti.endpoint('logout') + '?name=' + requester.email)
    // .expectStatus(200)
    // .toss();
