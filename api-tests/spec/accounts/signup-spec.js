'use strict';

//=======================================
//
// Test file for signup and account-related actions
//
// Test last updated for:
// + commit 3cae8cb98447620d5862cb68059503a4ed6e8cfb
//
//=======================================

var fs = require('fs');
var frisby = require('frisby');
var ti = require('../takein');


// Grab some user data
var newUserData = ti.provideTestData('testdata/users-new.json').shift();
// Make unique
newUserData.username = +(new Date) + '_' + newUserData.username;
newUserData.email = +(new Date) + '_' + newUserData.email;


//===================
// Signup

frisby.create('Ensure signups work')
    .post(ti.endpoint('user/signup'), { user: newUserData })
    .expectStatus(201)
    .toss();
