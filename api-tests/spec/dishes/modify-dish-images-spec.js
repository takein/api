'use strict';

//=======================================
//
// Test file for Dish image uploading
// Todo
//
//=======================================

const frisby = require('frisby');
const ti = require('../takein');

// Grab some user data
var requester = ti.provideTestData('testdata/users-existing.json').shift();
var dishesData = ti.provideTestData('testdata/dishes-new.json', 2);
var dishData1 = dishesData.shift(), dishData2 = dishesData.shift();

