'use strict';

//=======================================
//
// Test file for Dishes - Create
//
// Test last updated for:
// + commit e5a720a4e8489bfe7551a75ad1a97657aefd891f
//
//=======================================

const frisby = require('frisby');
const ti = require('../takein');

// Grab some user data
var requester = ti.provideTestData('testdata/users-existing.json').shift();
var dishesData = ti.provideTestData('testdata/dishes-new.json', 2);
var dishData1 = dishesData.shift(), dishData2 = dishesData.shift();

//==========================
// Create 2x dishes as standard use cases

frisby.create('Dish creation 1')
    .post(ti.endpoint('dish'), { dish: dishData1 })
    .addHeaders({ 'X-API-Token': requester.api_token })
    .expectStatus(201)
    .toss();

frisby.create('Dish creation 2')
    .post(ti.endpoint('dish'), { dish: dishData1 })
    .addHeaders({ 'X-API-Token': requester.api_token })
    .expectStatus(201)
    .expectJSONTypes('success', {
      result: String,
      id: Number,
      type: String
    })
    .afterJSON(function(json){
        
        //===============================
        // Updates and deletes

        (function(dishId){

            //======================
            // Update the dish
            
            // Todo: create some more inventive deltas
            var delta1 = { name: 'Iron Grill double burger' };
            var delta2 = { owner_id: 'Iron Grill double burger' };

            frisby.create('Dish update with PATCH')
                .patch(ti.endpoint('dish/'+dishId), { dish: delta1 })
                .addHeaders({ 'X-API-Token': requester.api_token })
                .expectStatus(200)
                .toss();


            frisby.create('Dish update with PUT')
                .put(ti.endpoint('dish/'+dishId), { dish: delta1 })
                .addHeaders({ 'X-API-Token': requester.api_token })
                .expectStatus(200)
                .toss(); 

            //=============
            // Delete the dishes

            frisby.create('Delete dish')
                .delete(ti.endpoint('dish/'+dishId))
                .addHeaders({ 'X-API-Token': requester.api_token })
                .expectJSON('success', {
                    result: 'deleted',
                    type: 'listing'
                })
                .expectStatus(200)
                .toss(); 

            
        }(json.success.id));

    })
    .toss();

    
    
frisby.create('Dish creation 3 - delivery_cost break case')
    .post(ti.endpoint('dish'), {  
       "dish":{  
          "name":"Salted Egg Fish",
          "description":"Extremely delish salted egg fish",
          "price_per_serving":6.8,
          "calories":480,
          "date":"2016-02-23T13:00:00.000Z",
          "cutoff_time":"2016-04-15T16:05:00.000Z",
          "delivery":true,
          "start_delivery_time":"1970-01-01T04:00:00.000Z",
          "end_delivery_time":"1970-01-01T08:00:00.000Z",
          "delivery_radius":"3088",
          "delivery_notes":"Message me for specific delivery times",
          "pickUp":true,
          "start_pickup_time":"1970-01-01T06:00:00.000Z",
          "end_pickup_time":"1970-01-01T10:00:00.000Z",
          "pickup_notes":"Call before you head out please.",
          "pickup_delivery_options":"DP",
          "servings_total":3,
          "servings_remaining":3,
          "delivery_address_center_input":{  
             "address_components":[  
                {  
                   "long_name":"1",
                   "short_name":"1",
                   "types":[  
                      "street_number"
                   ]
                },
                {  
                   "long_name":"Pearl Street",
                   "short_name":"Pearl St",
                   "types":[  
                      "route"
                   ]
                },
                {  
                   "long_name":"Hurstville",
                   "short_name":"Hurstville",
                   "types":[  
                      "locality",
                      "political"
                   ]
                },
                {  
                   "long_name":"New South Wales",
                   "short_name":"NSW",
                   "types":[  
                      "administrative_area_level_1",
                      "political"
                   ]
                },
                {  
                   "long_name":"Australia",
                   "short_name":"AU",
                   "types":[  
                      "country",
                      "political"
                   ]
                },
                {  
                   "long_name":"2220",
                   "short_name":"2220",
                   "types":[  
                      "postal_code"
                   ]
                }
             ],
             "formatted_address":"1 Pearl St, Hurstville NSW 2220, Australia",
             "geometry":{  
                "bounds":{  
                   "south":-33.9626517,
                   "west":151.09633080000003,
                   "north":-33.9626391,
                   "east":151.0963418
                },
                "location":{  
                   "lat":-33.9626517,
                   "lng":151.0963418
                },
                "location_type":"RANGE_INTERPOLATED",
                "viewport":{  
                   "south":-33.9639943802915,
                   "west":151.09498731970848,
                   "north":-33.9612964197085,
                   "east":151.09768528029144
                }
             },
             "place_id":"EioxIFBlYXJsIFN0LCBIdXJzdHZpbGxlIE5TVyAyMjIwLCBBdXN0cmFsaWE",
             "types":[  
                "street_address"
             ]
          },
          "pickup_address_input":{  
             "address_components":[  
                {  
                   "long_name":"Hurstville Station",
                   "short_name":"Hurstville Station",
                   "types":[  
                      "point_of_interest",
                      "establishment"
                   ]
                },
                {  
                   "long_name":"Hurstville",
                   "short_name":"Hurstville",
                   "types":[  
                      "locality",
                      "political"
                   ]
                },
                {  
                   "long_name":"Sydney",
                   "short_name":"Sydney",
                   "types":[  
                      "colloquial_area",
                      "locality",
                      "political"
                   ]
                },
                {  
                   "long_name":"New South Wales",
                   "short_name":"NSW",
                   "types":[  
                      "administrative_area_level_1",
                      "political"
                   ]
                },
                {  
                   "long_name":"Australia",
                   "short_name":"AU",
                   "types":[  
                      "country",
                      "political"
                   ]
                },
                {  
                   "long_name":"2220",
                   "short_name":"2220",
                   "types":[  
                      "postal_code"
                   ]
                }
             ],
             "formatted_address":"Hurstville Station, Hurstville NSW 2220, Australia",
             "geometry":{  
                "location":{  
                   "lat":-33.9676282,
                   "lng":151.10231450000003
                },
                "location_type":"APPROXIMATE",
                "viewport":{  
                   "south":-33.9689771802915,
                   "west":151.10096551970855,
                   "north":-33.9662792197085,
                   "east":151.10366348029152
                }
             },
             "place_id":"ChIJBX8vBqO5EmsR3WWdfEoH19o",
             "types":[  
                "train_station",
                "transit_station",
                "point_of_interest",
                "establishment"
             ]
          },
          "delivery_cost":2,
          "date_available":null,
          "delivery_address_center":{  
             "address_components":[  
                {  
                   "long_name":"1",
                   "short_name":"1",
                   "types":[  
                      "street_number"
                   ]
                },
                {  
                   "long_name":"Pearl Street",
                   "short_name":"Pearl St",
                   "types":[  
                      "route"
                   ]
                },
                {  
                   "long_name":"Hurstville",
                   "short_name":"Hurstville",
                   "types":[  
                      "locality",
                      "political"
                   ]
                },
                {  
                   "long_name":"New South Wales",
                   "short_name":"NSW",
                   "types":[  
                      "administrative_area_level_1",
                      "political"
                   ]
                },
                {  
                   "long_name":"Australia",
                   "short_name":"AU",
                   "types":[  
                      "country",
                      "political"
                   ]
                },
                {  
                   "long_name":"2220",
                   "short_name":"2220",
                   "types":[  
                      "postal_code"
                   ]
                }
             ],
             "formatted_address":"1 Pearl St, Hurstville NSW 2220, Australia",
             "geometry":{  
                "bounds":{  
                   "south":-33.9626517,
                   "west":151.09633080000003,
                   "north":-33.9626391,
                   "east":151.0963418
                },
                "location":{  
                   "lat":-33.9626517,
                   "lng":151.0963418
                },
                "location_type":"RANGE_INTERPOLATED",
                "viewport":{  
                   "south":-33.9639943802915,
                   "west":151.09498731970848,
                   "north":-33.9612964197085,
                   "east":151.09768528029144
                }
             },
             "place_id":"EioxIFBlYXJsIFN0LCBIdXJzdHZpbGxlIE5TVyAyMjIwLCBBdXN0cmFsaWE",
             "types":[  
                "street_address"
             ]
          },
          "pickup_address":{  
             "address_components":[  
                {  
                   "long_name":"Hurstville Station",
                   "short_name":"Hurstville Station",
                   "types":[  
                      "point_of_interest",
                      "establishment"
                   ]
                },
                {  
                   "long_name":"Hurstville",
                   "short_name":"Hurstville",
                   "types":[  
                      "locality",
                      "political"
                   ]
                },
                {  
                   "long_name":"Sydney",
                   "short_name":"Sydney",
                   "types":[  
                      "colloquial_area",
                      "locality",
                      "political"
                   ]
                },
                {  
                   "long_name":"New South Wales",
                   "short_name":"NSW",
                   "types":[  
                      "administrative_area_level_1",
                      "political"
                   ]
                },
                {  
                   "long_name":"Australia",
                   "short_name":"AU",
                   "types":[  
                      "country",
                      "political"
                   ]
                },
                {  
                   "long_name":"2220",
                   "short_name":"2220",
                   "types":[  
                      "postal_code"
                   ]
                }
             ],
             "formatted_address":"Hurstville Station, Hurstville NSW 2220, Australia",
             "geometry":{  
                "location":{  
                   "lat":-33.9676282,
                   "lng":151.10231450000003
                },
                "location_type":"APPROXIMATE",
                "viewport":{  
                   "south":-33.9689771802915,
                   "west":151.10096551970855,
                   "north":-33.9662792197085,
                   "east":151.10366348029152
                }
             },
             "place_id":"ChIJBX8vBqO5EmsR3WWdfEoH19o",
             "types":[  
                "train_station",
                "transit_station",
                "point_of_interest",
                "establishment"
             ]
          }
       }
    })
    .addHeaders({ 'X-API-Token': requester.api_token })
    .expectStatus(201)
    .toss();

    

//===================
// Create dish wrong token
// Create dish no token
    
frisby.create('Dish creation with wrong token')
    .post(ti.endpoint('dish'), { dish: dishData1 })
    .addHeaders({ 'X-API-Token': 'zzzzzzzzzzzzzzzzzzzzz' })
    .expectStatus(401)
    .toss();
    
    
frisby.create('Dish creation without a token')
    .post(ti.endpoint('dish'), { dish: dishData2 })
    .expectStatus(401)
    .toss();


//======================
// Create dish but missing vital info

(function(dishData){
    delete dishData.price_per_serving; 
    delete dishData.name;
    
    frisby.create('Dish creation missing vital info (1)')
        .post(ti.endpoint('dish'), { dish: dishData })
        .addHeaders({ 'X-API-Token': requester.api_token })
        .expectStatus(400)
        .toss();
    
}(ti.provideTestData('testdata/dishes-new.json').shift()));

(function(dishData){
    delete dishData.cutoff_time; 
    
    frisby.create('Dish creation missing vital info (2)')
        .post(ti.endpoint('dish'), { dish: dishData })
        .addHeaders({ 'X-API-Token': requester.api_token })
        .expectStatus(400)
        .toss();
    
}(ti.provideTestData('testdata/dishes-new.json').shift()));


//===================
// Dish editing policy test

(function(){
    
    var dishData, failsafeCounter = 0, failsafeLimit = 99;
    do {
        dishData = ti.provideTestData('testdata/dishes-existing.json').shift();
        failsafeCounter++;
    } while (dishData.owner_id !== requester.id && failsafeCounter <= failsafeLimit);
    
    frisby.create('Dish modification with wrong token')
        .put(ti.endpoint('dish/'+dishData.id), { dish: dishData })
        .addHeaders({ 'X-API-Token': 'zzzzzzzzzzzzzzzzzzzzz' })
        .expectStatus(401)
        .toss();
        
    frisby.create('Dish update without a token')
        .put(ti.endpoint('dish/'+dishData.id), { dish: dishData })
        .expectStatus(401)
        .toss();
    
    frisby.create('Dish deletion with wrong token')
        .delete(ti.endpoint('dish/'+dishData.id))
        .addHeaders({ 'X-API-Token': 'zzzzzzzzzzzzzzzzzzzzz' })
        .expectStatus(401)
        .toss();
    
    
    frisby.create('Dish deletion without a token')
        .delete(ti.endpoint('dish/'+dishData.id))
        .expectStatus(401)
        .toss();
        
    
}());