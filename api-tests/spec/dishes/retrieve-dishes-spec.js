'use strict';

//=======================================
//
// Test file for Dishes
//
// Test last updated for:
// + commit e5a720a4e8489bfe7551a75ad1a97657aefd891f
//
//=======================================

const frisby = require('frisby');
const ti = require('../takein');

// Grab some user data
var requester = ti.provideTestData('testdata/users-existing.json').shift();

// Set some stuff
var dishLimit = 1;
    
//===================
// Retrieve dishes

frisby.create('Ensure get all dishes returns results')
    .get(ti.endpoint('dishes'))
    .expectStatus(200)
    .toss();
    

//===================
// Retrieve dishes limited
// Retrieve dishes with pagination

(function(dishLimit){

    frisby.create('Ensure get all dishes with limit returns correct limit')
        .get(ti.endpoint('dishes') + '?max=' + dishLimit)
        .expectStatus(200)
        .expectJSON('meta', {
            total: dishLimit
        })
        .expectJSONLength('dishes', dishLimit)
        .afterJSON(function(json){
            
            // Get next page - testdata has at least 2 dishes
            frisby.create('Ensure dishes prev pagination works (Limited URL)')
                .get(json.cursor.beforeUrlLimited)
                .expectStatus(200)
                .toss(); 
                
            frisby.create('Ensure dishes prev pagination works (Unlimited URL)')
                .get(json.cursor.beforeUrl)
                .expectStatus(200)
                .toss(); 
            
        })
        .toss();
    
}(dishLimit));


//===========================================
// Retrieve my own dishes (if you are a chef)

frisby.create('Ensure own listings turn up')
    .get(ti.endpoint('user/me/dishes'))
    .addHeaders({ 'X-API-Token': requester.api_token })
    .expectStatus(200)
    .expectJSONTypes({
        dishes: Object,
        meta: Object,
        cursor: Object
    })
    .toss();



//======================
// Retrieve any dish
    
frisby.create('Ensure dish retrieval returns correct data')
    .get(ti.endpoint('dish/1'))
    .expectStatus(200)
    .expectJSONTypes('dish', {
        chef: Object
    })
    .afterJSON(function(json){
        // Check dates are returning in the right format
        expect(json.dish.created_at).toMatch(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{4}$/);
    })
    .toss();


//======================
// Retrieve a non-existent dish
    
frisby.create('Ensure nonexist dish returns not found')
    .get(ti.endpoint('dish/999999559999999'))
    .expectStatus(404)
    .toss();

    
