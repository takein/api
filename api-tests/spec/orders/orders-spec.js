'use strict';

//=======================================
//
// Test file for Dishes - Create
//
// Test last updated for:
// + commit e5a720a4e8489bfe7551a75ad1a97657aefd891f
//
//=======================================

var frisby = require('frisby');
var ti = require('../takein');

var requesters = ti.provideTestDataAll('testdata/users-existing.json');
var requester10 = requesters.find(function(requester){
    return requester.id === 10;
});
var requester12 = requesters.find(function(requester){
    return requester.id === 12;
});

var dishes = ti.provideTestDataAll('testdata/dishes-existing.json');
var dish10 = dishes.find(function(dish){
    return dish.owner_id === 10;
});


//===================
// Make an order

frisby.create('Create an order pickup')
    .post(ti.endpoint('dish/'+dish10.id+'/order'), {
        order: {
            servings_purchased: 1,
            pickup_delivery: 'P',
            pickup_preferred_time: '2016-04-02 20:20:20',
            notes: 'I am keen',
        }
    })
    .addHeaders({ 'X-API-Token': requester12.api_token })           // Use requester12 to order requester10's stuff
    .expectStatus(201)
    .afterJSON(function(json){
        expect(json.success).not.toBeUndefined();           // sanity check
    })
    .toss();
    
   
//===================
// Order retrieval

var existingOrderIds = [];

frisby.create('Retrieval of orders for an existing user')
    .get(ti.endpoint('user/me/orders'))
    .addHeaders({ 'X-API-Token': requester10.api_token })
    .expectStatus(200)
    .afterJSON(function(json){
        
        expect(json.orders).not.toBeUndefined();
        expect(json.orders.length).toBeGreaterThan(0);
        json.orders.forEach(function(order){
            existingOrderIds.push(order.id);
        });
        
    })
    .toss();

existingOrderIds.forEach(function(orderId){
    frisby.create('Retrieve an order directly by ID')
        .get(ti.endpoint('order/'+orderId))
        .addHeaders({ 'X-API-Token': requester10.api_token })
        .expectStatus(200)
        .afterJSON(function(json){
            expect(json.order).not.toBeUndefined();
            expect(json.order.id).toEqual(4);
        })
        .toss();
});

frisby.create('Retrieve an order that is someone else\'s')
    .get(ti.endpoint('order/' + existingOrderIds[0]))
    .addHeaders({ 'X-API-Token': requester12.api_token })                   // Specifically use requester12 instead of requester10
    .expectStatus(403)
    .toss();
    
frisby.create('Retrieve an order that won\'t exist')
    .get(ti.endpoint('order/999999999'))
    .addHeaders({ 'X-API-Token': requester12.api_token })
    .expectStatus(403)
    .toss();
    

//===============
// Dish order retrieval

frisby.create('Retrieve orders for a dish as other user')
    .get(ti.endpoint('dish/'+dish10.id+'/orders'))
    .addHeaders({ 'X-API-Token': requester12.api_token })                   // Specifically use requester12 instead of requester10
    .expectStatus(200)
    .expectJSONTypes('orders.*', {
        id: Number,
        tx: Object
    })
    .expectJSONTypes('orders.*.tx', {
        seller_id: Number,
        buyer_id: Number
    })
    .toss();
    
frisby.create('Retrieve orders for a dish as the owner')
    .get(ti.endpoint('dish/'+dish10.id+'/orders'))
    .addHeaders({ 'X-API-Token': requester12.api_token })                   // Specifically use requester12 instead of requester10
    .expectStatus(200)
    .expectJSONTypes('orders.*', {
        id: Number,
        tx: Object
    })
    .expectJSONTypes('orders.*.tx', {
        id: Number,
        listing_id: Number,
        accept_state: String,
        servings_purchased: Number,
        preferred_pickup_time: function(val) { expect(val).toBeTypeOrNull(String); },              // e.g. 2016-03-11T10:59:53+0000       //toMatch(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{4}/)
        preferred_delivery_time: function(val) { expect(val).toBeTypeOrNull(String); }, 
        delivery_address: String,
        notes: String,
    })
    .toss();