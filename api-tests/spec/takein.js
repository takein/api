'use strict';

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const fs = require('fs');
const https = require('https');
const extend = require('extend');

module.exports = (function(){
    
    var endpointBase = (function(){
        var protocol = process.env.TI_TEST_ENDPOINT_PROTOCOL || 'https://';
        var domain = process.env.TI_TEST_ENDPOINT_DOMAIN || 'dev.take-in.net.au';
        var prefix = process.env.TI_TEST_ENDPOINT_PREFIX || '/api/v1';
        return [protocol, domain, prefix].join('');        
    }());
    
    return {
        provideTestData: provideTestData,
        provideTestDataAll: provideTestDataAll,
        endpoint: endpoint,
    };
    
    //============
    // Functions

    function endpoint(path){
        return endpointBase + ( path.indexOf(0) !== '/' ? '/' + path : path );
    }
    
    function provideTestDataAll(filepath){
        filepath = __dirname + ( filepath.indexOf(0) === '/' ? '/..' : '/../' ) + filepath;
        var testDataItems = JSON.parse(fs.readFileSync(filepath, 'utf8'));                      // Let's grab some testDataItem, expect an array
        if (! (testDataItems instanceof Array)){
            throw "Expected test data source file to be JSONArray";
        }
        return testDataItems;
    }
        
    function provideTestData(filepath, numItems, opts){
        var testDataItems = provideTestDataAll.call(this, filepath);
        numItems = numItems || testDataItems.length;
        if (numItems > testDataItems.length){
            return testDataItems
        }
        else {
            var randomIndexes = [];
            for (var i = 0; i < numItems; i++){
                var suggestedRandomNumber = 0, failsafeCounter = 0, failsafeLimit = 99;
                do {
                    suggestedRandomNumber = Math.floor(Math.random() * testDataItems.length);
                    failsafeCounter++;
                } while (!~randomIndexes.indexOf(suggestedRandomNumber) && failsafeCounter <= failsafeLimit);
                randomIndexes.push(suggestedRandomNumber);
            }
            return randomIndexes.map(function(randomIndex){
                return testDataItems[randomIndex];
            });
        }
    }

}());
