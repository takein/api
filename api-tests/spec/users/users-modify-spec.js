'use strict';

//==============================================================================
// Test file for User accounts modification (updating and disabling)
//==============================================================================

var frisby = require('frisby');
var ti = require('../takein');

var requesters = ti.provideTestDataAll('testdata/users-existing.json');
var requester10 = requesters.find(function(requester){
    return requester.id === 10;
});
var requester12 = requesters.find(function(requester){
    return requester.id === 12;
});

//===================
// Update my own details

var testNameStub = 'Test' + +(new Date);

frisby.create('Update my own user details (PATCH)')
    .patch(ti.endpoint('user/me'), {
        user: {
            username: testNameStub,
            email: testNameStub + '@gmail.com',
            street_address: '1234 Jordane St, Roseville NSW'
        }
    })
    .addHeaders({ 'X-API-Token': requester12.api_token })           // Use requester12 to order requester10's stuff
    .expectStatus(200)
    .expectJSON('success', {
        result: 'updated'
    })
    .toss();
    
frisby.create('Update my own user details (PUT)')
    .put(ti.endpoint('user/me'), {
        user: {
            username: testNameStub,
            email: testNameStub + '@gmail.com',
            street_address: '2345 Mundane St, Roseville NSW'
        }
    })
    .addHeaders({ 'X-API-Token': requester12.api_token })           // Use requester12 to order requester10's stuff
    .expectStatus(200)
    .expectJSON('success', {
        result: 'updated'
    })
    .toss();
    
    
//======================
// Update to conflicted name

frisby.create('Update to an already existing username')
    .put(ti.endpoint('user/me'), {
        user: {
            username: testNameStub,
            street_address: '1234 Jordane St, Roseville NSW'
        }
    })
    .addHeaders({ 'X-API-Token': requester10.api_token })   
    .expectStatus(400)
    .expectJSON('error', {
        code: 4110
    })
    .toss();
    
    
//======================
// Update to conflicted email

frisby.create('Update to an already existing email')
    .put(ti.endpoint('user/me'), {
        user: {
            email: testNameStub + '@gmail.com'
        }
    })
    .addHeaders({ 'X-API-Token': requester10.api_token })  
    .expectStatus(400)
    .expectJSON('error', {
        code: 4115
    })
    .toss();
    
    
    
//======================
// Disable the account
// Todo: Figure out automated testing that involves receiving SMS code, and 
// deals gracefully with killing the API token etc

/*
frisby.create('Update to an already existing email')
    .delete(ti.endpoint('user/me'))
    .addHeaders({ 'X-API-Token': requester10.api_token })  
    .expectStatus(200)
    .expectJSON('success', {
        result: 'disabled'
    })
    .toss();
*/

//===================
// Update my password
// Todo: Figure out automated testing that involves receiving SMS code, and 
// deals gracefully with killing the API token etc

/*
frisby.create('Update password')
    .patch(ti.endpoint('user/me'), {
        password: {
            username: testNameStub
        }
    })
    .addHeaders({ 'X-API-Token': requester12.api_token })           // Use requester12 to order requester10's stuff
    .expectStatus(200)
    .toss();
*/
    
