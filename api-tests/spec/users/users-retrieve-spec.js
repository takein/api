'use strict';

///=======================================
///
/// Test file for User accounts getting (/user/me, /user/x)
///
///=======================================

var frisby = require('frisby');
var ti = require('../takein');

var requesters = ti.provideTestDataAll('testdata/users-existing.json');
var requester1 = requesters.find(function(requester){
    return requester.id === 10;
});
var requester2 = requesters.find(function(requester){
    return requester.id === 12;
});


//===============
// User data requests

frisby.create('Requesters own user data')
    .get(ti.endpoint('user/me'))
    .addHeaders({ 'X-API-Token': requester1.api_token })           // Use requester12 to order requester10's stuff
    .expectStatus(200)
    .expectJSON('user', {
        id: requester1.id
    })
    .expectJSONTypes('user', {
        id: Number,
        // first_name: String,
        // last_name: String,
        username: String,
        email: String,
        mobile: String,
        force_password_set: Boolean,        // important-expect these to be in /user/me
        is_system: Boolean,                     // important-expect these to be in /user/me
        member_since: String,
        chef: function(val) { expect(val).toBeTypeOrNull(Object); }
    })
    .afterJSON(function(json){
       expect(json.user.member_since).toMatch(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{4}$/); 
       expect(json.user.mobile).toMatch(/^\+\d{10,12}$/); 
    })
    .toss();
    
frisby.create('User data (when requester authed)')
    .get(ti.endpoint('user/'+requester2.id))
    .addHeaders({ 'X-API-Token': requester1.api_token })           // Use requester12 to order requester10's stuff
    .expectStatus(200)
    .expectJSON('user', {
        id: requester2.id
    })
    .expectJSONTypes('user', {
        id: Number,
        // first_name: String,
        // last_name: String,
        username: String,
        email: String,
        mobile: String,
        member_since: String,
        chef: function(val) { expect(val).toBeTypeOrNull(Object); }
    })
    .afterJSON(function(json){
       expect(json.user.member_since).toMatch(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{4}$/); 
       expect(json.user.mobile).toMatch(/^\+\d{10,12}$/); 
    })
    .toss();
    
    
frisby.create('User data (when requester anon)')
    .get(ti.endpoint('user/'+requester2.id))
    .expectStatus(200)
    .expectJSON('user', {
        id: requester2.id
    })
    .expectJSONTypes('user', {
        id: Number,
        // first_name: String,
        // last_name: String,
        username: String,
        member_since: String,
        email: function(val) { expect(val).toBeUndefined(); },
        mobile: function(val) { expect(val).toBeUndefined(); }
    })
    .afterJSON(function(json){
       expect(json.user.member_since).toMatch(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{4}$/); 
    })
    .toss();



//===============
// /user/x/image test

frisby.create('Requester own image turns up correctly')
    .get(ti.endpoint('user/me/image'))
    .addHeaders({ 'X-API-Token': requester1.api_token })           // Use requester12 to order requester10's stuff
    .expectStatus(200)
    .expectJSON('image', {
        id: requester1.id
    })
    .toss();
    
frisby.create('Another users image turns up (when requester authed)')
    .get(ti.endpoint('user/'+requester2.id+'/image'))
    .addHeaders({ 'X-API-Token': requester2.api_token })           // Use requester12 to order requester10's stuff
    .expectStatus(200)
    .expectJSON('image', {
        id: requester2.id
    })
    .toss();
    
frisby.create('Another users image turns up (when requester anon)')
    .get(ti.endpoint('user/'+requester2.id+'/image'))
    .expectStatus(200)
    .expectJSON('image', {
        id: requester2.id
    })
    .toss();
    
