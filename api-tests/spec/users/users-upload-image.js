'use strict';

///========================================================
/// Test file for User accounts getting (/user/me, /user/x)
///========================================================

var fs = require('fs');
var frisby = require('frisby');
var ti = require('../takein');

///==========================
/// Upload a user profile pic

var userPicBitmap = fs.readFileSync(__dirname + '/../../data/pics/userpic1.jpg');
var userPicEnc = new Buffer(userPicBitmap).toString('base64');

frisby.create('Upload profile pic')
    .post(ti.endpoint('user/me/image', {
        user_image_file: userPicEnc
    })
    .addHeaders({ 'X-API-Token': requester.api_token, 'content-type': 'multipart/form-data' })
    .expectStatus(201)
    .toss();
