<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class TempTokensCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'db:tokens';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Display tokens/codes from the temporary tokens table.';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $howMany = $this->option('num');
        
        $tokens = DB::table('system_temp_tokens AS stt')
            ->select('stt.user_id', 'users.username', 'stt.manual_code', 'stt.expires_at', 'stt.deleted_at')
            ->join('users', 'users.id', '=', 'stt.user_id')
            ->orderBy('stt.created_at', 'desc')
            ->take($howMany)
            ->get();
        
        
        $rows = collect($tokens)->map(function ($item) {
            return ( array ) $item;
        });
        
        $headers = array('UserID', 'Username', 'Code', 'Expires', 'Used on');
        $this->table($headers, $rows);
    }
    
    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['num', null, InputOption::VALUE_OPTIONAL, 'How many recent tokens/codes to retrieve.', '10'],
        ];
    }

}
