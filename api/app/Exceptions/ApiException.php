<?php

namespace App\Exceptions;

use Log;
use Exception;
use Illuminate\Http\Request;
use App\Http\Requests\ApiRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ApiException extends Exception
{
    protected $request;     // the Illuminate\Http\Request object associated with this API error
    protected $previous;
    
    public function __construct($message, $code = 0, Request $request = null, Exception $previous = null)
    {
        $this->request = $request;
        $this->previous = $previous;
        parent::__construct($message, $code);
    }
    
    public function toString()
    {
        __CLASS__ . " Code {$this->getCode()}, '{$this->getMessage()}', routed to {$this->request->method()} {$this->request->route()}, requested by {$this->getRequestingUserDesc()}" ;
    }
    
    public function getRequest()
    {
        return $this->request;
    }
        
    public function getInnerException()
    {
        return $this->previous;
    }
    
    public function getPreviousException()
    {
        return $this->previous;    
    }
    
    public function getRequestingUserDesc()
    {
        return !empty($this->request) ? "{$this->request()->user()->id} {$this->request()->user()->username}" : "";
    }
    
    public function getRequestSummary()
    {
        return !empty($this->request) ? "{$this->request->method()} {$this->request->route()}" : "";
    }
    
}
