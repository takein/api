<?php

namespace App\Exceptions;

use Exception;
use Log;
use App\Exceptions\ApiException;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        if ($e instanceof ApiException){
            
            $apiLog = new Logger('API Exception Log');
            $apiLog->pushHandler(new StreamHandler('storage/logs/api.log', Logger::ERROR));
            $apiLog->addError($e->getMessage(), [
                'code' => $e->getCode(), 
                'request' => $e->getRequestSummary(), 
                'user' => $e->getRequestingUserDesc()
            ]);
            
            // Log::error();
        }
        
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        // Always render 404's as a JSON response
        if ($e instanceof ModelNotFoundException || $e instanceof NotFoundHttpException){
            return response()->json([
                'error' => [
                    'code' => 404,
                    'message' => 'Not Found'
                ]
            ], 404);
        }
        
        // If not in debug, render any other errors as JSON too
        $debug = env('APP_DEBUG') == 'true';
        if (!$debug){
            return response()->json([
                'error' => [
                    'code' => 500,
                    'type' => gettype($e),
                    'message' => $e->getMessage()
                ]
            ], 500);
        }

        return parent::render($request, $e);
    }
}
