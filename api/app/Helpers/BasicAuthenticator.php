<?php

namespace App\Helpers;

use DB;
use Exception;
use Illuminate\Contracts\Auth\SupportsBasicAuth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Exceptions\ApiException;

class BasicAuthenticator
{
	private $request;

	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	public function basic($field = 'username', $extraConditions = [])
	{
		throw new BadMethodCallException();
	}

    /**
     * Perform a stateless HTTP Basic login attempt.
     *
     * Derived from https://github.com/laravel/framework/blob/5.2/src/Illuminate/Auth/SessionGuard.php#L289
     * 
     * @param  string  $field
     * @param  array  $extraConditions
     * @return \Symfony\Component\HttpFoundation\Response|null
     */
	public function onceBasic($field = 'username', $extraConditions = [])
	{
		list($username, $plainPassword) = $this->getBasicCredentials();
		$userHashedPassword = User::where($field, $username)->value('password');
                
		if (!empty($userHashedPassword)){
			return app('hash')->check($plainPassword, $userHashedPassword);
		}
		return false;
	}
	
    protected function getBasicCredentials()
    {
        return [$this->request->getUser(), $this->request->getPassword()];
    }

}