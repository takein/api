<?php

namespace App\Helpers;

use Exception;
use Validator;
use Illuminate\Http\Request;
use App\Exceptions\ApiException;

class Formatter
{
    private static $cursorSeparator = '.';
    
    /**
     * Standardise the mobile phone number input
     * 
     * @param mixed  A representation of a phone number
     * @return string  Normalised phone number
     */
    public static function MobilePhone($mobile)
    {
        $mobile = preg_replace('/[a-zA-Z]/', '', $mobile);     // strip alphabet chars
        $mobile = preg_replace('/\s+/', '', $mobile);       // strip spaces
        
        // Replace a leading zero with +61
        $mobile = preg_replace('/^0/', '+61', $mobile);
        
        return $mobile;
    }
    
    /**
     * Removes spaces from stuff where it shouldn't be
     *
     * @param string  original
     * @return string  stripped
     */
    public static function StripSpaces($incoming)
    {
        return preg_replace('/\s+/', '', $incoming);
    }
    
    /**
     * Universal data masker
     * Replace certain parts with asterisks
     *
     * @param string  Sensitive data
     * @return string  Obscured data
     */
    public static function MaskData($sensitive = null)
    {
        if (is_null($sensitive)){
            return '?';         // Todo: would this reveal the non-existence of this data?
        }
        
        // Simple detect if it might be an email address
        if (strpos($sensitive, '@') !== false){
            $masked = '';
            $emailParts = explode('@', $sensitive);
            
            // two chars from beginning and one from end
            // Todo: move these into config or something
            $revealFromFront = 2;
            $revealFromBack = 1;
            $revealLength = $revealFromFront + $revealFromBack;
            
            if (strlen($emailParts[0]) > $revealLength){
                $masked .= substr($emailParts[0], 0, $revealFromFront) . str_repeat('*', strlen($emailParts[0]) - $revealLength) . substr($emailParts[0], -$revealFromBack);
            }
            else {
                $masked .= str_repeat('*', $revealLength);
            }
            $masked .= '@';
            if (strpos($emailParts[1], '.') !== false){
                $domainParts = explode('.', $emailParts[1]);
                $tld = array_pop($domainParts);
                $domainName = implode('.', $domainParts);
                if (strlen($emailParts[0]) > $revealLength){
                    $masked .= substr($domainName, 0, $revealFromFront) . str_repeat('*', strlen($domainName) - $revealLength) . substr($domainName, -$revealFromBack);
                }
                else {
                    $masked .= str_repeat('*', $revealLength);
                }
                $masked .= ".$tld";
            }
            return $masked;
        }
        
        // Or maybe it's a phone number
        $maybePhone = preg_replace('/[\+\*\s+]/', '', $sensitive);
        $revealFromBack = 4;
        if (is_numeric($maybePhone) && strlen($maybePhone) > $revealFromBack){
            return str_repeat('*', strlen($maybePhone) - $revealFromBack) . substr($maybePhone, -$revealFromBack);
        }

        // otherwise, dunno
        return $sensitive;
    }


    /**
     * Creates 2 cursors (before/after) based on timestamp. 
     * 
     * @param  \Illuminate\Http\Database\Model  The last record in the set
     * @param  \Illuminate\Http\Database\Model  The first record in the set
     * @param  string  The field name of the column containing the ID. Defaults to `id`
     * @return  string[]  Cursor strings for before and after
     */
    public static function encodeCursors($lastRecord, $firstRecord, $idColumnPath = 'id')
    {
        // before = $dishes->last()->created_at
        // after = $dishes->first()->created_at
        
        if (is_string($idColumnPath) && strpos($idColumnPath, '.') !== false){
            $idColumnPath = explode('.', $idColumnPath);
        }
        
        if (is_array($idColumnPath)){
            $responseCursorBeforeId = $lastRecord;
            $responseCursorAfterId = $firstRecord;
            foreach ($idColumnPath as $part){
                $responseCursorBeforeId = $responseCursorBeforeId->$part;
                $responseCursorAfterId = $responseCursorAfterId->$part;                
            }
        }
        else {
            $responseCursorBeforeId = $lastRecord->$idColumnPath;
            $responseCursorAfterId = $firstRecord->$idColumnPath;
        }
        
        $responseCursorBefore = strtotime($lastRecord->created_at) . self::$cursorSeparator . $responseCursorBeforeId;
        $responseCursorAfter = strtotime($firstRecord->created_at) . self::$cursorSeparator . $responseCursorAfterId;
        return [$responseCursorBefore, $responseCursorAfter];
    }
    
    /**
     * Returns a representation of a cursor, broken down into date, id, etc. If two cursors
     * are provided, then the first one takes precedence if valid.
     * 
     * @param string $cursor1  optional Cursor representing after
     * @param string $cursor2  optional Cursor representing before
     * @return StdClass  Object representation of the prioritised cursor
     */
    public static function parseCursors($cursor1 = false, $cursor2 = false)
    {
        if (!empty($cursor1) && strpos($cursor1, self::$cursorSeparator) !== false){
            list($timestamp, $recordId) = explode(self::$cursorSeparator, $cursor1);
            $date = date(\DateTime::ISO8601, intval($timestamp)); 
            $recordId = intval($recordId);
            return ( object) ['type' => 'after', 'dir' => 1, 'comp' => '>', 'datetime' => $date, 'recordId' => $recordId];
        }
        elseif (!empty($cursor2) && strpos($cursor2, self::$cursorSeparator) !== false){
            list($timestamp, $recordId) = explode(self::$cursorSeparator, $cursor2);
            $date = date(\DateTime::ISO8601, intval($timestamp)); 
            $recordId = intval($recordId);
            return ( object) ['type' => 'before', 'dir' => -1, 'comp' => '<', 'datetime' => $date, 'recordId' => $recordId];
        }
        else {
            return null;
        }
    }
    
    /**
     * Converts kJ to calories
     * 
     * @param mixed $kj Kilojoules 
     * @return int 
     */
    public static function kjToCalories($kj)
    {
        return floatval($kj) * 239.006;
    }
    
    /**
     * Converts kJ to calories
     * 
     * @param mixed $kj Kilojoules 
     * @return int 
     */
    public static function caloriesToKj($calories)
    {
        return floatval($calories) * 0.004184;
    }
    
    
}