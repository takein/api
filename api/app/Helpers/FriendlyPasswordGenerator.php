<?php

namespace App\Helpers;

use DB;
use Exception;
use App\Models\User;
use App\Exceptions\ApiException;

class FriendlyPasswordGenerator
{
    
    private static $instance;
    
    /**
     * Singleton initializer
     *
     * @returns  FriendlyPasswordGenerator  $instance
     */
    public static function init()
    {
        if (null === static::$instance){
            static::$instance = new static();
        }
        return static::$instance;
    }
        
    /**
     * Password strength tester
     *
     * @param  string  $password
     * @returns  bool  Validation result
     */
    public static function PasswordStrength($plainPassword, array $conditions = ['numbers', 'letters', 'symbols'])
    {
        
        return strlen($plainPassword) >= 6;         // this was just an arbitrary limit
        return true;            // pass all for now:w
        
        $hasUpper = preg_match('/[A-Z]/', $plainPassword);
        $hasNumeric = preg_match('/[0-9]/', $plainPassword);   // || in_array('numbers', $conditions);
        $hasSymbol = preg_match('/[!@#\$%\^&\*\(\)<>\.\?\\{\}\:]/', $plainPassword);
        return ($hasNumeric && $hasSymbol);
        
        // $v = Validator::make([$plainPassword], [ 'password' => implode('|', $conditions) ]);
        // return $v->passes();
    }
    
    
    
    
    private $words;
    
    private function __clone(){}
    private function __wakeup(){}
    private function __construct()
    {
        // Todo: connect to a database and retrieve these words or 
        // set it up for a lazy load
        
        // Load the default word set
        $this->words = $this->wordProvider();
    }
    
    
    public function category($categoryName)
    {
        $this->words = $this->wordProvider($categoryName);
        return $this;
    }
    
    public function generate()
    {
        // Selects a word and adds 2-4 digits to the end of it
        $digits = '';
        for ($i = 0; $i < mt_rand(2,4); $i++){ $digits .= mt_rand(1,9); }
        return $this->words[array_rand($this->words)] . $digits;
    }
    
    /**
     * Provides a list of words matching the category
     *
     * @param string  Category
     * @return array  List of terms for the category
     */
    private function wordProvider($category = '')
    {
        // Return some longer-named or unusual items in category
        switch (strtolower($category)){
            case 'animals':
                return ['Squirrel', 'Butterfly', 'Buzzard', 'Cuttlefish', 'Penguin', 'Marmoset', 'Scorpion', 'Tamarin', 'Macaque', 'Capuchin', 'Pademelon', 'Giraffe', 'Tortoise', 'Elephant', 'Chincilla', 'Dolphin', 'Buffalo', 'Spaniel', 'Hedgehog', 'Spoonbill'];
            case 'cities':
                return ['Amsterdam', 'Budapest', 'Canberra', 'Chicago', 'Geneva', 'Havana', 'Jakarta', 'Kathmandu', 'Melbourne', 'Santiago', 'Shanghai', 'Stockholm', 'Sydney', 'Toronto', 'Washington'];
            case 'dessert':
            case 'food':
            case 'meals':
            default: 
                return ['IceCream', 'Gelato', 'Cheesecake', 'Marshmallow', 'Shortcake', 'Chocolate', 'Caramel', 'RockyRoad', 'Meringue', 'Nutella', 'Gingerbread', 'Pavlova', 'Brownie', 'Macaroon'];
        }
    }
}