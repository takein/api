<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class ApiMetadataController extends BaseController
{
    /** 
     * Displays facts about this API such as version, environment, etc
     * 
     * @return Response  JSON response
     */
    public function cover(Request $request)
    {
        $facts = [
            'authenticated' => !empty(app('request')->user()), 
            'available' => true,
            'version' => 'v1',
            'routes_url' => $request->root() . '/api/v1/routes',
        ];
        return response()->json($facts);
    }
    
    /** 
     * Displays a HTML view containing a table of registered routes
     * 
     * @return Response  HTML view
     */
    public function routes()
    {
        $env = env('APP_ENV', 'Unknown env');
        $listedRoutes = shell_exec('php ../artisan route:list');
        $lastTokens = shell_exec('php ../artisan db:tokens');
        return view('meta', ['env' => $env, 'listedRoutes' => $listedRoutes, 'lastTokens' => $lastTokens]);  
    }
    
}
