<?php

namespace App\Http\Controllers\Auth;

use DB;
use Exception;
use Illuminate\Http\Request;
use App\Exceptions\ApiException;
use App\Helpers\Formatter;
use App\Helpers\FriendlyPasswordGenerator as PassGen;
use App\Http\Controllers\TakeInController as BaseController;
use App\Http\Controllers\Traits\ManagesTempTokensTrait;
use App\Jobs\SendActivation;
use App\Jobs\SendActivationSuccess;
use App\Jobs\SendPasswordReset;
use App\Models\SystemTempToken;
use App\Models\UserHighDetail;
use App\Models\User;
use App\Models\Chef;

class AccountController extends BaseController
{
    use ManagesTempTokensTrait;  
    
    protected $salt;
    
    public function __construct()
    {
        $this->salt = config('takeIn.api.apiTokenSalt');
        $this->massFillableFields = config('takeIn.user.massFillableFields');
    }
    
    /**
     * Quickly determine the validity of a username or user email before 
     * attempting a login operation
     * 
     * @return JsonResponse
     */
    public function handshake(Request $request)
    {
        $currentUser = $request->user();
        
        // If it's an email address, resolve it into the username
        $expected = $request->query('name', '');
        
        // If authenticated user, return the userID as well. 
        // Otherwise just the username
        if (!empty($currentUser)){      // authed
            list($validatedId, $validatedUsername) = $this->getUsernameByEmail($expected, true) ?: $this->validateUsername($expected, true);
            if (!empty($validatedId)){
                return response()->json([
                    'user_id' => $validatedId,
                    'username' => $validatedUsername
                ]); 
            }
        }
        else {      // anon
            $validated = $this->getUsernameByEmail($expected) ?: $this->validateUsername($expected);
            if (!empty($validated)){
                return response()->json(['username' => $validated]); 
            }
        }
        
        return $this->errorResponse(new ApiException('Not a valid user', 4000), 404);
    }
    
    /**
     * Create a user who's just self-signed up
     * 
     * Todo: queue out the email and sms
     * 
     * @return JsonResponse
     */ 
    public function signUp(Request $request)
    {    
        if ($request->has('user')){
            try {
                
                $token = null;
                $manualCode = null;
                
                $userData = $request->input('user');
                $this->clean($userData);
                $this->formatPersonalDetails($userData);
                
                $dispatchedNotifications = [];
                $emailTo = $dispatchedNotifications['email_to'] = $request->input('user.email', null);
                $smsTo = $dispatchedNotifications['sms_to'] = $request->input('user.mobile', null);
                $dispatchedNotifications['email_sent'] = !is_null($emailTo);                
                $dispatchedNotifications['sms_sent'] = !is_null($smsTo);
                
                $userId = DB::transaction(function () use ($userData, $dispatchedNotifications) {
                    
                    // Note: password will not be inserted.
                    $user = User::create($userData);
                    $userId = $user->id;
                    
                    // Force a password reset
                    $user->force_password_set = 1;
                    $user->save();
                    
                    // Check if we are universally creating a chef for this env/configuration
                    if (config('takeIn.account.createChefAlongsideUser', false) == true){
                        $chef = new Chef();
                        $user->chef()->save($chef);
                    }
                    
                    // Email confirmation with a password reset or something
                    // Set the password to some gibberish
                    // Also by default, set their account to be deleted_at i.e. inactive
                    $unguessablePassword = app('hash')->make(str_random(60));
                    DB::table('users')->where('id', $userId)->update([
                        'password' => $unguessablePassword, 
                        'deleted_at' => date(DATETIME_FORMAT_MYSQL)
                    ]);
                    
                    // Create a temp token entry -- give them 7 days to activate it
                    list($tempToken, $longCode, $manualCode) = $this->generateTempToken($userId, 24*7, true);

                    $emailWillSend = $dispatchedNotifications['email_sent'];
                    $smsWillSend = $dispatchedNotifications['sms_sent'];
                    
                    $this->dispatch(new SendActivation($tempToken, [ 'sms' => $smsWillSend, 'email' => $emailWillSend ]));

                    return $userId;     // end transaction
                });

                return response()->json(array_merge([
                    'user_id' => $userId,
                ], $dispatchedNotifications), 201);
                
            } catch (Exception $ex){
                
                if (strpos($ex->getMessage(), 'users_username_unique') !== false){
                    // Catch a database foreign key exception
                    return $this->errorResponse(new ApiException('Cannot signup: Username has already been used', 4110), 400);
                }
                if (strpos($ex->getMessage(), 'users_email_unique') !== false){
                    // Catch a database foreign key exception
                    return $this->errorResponse(new ApiException('Cannot signup: Email has already been used', 4115), 400);
                }
                return $this->errorResponse(new ApiException('API could not complete your request', 4000, $request, $ex), 500);
                
            }
        }
        
        return $this->errorResponse(new ApiException('Invalid request', 4000, $request), 400);
    }
    
    
    /**
     * Password forgot (first stage of reset request)
     * 
     * They must correctly know both their username or email address, and mobile number
     * 
     * @return JsonResponse
     */ 
    public function forgotPassword(Request $request)
    {
        // Check the username is OK
        $username = $request->input('username', '');
        $user = DB::table('users')->where('username', $username)->first() ?: DB::table('users')->where('email', $username)->first();        // we want to get their email and mobile, too
        if (empty($user)){
            return $this->errorResponse(new ApiException('Invalid request', 4121, $request), 400);
        };
        
        // Check mobile is OK: match mobile with user's
        $mobileIn = Formatter::MobilePhone($request->input('mobile', ''));
        if ($mobileIn != $user->mobile){
            // Don't giveaway that the username exists
            return $this->errorResponse(new ApiException('Invalid request', 4122, $request), 400);
        }
        
        try {

            // Create a temp token entry -- give them 3 hours
            $userId = $user->id;
            list($tempToken, $longCode, $manualCode) = $this->generateTempToken($userId, 3);
            
            // Force the user to reset their password straight away
            DB::table('users')->where('id', $userId)->update(['force_password_set' => 1]);

            $dispatchedNotifications = [];
            
            $emailWillSend = $dispatchedNotifications['email_sent'] = !empty($user->email);
            if ($emailWillSend){
                $dispatchedNotifications['email_to'] = Formatter::MaskData($user->email);       // masked
            }
            
            $smsWillSend = $dispatchedNotifications['sms_sent'] = !empty($user->mobile);
            if ($smsWillSend){
                $dispatchedNotifications['sms_to'] = Formatter::MaskData($user->mobile);        // masked
            }
            
            $this->dispatch(new SendPasswordReset($tempToken, [ 'sms' => $smsWillSend, 'email' => $emailWillSend ]));
        
            return response()->json(array_merge([
                'user_id' => $userId,
            ], $dispatchedNotifications));

        } catch (Exception $ex){
            
            return $this->errorResponse(new ApiException('API could not complete your request', 4000, $request, $ex), 500);
            
        }
        
        return $this->errorResponse(new ApiException('Invalid request', 4120, $request), 400);
    }
    
    
    /**
     * Resend password forgot SMS, send the manual code only
     * If it's within expiry, bump the expiry and send the code out again
     * 
     * @param Request request
     * @return Response as json
     */ 
    public function resendCode(Request $request)
    {
        // Check the username is OK
        $username = $request->input('username', '');
        $user = DB::table('users')->where('username', $username)->first() ?: DB::table('users')->where('email', $username)->first();        // we want to get their email and mobile, too
        if (empty($user)){
            return $this->errorResponse(new ApiException('Invalid request', 4121, $request), 400);
        };
        
        try {
            
            $userId = $user->id;
            $maskedEmail = Formatter::MaskData($user->email);
            $maskedMobile = Formatter::MaskData($user->mobile);
            
            list($tokenRecordId, $token, $manualCode) = $this->renewTempToken($userId);

            $tempToken = SystemTempToken::with('user')->findOrFail($tokenRecordId);
            $dispatchedNotifications = [];

            $smsWillSend = $dispatchedNotifications['sms_sent'] = !empty($user->mobile);
            if ($smsWillSend){
                $dispatchedNotifications['sms_to'] = Formatter::MaskData($user->mobile);        // masked
            }
            
            if ($this->checkTokenPurposeActivation($tempToken)){
                $this->dispatch(new SendActivation($tempToken, [ 'sms' => $smsWillSend, 'email' => false ]));
            }
            else {
                $this->dispatch(new SendPasswordReset($tempToken, [ 'sms' => $smsWillSend, 'email' => false ]));
            }
            
            return response()->json(array_merge([
                'user_id' => $userId,
            ], $dispatchedNotifications));

        } catch (ApiException $aex) {
           
            return $this->errorResponse($aex, 500);
            
        } catch (Exception $ex){
            
            return $this->errorResponse(new ApiException('API could not complete your request', 4000, $request, $ex), 500);
            
        }
        
        return $this->errorResponse(new ApiException('Invalid request', 4120, $request), 400);
    }
    
    
    /**
     * Activate or re-activate a user using a manual code
     * 
     * Payload needs to contain an "activation code"
     * Will change the `deleted_at` flag to null 
     * 
     * @return JsonResponse
     */ 
    public function processCode(Request $request)
    {
        try {
            
            $manualCode = $request->input('activation_code', '');
            $username = $request->input('username', '');
            $userId = DB::table('users')->where('username', $username)->value('id') ?: DB::table('users')->where('email', $username)->value('id');
            
            if (empty($userId)){
                return $this->errorResponse(new ApiException('Invalid user or code', 4811), 401);
            }
            
            $matchedTempToken = $this->retrieveTempToken($manualCode, $userId);
            list($user, $passwordPlain) = $this->consumeTokenForTempPassword($matchedTempToken);

            if ($this->tokenIsForActivation($matchedTempToken)){
                // Do not give a choice. Activation should always send an email.         // TODO: Move this out into an event?
                $this->dispatch(new SendActivationSuccess($user, [ 'email' => true ]));                
            }
            
            return response()->json([
                'user_id' => $user->id,
                'temp_password' => $passwordPlain
            ]);
            
        } catch (ApiException $aex){
            
            return $this->errorResponse($aex, 500);
            
        } catch (Exception $ex){
            
            return $this->errorResponse(new ApiException('Could not activate', 4800, $request, $ex), 500);
            
        }
    }
    
    /**
     * Process an activation token then send to landing page
     * 
     * TODO: Rewrite so that it processes THEN redirects to a config()-set redirect URL
     * 
     * Query string needs to be set as ?token=XXXXXXXXXX
     * Will change the `deleted_at` flag to null, if successful
     * 
     * @return View
     */ 
    public function processLinkToken(Request $request)
    {
        // Default: Assume this is for a password change
        $noSelfServiceFallback = url('/');
        $resultDisplayUrl = config('takeIn.activation.selfServiceUrl', $noSelfServiceFallback) . '/m/passwordchanged';
        
        try {            
            // Check for query-string param which is a hash
            $longCode = base64_decode($request->query('c', ''));
            $matchedTempToken = $this->retrieveTempToken($longCode);
            list($user, $passwordPlain) = $this->consumeTokenForTempPassword($matchedTempToken);
            
            // Dynamically define the endpoint based on the purpose of the token
            if ($this->tokenIsForActivation($matchedTempToken)){
                $resultDisplayUrl = config('takeIn.activation.selfServiceUrl', $noSelfServiceFallback) . '/m/activated';
            }
            
            if (!empty($user)){
                $viewArgs = [
                    'success' => 'ok',
                    'cred' => base64_encode($user->username . ':' . $passwordPlain),
                ];
                return redirect("$resultDisplayUrl?".http_build_query($viewArgs));
            }
            else {
                return redirect("$resultDisplayUrl?success=failed&message=(UserNotFound)");
            }

        } catch (Exception $ex){
            
            return redirect("$resultDisplayUrl?success=failed&message=(Exception,{$ex->getCode()})");
            
        }
    }
    
    // Helper: return a username via blind email check
    protected function getUsernameByEmail($email, $extended = false)
    {
        // Todo: If moving to Eloquent, remember withTrashed()
        $user = DB::table('users')->where('email', $email)->whereNull('deleted_at')->first();
        if (!empty($user)){
            return $extended ? [$user->id, $user->username] : $user->username;
        }
    }
    
    // Helper: return a username if that username exists
    protected function validateUsername($username, $extended = false)
    {
        $user = DB::table('users')->where('username', $username)->whereNull('deleted_at')->first();
        if (!empty($user)){
            return $extended ? [$user->id, $user->username] : $user->username;
        }
    }
    
}
