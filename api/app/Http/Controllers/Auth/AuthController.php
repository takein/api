<?php

namespace App\Http\Controllers\Auth;

use DB;
use Exception;
use Illuminate\Http\Request;
use App\Exceptions\ApiException;
use App\Helpers\BasicAuthenticator;
use App\Http\Controllers\Traits\ManagesApiTokensTrait;
use App\Http\Controllers\TakeInController as BaseController;
use App\Models\User;

class AuthController extends BaseController
{
    use ManagesApiTokensTrait;
    
    protected $salt;
    
    public function __construct()
    {
        $this->salt = config('takeIn.api.apiTokenSalt');
        $this->massCreateableFields = config('takeIn.user.massCreateableFields');
    }
    
    /**
     * Authenticates a user and returns an API token if valid
     * 
     * @return JsonResponse
     */
    public function login(Request $request, BasicAuthenticator $basicAuth)
    {
        try {
            
            // Find a user matching the username using basic auth. If return is
            // false then Basic Auth failed to match the username
            if (! app('BasicAuthenticator')->onceBasic()){
               return $this->errorResponse(new ApiException('Authentication failed', 2100), 401);
            }
            
            // Retrieve the associated user 
            $username = $request->getUser();
            // $user = DB::table('users')->where('username', $username)->first();
            $user = User::where('username', $username)->firstOrFail();
            
            
            // Check if this user already has a valid API token
            // If so, return that one, otherwise make one
            $token = $this->retrieveApiToken($user->id) ?: $this->generateApiToken($user->id);
            // $token = $this->retrieveApiToken($user->id) ?: 'xxxxxxxxxxx';
            return response()->json([
                'api_token' => $token->token,
                'expires_at' => $token->expires_at,
                'disabled' => ($user->deleted_at != null),
                'needs_reset' => $user->force_password_set,
            ]);
            
        } catch (Exception $ex) {
            
            return $this->errorResponse(new ApiException('API could not complete your request', 2000, $request, $ex), 500);
            
        }
    }
    
    /**
     * Wrapper to revoke the current API token
     * 
     * @return JsonResponse
     */ 
    public function logout(Request $request)
    {
        try {
            
            $user = $request->user();
            $this->revokeApiToken($user->id);
            return $this->successResponse($user->id, 'logout', ['type' => 'user'], 200);
            
        } catch (Exception $ex){
            
            return $this->errorResponse(new ApiException('API could not complete your request', 2000, $request, $ex), 500);
            
        }
    }
    
}
