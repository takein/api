<?php

namespace App\Http\Controllers\Dish;

use DB;
use Exception;
use Schema;
use Illuminate\Http\Request;
use Illuminate\Database\Schema\Builder;
use App\Exceptions\ApiException;
use App\Helpers\Formatter;
use App\Http\Controllers\TakeInController as BaseController;
use App\Http\Controllers\Traits\ChangesNewsfeedStrategies;
use App\Http\Controllers\Traits\PaginatesResults;
use App\Models\Dish;
use App\Models\Listing;
use App\Models\Review;

class DishController extends BaseController
{
    use ChangesNewsfeedStrategies, PaginatesResults;
    
    public function __construct()
    {
        parent::__construct();
        
        $this->defaultResultLimit = intval(config('takeIn.dish.defaultResultLimit'));
    }
    
    /**
     *  Show all dishes in a News feed, with pagination
     *
     *  Select a strategy to build the list - could be:
     *  1. Most popular
     *  2. Most recent
     *  3. Closest
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return Response
     */
    public function index(Request $request)
    {
        // Was there a specific max limit requested?
        $limit = $this->getPaginationLimit($request);
                            
        // Set a strategy lad
        $dishStrategyQuery = $this->indexMostRecent($limit);          // "Most recent" strategy
        
        try {
            
            // Filter by request cursor 
            // note: cursorBefore and cursorAfter are mutually exclusive. affinity is towards cursorAfter.
            $requestCursorInfo = Formatter::parseCursors($request->input('after', false), $request->input('before', false));
            
            if (!empty($requestCursorInfo)){ 
                $dishList = $dishStrategyQuery
                                    ->where('created_at', $requestCursorInfo->comp, $requestCursorInfo->datetime)
                                    ->where('id', $requestCursorInfo->dir < 0 ? '<' : '<', $requestCursorInfo->recordId)
                                    ->orderBy('created_at', $requestCursorInfo->dir < 0 ? 'desc' : 'asc')->get();
                
                // Check if needs to appear in reverse chrono order
                if ($requestCursorInfo->dir > 0){
                    $dishList = $dishList->reverse();       
                }
            }
            else {
                $dishList = $dishStrategyQuery->orderBy('created_at', 'desc')->get();
            }

            $resultSetSize = $dishList->count();
            if ($resultSetSize > 0){
                list($responseCursorBefore, $responseCursorAfter) = Formatter::encodeCursors($dishList->last(), $dishList->first());
            }
            else {
                $appErrorCode = empty($requestCursorInfo) ? 3410 : 3412;        // Check whether was the result of cursor search
                return $this->errorResponse(new ApiException('Dishes not found', $appErrorCode), 404);
            }
            
        } catch (Exception $ex){
            
            return $this->errorResponse(new ApiException('Incorrect cursor format', 3000, $request, $ex), 400);
            
        }

        $dishList->transform(function($dish, $key) use ($request) {
            
            // merge dish properties into the base listing & merge user properties into chef object
            foreach ( $dish->dish->toArray() as $key => $value){
                    $dish->$key = $value;                        
            }
            foreach ( $dish->chef->user->toArray() as $key => $value){
                $dish->chef->$key = $value;
            }
            
            // Calculate the average for the chef
            $ratings = $this->calculateChefAverageRating($dish->owner_id);
            $dish->chef->ratings = $ratings;
            
            unset($dish->dish);
            unset($dish->chef->user);
            return $dish;
        });
        
        return response()->json([
            'dishes' => $dishList,
            'meta' => [
                'total' => $resultSetSize,
            ],
            'cursor' => $this->formatPaginationCursors($request, $responseCursorBefore, $responseCursorAfter)
        ]);
    }
    
    /**
     * Show a particular dish including all nested reviews
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return Response
     */    
    public function show(Request $request, $id)
    {
        try 
        {
            $dish = Listing::with('dish', 'dish.reviews', 'chef.user')->findOrFail($id);
            
            // merge dish properties into the base listing & merge user properties into chef object
            foreach ( $dish->dish->toArray() as $key => $value){
                    $dish->$key = $value;                        
            }
            foreach ( $dish->chef->user->toArray() as $key => $value){
                $dish->chef->$key = $value;
            }
            
            $dish->reviews = collect($dish->reviews)->transform(function ($review) {
                $review = ( object ) $review;
                $r = Review::findOrFail($review->id);
                $buyer = $r->order->tx->buyer;
                $review->purchase_date = $r->order->tx->created_at;
                $review->review_date = $r->created_at;
                $review->reviewer_id = $buyer->id;
                $review->reviewer_first_name = $buyer->first_name;
                $review->reviewer_last_name = $buyer->last_name;
                unset($review->created_at);
                return $review;
            });
            
            // Calculate the average review rating for this dish
            $ratings = $this->calculateDishAverageRating($dish->id);
            $dish->ratings = $ratings;
            
            unset($dish->dish);
            unset($dish->chef->user);
            
            return response()->json([
                'dish' => $dish
            ]);
            
        } catch (Exception $ex) {
            
             return $this->errorResponse(new ApiException('Dish not found', 3000, $request, $ex), 404);  
           
        }
        
    }
    
    /**
     * Save a new dish. 
     * Calories/kj will be automatically calculated
     *
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function store(Request $request)
    {  
        $postData = $request->input('dish', []);
        
        // Todo: replace with Laravel validation
        $validationOk = true;
        $requiredKeys =['price_per_serving', 'name', 'cutoff_time'];
        foreach ($requiredKeys as $keyName){
            if (!array_key_exists($keyName, $postData)){
                $validationOk = false;
            }
        }
        // $validationOk = $validationOk && count($postData) > 0;
        
        
        if ($validationOk){
            try {

                $this->clean($postData);
                
                // Process conversions of calories<=>kilojoules
                $caloriesExists = array_key_exists('calories', $postData);
                $kjExists = array_key_exists('kilojoules', $postData);
                if ($caloriesExists && !$kjExists){
                    $postData['kilojoules'] = Formatter::caloriesToKj($postData['calories']);
                }
                elseif ($kjExists && !$caloriesExists){
                    $postData['calories'] = Formatter::kjToCalories($postData['kilojoules']);
                }
                
                // Bind the current user as the owner_id
                // Therefore by design overwrites any incoming payload owner_id
                $ownerId = $request->user()->id;
                
                $listingId = DB::transaction(function () use ($postData, $ownerId) {
                    
                    $listing = new Listing();
                    $listing->fill(array_only($postData, Schema::getColumnListing('listings')));
                    $listing->owner_id = $ownerId;
                    $listing->save();
                    
                    // $dish = Dish::create(array_only($postData, Schema::getColumnListing('dishes')));     // PK is listing_id 
                    $dish = new Dish();
                    $dish->fill(array_only($postData, Schema::getColumnListing('dishes')));
                    $dish->listing_id = $listing->id;
                    $dish->price_per_serving = $postData['price_per_serving'];      // price-per-serving cannot be changed after creation
                    $dish->servings_remaining = $postData['servings_total'];        // Default set servings_remaining to servings_total
                    $dish->save();
                    
                    return $listing->id;
                    
                });
                        
                return $this->successResponse($listingId, 'created', ['type' => 'listing'], 201);

            } catch (Exception $ex) {
                
                return $this->errorResponse(new ApiException('Could not save dish', 3000, $request, $ex), 500);  
                
            }
        }
        return $this->errorResponse(new ApiException('Missing post data', 3000), 400);
    }
    
    /**
     * Update an existing dish
     * 
     * Several operations can happen:
     * 
     * 1. Update listing data
     * 2. Set it to archived -- TODO
     * 3. Set it to 'disabled' and it won't show up (so the owner can re-enable it later) -- TODO
     * 4. Set it to 'enabled'  -- TODO
     *     
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        // 0. Determine permission to perform update
        $dish = DB::table('listings')->where('id', $id)->first();
        if (!empty($dish) && $request->user()->id == $dish->owner_id){
            
            // 1. Standard data update 
            $postData = $request->input('dish', []);
            if (count($postData) > 0){
                
                // Save it
                try {
                    $this->clean($postData);
                    
                    // Gotta update both objects, use Schema::getColumnListings() to retrieve the columns
                    $listingUpdates = array_only($postData, Schema::getColumnListing('listings'));
                    $dishUpdates = array_only($postData, Schema::getColumnListing('dishes'));
                    
                    if (count($listingUpdates) > 0){
                        Listing::findOrFail($id)->update($listingUpdates);
                    }
                    
                    if (count($dishUpdates) > 0){
                        Dish::findOrFail($id)->update($dishUpdates);
                    }
                    
                    return $this->successResponse($id, 'updated', ['type' => 'listing']);
                    
                } catch (Exception $ex) {
                    
                    return $this->errorResponse(new ApiException('Something went wrong while updating', 3000, $request, $ex), 500);
                    
                }
            }
            
            // 2. Set it to archived
            // 3. Set it to disabled
            // 4. Set it to enabled
            
            return $this->errorResponse(new ApiException('Missing post data', 3000), 400);
            
        }
        
        
    }
    
    /**
     * Delete an existing dish
     * 
     * Factors precluding deletion could include:
     * 
     * + If it is currently in pickup or delivery mode
     * + It may have transactions or reviews attached to it
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return Response
     */
    public function destroy(Request $request, $id)
    {        
        try {

            Listing::findOrFail($id)->delete();
            return $this->successResponse($id, 'deleted', ['type' => 'listing'], 200);
            
        } catch (Exception $ex) {
            
            return $this->errorResponse(new ApiException('Something went wrong while deleting', 3000, $request, $ex), 500);
            
        }
    }

    
    //=====================
    // Private methods

    // Get a summary of chef ratings
    protected function calculateChefAverageRating($chefId)
    {
        $chefId = intval($chefId);
        $ratings = DB::select("CALL CalculateChefAverageRating($chefId)");
        return array_shift($ratings);
    }
    
    // Get a summary of dish ratings
    protected function calculateDishAverageRating($dishId)
    {
        $dishId = intval($dishId);
        $ratings = DB::select("CALL CalculateDishAverageRating($dishId)");
        return array_shift($ratings);
    }
    
    
}



