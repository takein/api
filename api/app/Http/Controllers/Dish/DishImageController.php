<?php

namespace App\Http\Controllers\Dish;

use DB;
use Exception;
use Illuminate\Http\Request;
use App\Exceptions\ApiException;
use App\Http\Controllers\TakeInImageController as BaseController;

class DishImageController extends BaseController
{
    private $dishImageUploadKey;
    
    public function __construct()
    {
        $this->imageUploadKeys = config('takeIn.dish.imageUploadKeys');
        parent::__construct();
    }
    
    public function index(Request $request, $dishId)
    {

        // Check if the image_url is formatted as a temp image i.e.
        // matches the scheme temp://542.jpg   
        // Where the ".jpg" extn or whatever is optional but handy if included
        
        $images = DB::table('listings_images')->select(['id', 'image_url', 'caption', 'created_at'])->where('listing_id', $dishId)->get();
        
        foreach ($images as &$image){
            $this->convertDates($image);
            
            $matches = [];
            if (preg_match('/^temp:\/\/(\d+)(?:\.[a-zA-Z]{1,4})?$/', $image->image_url, $matches)){
                $imageId = $matches[1];
                // $imageUrl = route('dishImage', ['iid' => $imageId]);
                unset($image->image_url);
                $image->url = route('dishImage', ['iid' => $imageId]);        // overwrite the image url
            } 
        }
    
        return response()->json([
            'images' => $images
        ]);
    }
    
    

    public function store(Request $request, $dishId)
    {
        try {
            
            list($imageId, $imageUrl) = DB::transaction(function () use ($request, $dishId) {

                $file = null;
                if (is_array($this->imageUploadKeys)){
                    foreach ($this->imageUploadKeys as $key){
                        if ($request->hasFile($key)){
                            $file = $request->file($key);
                            break;
                        }
                    }
                }
                else {
                    $file = $request->file($this->imageUploadKeys);
                }
                
                if (!empty($file) && $file->isValid() && $file->isReadable()){
                    
                    // Read the blob out
                    $fileObj = $file->openFile();
                    $blob = $fileObj->fread($fileObj->getSize());
                    $mimeType = $file->getMimeType();
                    $extension = $file->guessExtension();
                    
                    
                    // Are we storing it in database temp solution
                    if (env('IMAGES_IN_DATABASE', 'false') == 'true'){
                        $imageId = DB::table($this->tempImagesTable)->insertGetId([
                            'image_blob' => $blob,
                            'mime_type' => $mimeType,
                            'extension' => $extension
                        ]);
                    }
                    else {
                        throw new ApiException('No image storage method available', 1899);
                        // abort(405, 'No image storage method available');
                    }
                    
                    
                    // Construct a temp string to store into `dishes`
                    $imageUrl = "temp://$imageId.$extension";
                    
                    $imageId = DB::table('listings_images')->insertGetId([
                        'listing_id' => $dishId,
                        'image_url' => $imageUrl
                    ]);
                    
                    return [$imageId, $imageUrl];

                }
                // abort(400, 'Bad or invalid image upload');
                throw new ApiException('Bad or invalid image upload', 1210);
                
            });
            
            return $this->successResponse($dishId, 'created', ['dish_image_id' => $imageId, 'type' => 'listing_image', 'image_url' => $imageUrl], 201);
            
        } catch (ApiException $ex) {
            
            return $this->errorResponse($ex, 400);
            
        } catch (Exception $ex) {
            
            return $this->errorResponse(new ApiException('Dish image failed to save', 3200, $request, $ex), 500);
            
        }
        
    }

    public function destroy(Request $request, $imageId)
    {
        try {
            
            $imageUrl = DB::table('listings_images')->where('id', $imageId)->value('image_url');
            // $imageUrl = $listingImage->image_url;
            if (empty($imageUrl)){
                return $this->errorResponse(new ApiException('Requested image does not exist', 3200), 404);
            }
            
            $deletion = DB::transaction(function () use ($imageUrl, $imageId) {
                                
                // TEMP IMAGE PROCESSING
                if (env('IMAGES_IN_DATABASE', 'false') == 'true'){
                    if (preg_match('/^temp:\/\/(\d+)(?:\.[a-zA-Z]{1,4})?$/', $imageUrl, $matches)){
                        DB::table($this->tempImagesTable)->where('id', $matches[1])->delete();
                    } 
                }
                // END TEMP IMAGE PROCESSING

                DB::table('listings_images')->where('id', $imageId)->delete();
                
            });
            
            return $this->successResponse($imageId, 'deleted', ['type' => 'listing_image'], 200);
            
        } catch (Exception $ex) {
            
            return $this->errorResponse(new ApiException('Failed to delete dish image', 3200, $request, $ex), 500);
            
        }

    }

}
