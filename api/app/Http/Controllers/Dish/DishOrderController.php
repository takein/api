<?php

namespace App\Http\Controllers\Dish;

use DB;
use Exception;
use Schema;
use Illuminate\Http\Request;
use App\Exceptions\ApiException;
use App\Helpers\Formatter;
use App\Http\Controllers\TakeInController as BaseController;
use App\Models\Dish;
use App\Models\Order;
use App\Models\Transaction;


class DishOrderController extends BaseController
{
    
    public function __construct()
    {
        $this->txFillableFields = config('takeIn.tx.massFillableFields');
        $this->orderFillableFields = config('takeIn.order.massFillableFields');
        $this->orderNonOwnerCanSee = config('takeIn.order.nonOwnerCanSee');
        $this->txNonOwnerCanSee = config('takeIn.tx.nonOwnerCanSee');
    }
    
    /**
     * Get orders for a dish
     *
     * Amount of detail depends on whether the requester is the owner or not
     * + If owner, will see transaction details, status, quantity details, etc
     * + If not owner, will see only the purchaser
     *
     * Additionally a meta object will summarise info about the orders
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return Response
     */
    public function index(Request $request, $id)
    {
        try {
            
            $dish = Dish::findOrFail($id);
            $user = $request->user();
            $orders = Order::ofListing($dish->listing_id)->get();
            
            $isOwner = !(empty($user) || $user->id != $dish->listing->owner_id);
            if ($isOwner){
                // For owners
                $orders->transform(function ($order, $key) {
                    // $this->convertDates($order);
                    //$order->tx = $order->tx;        // seems counterintuitive, but this overwrites the object magic
                    $order->id = $order->tx_id;
                    unset($order->tx_id);               // discard unnice names
                    unset($order->reverse_tx_id);   // discard unnice names
                    return $order;
                });
            }
            else {
                // Non-owners
                $orders->transform(function ($order, $key) {
                    $tx = ( object ) array_only($order->tx->toArray(), $this->txNonOwnerCanSee);
                    // $this->convertDates($tx);
                    return ( object ) [
                        'id' => $order->tx_id,
                        'tx' => $tx
                    ];
                });
            }
            
            if (count($orders) > 0){
                return response()->json([
                    'orders' => $orders,
                    'meta' => [
                        'total' => count($orders),
                        'is_owner' => $isOwner
                    ]
                ]);                
            }
            return $this->errorResponse(new ApiException('No orders found for this dish', 3500), 404);
            
        } catch (Exception $ex) {
            
            $httpCode = $ex->getCode() == 404 ? 404 : 500;
            return $this->errorResponse(new ApiException('Could not get orders for this dish', 3500, $request, $ex), $httpCode);
            
        }
    }
    
    
    /**
     * For the requested dish, create an order with an underlying transaction
     *
     * Order object in Request must contain
     * + Number of servings ordered
     * + Special notes to seller
     * + Pickup or delivery preference 
     * + Time to pickup or delivery
     * + Delivery address
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return Response
     */
    public function store(Request $request, $id)
    {
        $postData = $request->input('order', []);
        if (count($postData) > 0){
            try {
                
                $this->clean($postData);
                $buyerId = $request->user()->id;
                $postData['buyer_id'] = $buyerId;

                // Perform a check to see, right now, if there are servings available
                $txId = DB::transaction(function () use ($id, $buyerId, $postData) {
                    
                    $dish = Dish::where('listing_id', $id)->sharedLock()->first();
                    
                    // Check/enforce seller cannot be buyer
                    if ($buyerId == $dish->listing->owner_id){
                        throw new ApiException('The buyer cannot be the seller', 3520);
                    }
                    
                    // Check there's enough servings to proceed
                    $remainingServings = $dish->servings_remaining;
                    if ($remainingServings - intval($postData['servings_purchased']) < 0){
                        throw new ApiException('Not enough dish servings available', 3510);
                    }
                    
                    $ts = date(DATETIME_FORMAT_MYSQL);
                    
                    //============================================
                    // Todo: Perform the acquisition of money etc.
                    //============================================

                    // $txFields = array_only($postData, Schema::getColumnListing('transactions'));   // not used... todo delete
                    $orderFields = array_only($postData, Schema::getColumnListing('orders'));

                    // Create a transaction record
                    $tx = new Transaction();
                    $tx->buyer_id = $buyerId;
                    $tx->seller_id = $dish->listing->owner_id;
                    $tx->subtotal_cost = floatval($dish->price_per_serving) * intval($orderFields['servings_purchased']);
                    if (stripos($orderFields['pickup_delivery'], 'd') !== false){
                        $tx->delivery_cost = $dish->delivery_cost;
                    }
                    $tx->save();
    
                    // Store metadata in an order record
                  
                    //$orderFields['listing_id'] = $dish->listing_id;
                    $order = new Order();
                    $order->listing_id = $dish->listing_id;
                    $order->fill($orderFields);
                    $tx->order()->save($order);
                    
                    // Reduce the amount of "servings_remaining" in dish table
                    $dish->servings_remaining = $remainingServings - intval($postData['servings_purchased']);
                    $dish->save();
                    
                    return $tx->id;
                });
                        
                return $this->successResponse($txId, 'created', ['type' => 'order'], 201);                    
                
            } catch (ApiException $aex) {
                
                return $this->errorResponse($aex, 500); 
                
            } catch (Exception $ex) {
                
                // return $this->errorResponse(new ApiException('Not enough dish servings available', 3510, $request, $ex), 500);
                return $this->errorResponse(new ApiException('Order could not be saved', 3500, $request, $ex), 500);
                
            }
        }
        
        return $this->errorResponse(new ApiException('Order could not be saved', 3500), 400);
    }
    
}
