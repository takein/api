<?php

namespace App\Http\Controllers\Order;

use DB;
use Exception;
use Illuminate\Http\Request;
use App\Exceptions\ApiException;
use App\Http\Controllers\TakeInController as BaseController;
use App\Models\Order;
use App\Models\Transaction;

class OrderController extends BaseController
{
    
    public function __construct()
    {
        $this->txFillableFields = config('takeIn.tx.massFillableFields');
    }

    /**
     * Lookup an order
     * 
     * @param \Illuminate\Http\Request $request
     * @param $id  Order ID
     * @return Response
     */
    public function show(Request $request, $id)
    {
        try {
            
            $order = Order::with('tx')->where('tx_id', $id)->firstOrFail();
            $order->id = $order->tx_id;     // for presentation purposes. Better way to do this?
            unset($order->tx_id);

            return response()->json([
                'order' => $order
            ]);
 
        } catch (Exception $ex){
            
            return $this->errorResponse(new ApiException('Could not retrieve this order', 5300, $request, $ex), 500);
            
        } 
    }
    
    
    /**
     * Approve an order. The only thing you can update is approval_status.
     * 
     * @param \Illuminate\Http\Request $request
     * @param $id  Order ID
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try {
            
            $order = Order::findOrFail($id);
            $order->approval_status = $request->input('order.approval_status', null);
            $order->save();

            return $this->successResponse($id, 'updated', ['type' => 'order']);
 
        } catch (Exception $ex){
            
            return $this->errorResponse(new ApiException('Could not retrieve this order', 5300, $request, $ex), 500);
            
        } 
    }
 
 
    /**
     * Undo an order, to perform a refund
     * 
     * A new (negative) transaction record is generated. The order is updated with reverse tx info.
     * The `onlyReverse` param array allows for only parts of the transaction to be refunded
     * 
     * _Caveats_: Only one refund tx can be done for any single order. Only the seller may initiate a refund.
     * 
     * @param \Illuminate\Http\Request $request
     * @param $id  Order ID
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            
            // Assume it's a full refund (of all costs) unless `only` contains a specific list:
            $onlyReverse = $request->input('only', ['subtotal_cost', 'delivery_cost', 'surcharge', 'fees']);
            $order = Order::findOrFail($id);
            
            //==========================
            // Todo: Perform the refund of money etc.
            //==========================  

            DB::transaction(function () use ($order, $onlyReverse) {
                
                // Lookup the old tx record and undo it
                $reverseTx = Transaction::create([
                    'seller_id' => $order->tx->seller_id,
                    'buyer_id' => $order->tx->buyer_id,
                    'subtotal_cost' => in_array('subtotal_cost', $onlyReverse) ? -($order->tx->subtotal_cost) : 0,
                    'delivery_cost' => in_array('delivery_cost', $onlyReverse) ? -($order->tx->delivery_cost): 0,
                    'surcharge' => in_array('surcharge', $onlyReverse) ? -($order->tx->surcharge): 0,
                    'fees' => in_array('fees', $onlyReverse) ? -($order->tx->fees): 0,
                ]);
            
                // add this transaction to the original Order record
                $order->reverse_tx_id = $reverseTx->id;
                $order->save();
                
                $order->delete();       // should be a soft delete
                
            });
            
            return $this->successResponse($id, 'reversed', ['type' => 'order']);
            
        } catch (Exception $ex) {
            
            return $this->errorResponse(new ApiException('Something went wrong while reversing this transaction', 5200, $request, $ex), 500);
            
        }        
    }

}
