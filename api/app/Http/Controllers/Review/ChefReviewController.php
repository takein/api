<?php

namespace App\Http\Controllers\Review;

use DB;
use Exception;
use Illuminate\Http\Request;
use App\Exceptions\ApiException;
use App\Http\Controllers\TakeInController as BaseController;
use App\Models\Chef;

class ChefReviewController extends BaseController
{
    /**
     * Show all reviews for a chef
     * 
     * TODO: Pagination
     *
     * @return JsonResponse
     */
    public function index(Request $request, $chefId)
    {
        try {
            
            // Todo: link chef to reviews to orders to transactions to sellers/buyers
            
            // $chef = Chef::findOrFail($chefId);
            // $reviews = $chef->reviews;
            
            $reviews = DB::table('reviews as r')
                ->select([
                    'r.id', 
                    'd.id as dish_id', 
                    'd.name as dish_name', 
                    'r.value_score', 
                    'r.value_score', 
                    'r.taste_score', 
                    'r.service_score', 
                    'r.comment', 
                    't.created_at as purchase_date',
                    'r.created_at as review_date',
                    't.buyer_id as reviewer_id', 
                    'u.first_name as reviewer_first_name', 
                    'u.last_name as reviewer_last_name',
                ])
                ->join('orders as o', 'o.tx_id', '=', 'r.order_id')
                ->join('transactions as t', 't.id', '=', 'o.tx_id')
                ->join('users as u', 'u.id', '=', 't.buyer_id')
                ->join('listings as d', 'd.id', '=', 'o.listing_id')
                ->where('t.seller_id', $chefId)
                ->orderBy('r.created_at', 'desc')
                ->get();
            
            if (count($reviews) == 0){
                return $this->errorResponse(new ApiException('Reviews not found', 3402), 404);
            }
            
            $this->convertDatesInArray($reviews);
            
            return response()->json([
                'reviews' => $reviews
            ]);
            
        } catch (Exception $ex) {
            
            return $this->errorResponse(new ApiException('Could not retrieve reviews', 4000, $request, $ex), $ex->getCode() == 404 ? 404 : 500);
            
        }
    }
    
}


