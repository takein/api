<?php

namespace App\Http\Controllers\Review;

use DB;
use Exception;
use Illuminate\Http\Request;
use App\Exceptions\ApiException;
use App\Http\Controllers\TakeInController as BaseController;

class ReviewController extends BaseController
{
    /**
     *  Show all reviews full stop
     *
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        return $this->errorResponse(new ApiException('Not implemented yet', 1000), 500);        
    }
    
    /**
     * Show a particular review
     *
     * @return JsonResponse
     */    
    public function show(Request $request, $id)
    {
        return $this->errorResponse(new ApiException('Not implemented yet', 1000), 500);
    }
    
    /**
     * Save a new review
     * 
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        return $this->errorResponse(new ApiException('Not implemented yet', 1000), 500);
    }
    
    /**
     * Update an existing review
     * 
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        return $this->errorResponse(new ApiException('Not implemented yet', 1000), 500);
    }
    
    /**
     * Delete an existing review
     *
     * @return JsonResponse
     */
    public function destroy(Request $request, $id)
    {        
        return $this->errorResponse(new ApiException('Not implemented yet', 1000), 500);
    }
    
}


