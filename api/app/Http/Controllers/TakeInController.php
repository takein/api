<?php

namespace App\Http\Controllers;

use File;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Exceptions\ApiException;
use App\Helpers\Formatter;
use App\Http\Controllers\Traits\CleansPostDataTrait;
use App\Http\Controllers\Traits\FormatsResponsesTrait;

abstract class TakeInController extends BaseController
{  
    use CleansPostDataTrait, FormatsResponsesTrait;

    protected $massFillableFields;
    
    protected $apiEndpointDomain;           // Domain name, with https
    protected $apiEndpointPrefix;             // The prefix e.g. /api/v1
    protected $apiEndpointBaseUrl;          // The complete base url
    
    public function __construct()
    {
        $this->apiEndpointPrefix = 'api/v1/';
        $this->apiEndpointDomain = config('app.url');
        $this->apiEndpointBaseUrl = $this->apiEndpointDomain . '/' . $this->apiEndpointPrefix;
    }
    
}
