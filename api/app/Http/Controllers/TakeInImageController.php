<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Exceptions\ApiException;

abstract class TakeInImageController extends BaseController
{
    use Traits\ConvertsDatesTrait, Traits\FormatsResponsesTrait;

    abstract public function index(Request $request, $parentId); 
    abstract public function store(Request $request, $parentId);
    abstract public function destroy(Request $request, $parentId);
    
    protected $tempImagesTable = 'temp_images';
    
    public function __construct()
    {
        
    }
    
    /**
     * Temporary: display the actual image resource
     * This is a stop-gap until Amazon S3 integration, at which point we can switch out this endpoint
     * for a proper S3 URL
     */
    public function displayDatabaseImage($imageId){
        $image = DB::table($this->tempImagesTable)->where('id', $imageId)->first();
                
        if (! empty($image)){
            return response($image->image_blob, 200)->header('Content-Type', $image->mime_type);
        }
        
        // Todo: return a default image?
        // Or just return a 404?
        
        return response('', 404);
    }

}
