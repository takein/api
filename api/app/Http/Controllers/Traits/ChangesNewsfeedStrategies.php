<?php

namespace App\Http\Controllers\Traits;

use App\Models\Listing;
use App\Models\Dish;

trait ChangesNewsfeedStrategies
{    
    //=====================
    // News feed Strategies
    //=====================
    
    /**
     * Most recent strategy
     * 
     * retrieves dishes based primarily on the dish's
     * + time created, and
     * + weighted popularity ie pageviews and likes -- (TODO)
     * + rough proximity -- (TODO)
     * 
     * @return QueryBuilder
     *
     */
    protected function indexMostRecent($limit = 1)
    {
        return Listing::with('dish', 'chef.user')->take($limit);
    }
    
    /**
     * Most popular
     * 
     * retrieves dishes based primarily on the dish's
     * + weighted popularity ie pageviews and likes -- (TODO)
     * + explicit preferences for the user -- (TODO)
     * + analysed preferences for the user -- (TODO)
     * + rough proximity -- (TODO)
     * 
     * @return QueryBuilder
     *
     */
    protected function indexMostPopular($limit = 1)
    {
        return Listing::with('dish', 'chef.user')->take($limit);
        // return DB::table('listings as l')
                    // ->join('dishes', 'l.id', '=', 'dishes.listing_id')
                    // ->take($limit)
                    // ->whereNull('deleted_at'); 
    }    
    
    
}