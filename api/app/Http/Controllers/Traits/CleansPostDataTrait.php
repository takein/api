<?php

namespace App\Http\Controllers\Traits;

use App\Helpers\Formatter;

trait CleansPostDataTrait 
{
     
    /**
     * Goes through a postData array byref and kills off keys that 
     * aren't whitelisted in $keepKeys
     *
     * @param array  Incoming post data
     * @param array  Post data keys which you want to keep
     * @return null
     */
    protected function clean(array &$postData, array $keepKeys = [])
    {
        // Keep only specified keys, if those were provided
        if (count($keepKeys) > 0){
            $postData = array_only($postData, $keepKeys);
        }

        // If any values are null, remove them
        $postData = array_where($postData, function ($key, $value) {
            return !is_null($value);
        });
        
        // Trim all strings
        foreach ($postData as $key => &$value){
            
            // If someone's trying to upload an object or array, JSONify it then turn it into a string.
            if (is_array($value) || is_object($value)){
                $value = json_encode(( array ) $value);
            }
            
            if (is_string($value)){
                $value = trim($value);
            }
            
        }
        
        // foreach ($postData as $key => $value){
        //     if (! in_array($key, $keepKeys)){
        //         if (is_object($postData)){
        //             unset($postData->$key);     // bye
        //         }
        //         else {
        //             unset($postData[$key]);     // bye
        //         }
        //     }
        // }
    }

    /**
     * Goes through post data that contains common personal-details fields
     *
     * @param array  Incoming post data
     * @return null
     */    
    protected function formatPersonalDetails(array &$postData)
    {
        // Fix up mobile phone numbers
        if (array_key_exists('mobile', $postData)){
            $postData['mobile'] = Formatter::MobilePhone($postData['mobile']);
        }
        
        // Todo: Enforce usernames are no spaces, etc
        if (array_key_exists('username', $postData)){
            $postData['username'] = Formatter::StripSpaces($postData['username']);
        }
        
        // Todo: Capitalize names that might have formats like   reagan-johnson => Reagan-Johnson,  o'connor => O'Connor etc
        if (array_key_exists('first_name', $postData)){
            $postData['first_name'] = ucwords($postData['first_name'], " -–—'\t\r\n\f\v");
        }
        if (array_key_exists('last_name', $postData)){
            $postData['last_name'] = ucwords($postData['last_name'], " -–—'\t\r\n\f\v");
        }
        
        // Todo: Fix email or throw exception if unexpected email format
        if (array_key_exists('email', $postData)){
            $postData['email'] = Formatter::StripSpaces($postData['email']);
            if (strpos($postData['email'], '@') === false){
                throw new ApiException('Email format appears invalid', 4116);
            }
        }
        
    }
    
}