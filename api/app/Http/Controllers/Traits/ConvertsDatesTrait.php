<?php

namespace App\Http\Controllers\Traits;

trait ConvertsDatesTrait 
{
     /**
     * For a single result, convert the MySQL default DateTimes into ISO 8601
     *
     * Todo: known bottleneck is the config() retrieval every time.
     *
     * @return null
     */    
    protected function convertDates(\StdClass &$result, $format = \DateTime::ISO8601)
    {
        $knownDateColumns = config('takeIn.data.knownDateColumns', ['date']);
        if (is_array($knownDateColumns)){
            $result = ( array ) $result;
            foreach ( $result as $key => &$value ){
                foreach ($knownDateColumns as $knownDate){
                    if (strpos($key, $knownDate) !== false && !empty($value)){
                        $value = (new \DateTime($value))->format($format);
                        break;      // prevent double-parsing
                    }
                }
            }            
            $result = ( object ) $result;
        }
    }
    
    /**
     * For result sets, convert all the MySQL default DateTimes into ISO 8601
     *
     * @return null
     */    
    protected function convertDatesInArray(array &$results, $format = \DateTime::ISO8601)
    {
        foreach ($results as &$result){
            $this->convertDates($result, $format);
        }
    }
    
    
}