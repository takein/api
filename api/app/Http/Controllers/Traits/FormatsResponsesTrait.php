<?php

namespace App\Http\Controllers\Traits;

use App\Exceptions\ApiException;

trait FormatsResponsesTrait
{
    
    /**
     * Return a JSON error object
     *
     * @return Response
     */    
    protected function errorResponse(ApiException $ex, $httpResponseCode = 200)
    {
        $debug = env('APP_DEBUG') == 'true';
        
        $errorResp = [
            'code' => $ex->getCode(),
            'message' => $ex->getMessage()
        ];
       
        if ($debug){
            if ($innerEx = $ex->getInnerException()){
                $errorResp['inner_ex'] = ( object ) [
                    'code' => $innerEx->getCode(),
                    'message' => $innerEx->getMessage(),
                    'line' => $innerEx->getLine(),
                    'file' => $innerEx->getFile(),
                    // 'trace' => $innerEx->getTraceAsString(),
                ];
            }
        }
        
        return response()->json([
            'error' => $errorResp
        ], $httpResponseCode);
    }
    
    /**
     * Return a JSON resource modification success message
     *
     * @return Response
     */    
    protected function successResponse($id, $actionResult, $extraResp = [], $httpResponseCode = 200)
    {
        $standardResp = [
            'result' => $actionResult,
            'id' => $id
        ];
        return response()->json([
            'success' => array_merge($standardResp, $extraResp)
        ], $httpResponseCode);
    }
    
}