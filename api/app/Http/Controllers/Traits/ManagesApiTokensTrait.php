<?php

namespace App\Http\Controllers\Traits;

use DB;
use Exception;
use App\Exceptions\ApiException;
use App\Models\SystemApiToken;

trait ManagesApiTokensTrait 
{
    /**
     * Helper: create an API token record entry 
     * Helper: create an API token record entry 
     * Note: expiry is measured in days here
     * 
     * @param int  User ID 
     * @param int  Expiry time days
     * @param bool  Invalidate other tokens
     * @return SystemApiToken  token object
     */ 
    protected function generateApiToken($userId, $expiryTimeDays = 60, $invalidateOtherTokens = true)
    {
        try {
            
            // return the actual token string, not the ID
            return DB::transaction(function () use ($userId, $expiryTimeDays, $invalidateOtherTokens) {   
                //Invalidate all other request tokens
                if ($invalidateOtherTokens){
                    SystemApiToken::where('user_id', $userId)->update([
                        'expires_at' => date(DATETIME_FORMAT_MYSQL),
                        'forced_expire' => 1,
                        'last_activity_at' => date(DATETIME_FORMAT_MYSQL),
                    ]);
                }
                
                $requestTime = time();      // for some randomness
                $token = app('hash')->make("$userId{$this->salt}$requestTime");
                
                $expiryDateTime = (new \DateTime())->add(new \DateInterval("P{$expiryTimeDays}D"));
                $expiryString = $expiryDateTime->format(DATETIME_FORMAT_MYSQL);
                
                $tokenEntry = SystemApiToken::create([
                    'user_id' => $userId,
                    'token' => $token,
                    'expires_at' => $expiryString,
                    'last_activity_at' => date(DATETIME_FORMAT_MYSQL),
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                    'user_agent' => $_SERVER['HTTP_USER_AGENT'],
                ]);
                
                return $tokenEntry;
            });
        
        } catch (Exception $ex){
            
            throw $ex;
            
        }
    }
    
    /**
     * Helper: If the userId has an existing valid token, return it
     * Otherwise give back nothing
     * 
     * @param int User ID 
     * @return SystemApiToken  The token object
     */ 
    protected function retrieveApiToken($userId)
    {
        try {
            
            return DB::transaction(function () use ($userId) {
                $token = SystemApiToken::unexpired()->where('user_id', $userId)->firstOrFail();
                $token->last_activity_at = date(DATETIME_FORMAT_MYSQL);
                $token->save();
                return $token;
            });
            
        } catch (Exception $ex) {
            
            return null;
            
        }     
    }
    
    /**
     * Helper: Revokes all API Tokesn for a given user
     * 
     * @return bool
     */ 
    protected function revokeApiTokens($userId)
    {
        // Update all their tokens' expiries to NOW.
        DB::transaction(function () use ($userId) {
            $token = SystemApiToken::unexpired()->where('user_id', $userId)->update([
                'expires_at' => date(DATETIME_FORMAT_MYSQL),
                'last_activity_at' => date(DATETIME_FORMAT_MYSQL),
                'forced_expire' => true,
            ]);
        });
    }
    
}