<?php

namespace App\Http\Controllers\Traits;

use DB;
use Exception;
use App\Exceptions\ApiException;
use App\Helpers\FriendlyPasswordGenerator as PassGen;
use App\Models\SystemTempToken;
use App\Models\User;

trait ManagesTempTokensTrait 
{
    /**
     * Helper: Generate a reset (temp) code
     * 
     * @param int $userId  User ID
     * @param int $expiryTimeHours  How long before the token expires
     * @param bool $requireUserInactive  Force user to be inactive to redeem this token
     * @return array
     */ 
    protected function generateTempToken($userId, $expiryTimeHours = 3, $requireUserInactive = false)
    {
        $expiryDateTime = (new \DateTime())->add(new \DateInterval("PT{$expiryTimeHours}H"));
        $expiryString = $expiryDateTime->format(DATETIME_FORMAT_MYSQL);
        
        do {
            $longCode = bin2hex(openssl_random_pseudo_bytes(32));
            $longCodeIsUnique = DB::table('system_temp_tokens')->where('long_code', $longCode)->first() === null;
        } while( ! $longCodeIsUnique);
        
        do {
            $manualCode = rand(100, 999) . rand(100, 999);
            $manualCodeIsUnique = DB::table('system_temp_tokens')
                ->where('user_id', $userId)
                ->where('manual_code', $longCode)
                ->first() === null;
        } while( ! $manualCodeIsUnique);
        
        // $tokenId = DB::table('system_temp_tokens')->insertGetId([
            // 'user_id' => $userId,
            // 'long_code' => $longCode,
            // 'manual_code' => $manualCode,
            // 'expires_at' => $expiryString,
            // 'require_user_inactive' => $requireUserInactive ? 1 : 0,
            // 'purpose' => $requireUserInactive ? 'activation' : 'passwordreset'
        // ]);
        $tempToken = new SystemTempToken();
        $tempToken->user_id = $userId;
        $tempToken->long_code = $longCode;
        $tempToken->manual_code = $manualCode;
        $tempToken->expires_at = $expiryString;
        $tempToken->require_user_inactive = $requireUserInactive ? 1 : 0;
        $tempToken->purpose = $requireUserInactive ? 'activation' : 'passwordreset';            // best guess
        $tempToken->save();
        
        if (!empty($tempToken->id)){
            return [$tempToken, $longCode, $manualCode];    // return the actual token string, not the ID
        }
    }
    
    /**
     * Helper: Renew a reset (temp) code
     * 
     * @param int $userId  User ID
     * @param int $expiryTimeHours  How long before the token expires
     * @param bool $requireUserInactive  Force user to be inactive to redeem this token
     * @return array
     */ 
    protected function renewTempToken($userId, $expiryTimeHours = 3, $requireUserInactive = true)
    {
        // Check that they already have a pending manual code
        $matched = DB::table('system_temp_tokens')
                ->where('user_id', $userId)
                ->where('expires_at', '>', date(DATETIME_FORMAT_MYSQL))
                ->whereNull('deleted_at')
                ->first();
                    
        if (!empty($matched) && $matched->retries < intval(config('takeIn.account.maxResendRetries'))){
            $expiryDateTime = (new \DateTime())->add(new \DateInterval("PT{$expiryTimeHours}H"));
            $expiryString = $expiryDateTime->format(DATETIME_FORMAT_MYSQL);
        
            $matchedId = $matched->id;
            DB::transaction(function () use ($matchedId, $expiryString) {
                DB::table('system_temp_tokens')->where('id', $matchedId)->increment('retries');
                DB::table('system_temp_tokens')->where('id', $matchedId)->update([
                    'expires_at' => $expiryString,
                    'updated_at' => date(DATETIME_FORMAT_MYSQL),
                ]);
            });
            
            return [$matched->id, $matched->long_code, $matched->manual_code];
        }

        throw new ApiException('Temporary token or code is no longer valid', 2310);
        
    }
    
    /**
     * Helper: Retrieve a temp token based on user and manual code, or
     * just the long code
     * 
     * @param int  User ID
     * @param bool $requireUserInactive  Force user to be inactive to redeem this token
     * @return array
     */ 
    protected function retrieveTempToken($longOrManualCode, $userId = null)
    {
        $matchedTempToken = null;
        
        if (is_null($userId)){
            // Assume we are looking for a long code (i.e. was sent out in email)
            $longCode = $longOrManualCode;
            $matchedTempToken = SystemTempToken::where('long_code', $longCode)->first();
        }
        else {
            // Assume we are looking for a manual code (i.e. was sent out in SMS)
            $manualCode = $longOrManualCode;
            $matchedTempToken = SystemTempToken::where('user_id', $userId)->where('manual_code', $manualCode)->first(); 
        }
        
        // Will trigger any errors that invalidate this token from being used
        $this->validateTempToken($matchedTempToken);
        
        return $matchedTempToken;
    }

    /**
     * Helper: Validate that a temp token is useable
     * 
     * @param SystemTempToken $token  User ID
     * @param bool $requireUserInactive  Force user to be inactive to redeem this token
     * @return array
     */ 
    protected function validateTempToken($tempToken)
    {      
        // Are we being sold a lemon
        if (empty($tempToken)){
            throw new ApiException('Cannot activate, token incorrect or not found', 4830);
        };
    
        // Check token expiry
        if (strtotime($tempToken->expires_at) < time()){
            throw new ApiException('Cannot activate, token expired', 4810);
        }
        
        // Check token already used
        if ($tempToken->deleted_at != null){
            throw new ApiException('Cannot activate, token already used', 4820);
        }
        
        // Attempt a self-activation
        // Note: only allow activate on disabled users (i.e. deleted_at is not null)
        $userId = $tempToken->user_id;
        $user = User::withTrashed()->where('id', $userId)->firstOrFail();
        if ($tempToken->require_user_inactive && $user->deleted_at == null){
            throw new ApiException('No inactive user was found (check if target user is already active)', 4812);
        }
      
        return true;
    }
    
    /**
     * Perform the activation using a temp token
     * Consumes the temp token and sets/returns a new temporary password
     * 
     * @param StdClass $tempToken  Token information
     * @return array 
     */
    protected function consumeTokenForTempPassword($tempToken)
    { 
        return DB::transaction(function () use ($tempToken) {
            
            $passwordPlain = PassGen::init()->generate();
            $passwordHashed = app('hash')->make($passwordPlain);
            
            // Update the user
            $user = User::withTrashed()->findOrFail($tempToken->user_id);
            if ($user->trashed()){
                $user->restore();
            }
            $user->password = $passwordHashed;
            $user->save();

            // Consume the token
            $tempToken->delete();       // soft delete

            return [$user, $passwordPlain];
            
        });
    }    
    

    /**
     * Helper: Check `purpose` field to match if this was an activation token
     * Useful in determining whether to send out activation emails
     * 
     * @param SystemTempToken $token  User ID
     * @return bool
     */ 
    protected function tokenIsForActivation(SystemTempToken $tempToken)
    {
        return property_exists($tempToken, 'purpose') && in_array(strtolower($tempToken->purpose), ['actv', 'activate', 'activation']);
    }

  
}