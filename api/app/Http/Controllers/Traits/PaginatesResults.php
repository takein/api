<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Http\Request;

trait PaginatesResults
{
    protected $defaultResultLimit = 1;
    
    /**
     * Was there a specific max limit requested?
     * 
     * @param $request Request
     * @return int $limit
     */
    protected function getPaginationLimit(Request $request)
    {
        if (intval($request->input('limit'))){
            $limit = intval($request->input('limit', 1));
        }
        elseif (intval($request->input('max'))){
            $limit = intval($request->input('max', 1));
        }
        else {
            $limit = $this->defaultResultLimit;
        }
        return $limit;
    }
    
    protected function formatPaginationCursors(Request $request, $cursorBefore, $cursorAfter)
    {
        $thisApiEndpoint = $this->apiEndpointDomain . '/' . $request->path();
        $limit = $this->getPaginationLimit($request);
        
        return [
            'cursorBefore' => $cursorBefore,
            'cursorAfter' => $cursorAfter,
            'beforeUrl' => $thisApiEndpoint . '?' . http_build_query(['before' => $cursorBefore]),
            'afterUrl' => $thisApiEndpoint . '?' . http_build_query(['after' => $cursorAfter]),
            'beforeUrlLimited' => $thisApiEndpoint . '?' . http_build_query(['before' => $cursorBefore, 'limit' => $limit]),
            'afterUrlLimited' => $thisApiEndpoint . '?' . http_build_query(['after' => $cursorAfter, 'limit' => $limit]),
        ];
    }
    
}