<?php

namespace App\Http\Controllers\User;

use DB;
use Exception;
use Illuminate\Http\Request;
use App\Exceptions\ApiException;
use App\Helpers\Formatter;
use App\Helpers\FriendlyPasswordGenerator as PassGen;
use App\Http\Controllers\TakeInController as BaseController;
use App\Http\Controllers\Traits\ManagesTempTokensTrait;
use App\Jobs\SendAuthorization;
use App\Jobs\SendPasswordSetSuccess;
use App\Models\Chef;
use App\Models\User;
use App\Models\UserHighDetail;

/*
===================================
 DEPRECATED 14 MAR 2016 Use UserController instead
===================================
*/

class UserAccountController extends BaseController
{
    use ManagesTempTokensTrait;
    
    public function __construct()
    {
        $this->massFillableFields = config('takeIn.user.massFillableFields');
        $this->userCanSee = config('takeIn.user.authedUserCanSee'); 
    }
    
    /**
     * Get the logged-in user's full details 
     * 
     * @return Response JsonResponse
     */
    public function show(Request $request)
    {
        try {
            
            $currentUser = $request->user();
            if (empty($currentUser)){
                return $this->errorResponse(new ApiException('Not authenticated', 2010), 404);
            }
            
            $user = UserHighDetail::findOrFail($currentUser->id);
            
            //
            // TODO: Many logics, little time, wow
            //
            
            return response()->json([
                'user' => $user,
                // Return some other interesting stuff like stats
            ]);
            
        } catch (Exception $ex) {
            
            return $this->errorResponse(new ApiException('Cannot retrieve your profile', 4000, $request, $ex), 500);
            
        }
        
    }
    
   
    /**
     * Update their own profile
     * 
     * @response JsonResponse
     */
    public function update(Request $request)
    {
        
        $currentUser = $request->user();
        if (empty($currentUser)){
            return $this->errorResponse(new ApiException('Not authenticated', 2010), 400);
        }
        
        //=====================================
        // 1. Check if this is a password set or reset
        //=====================================
        if ($request->has('password')){ 
            if (PassGen::PasswordStrength($request->input('password'))){
                try {
                    
                    // 1.1 Validate an auth code, if it was provided
                    $mfaSettings = json_decode($currentUser->mfa);
                    $dontRequireMfa = is_object($mfaSettings) && property_exists($mfaSettings, 'password') && !$mfaSettings->password;
                    $dontHaveMobile = empty($currentUser->mobile);
                    if ($dontRequireMfa || $dontHaveMobile){
                        $authCodeValid = true;      // let them pass, they didn't want MFA on passwords
                    }
                    elseif ($request->has('auth_code')){
                        $tempToken = $this->retrieveTempToken($request->input('auth_code'), $currentUser->id);
                        $tempToken->delete();       // soft delete
                        $authCodeValid = true;
                    }
                    else {
                        $authCodeValid = false;
                    }
                
                    // 1.2 Check if the auth code would be required
                    // Not required if the user is in forced password set
                    if ( ! ($currentUser->force_password_set || $authCodeValid)){
                        
                        // Create a tempToken and send this out via SMS
                        list($newTempToken, $longCode, $manualCode) = $this->generateTempToken($currentUser->id, 3);
                        $this->dispatch(new SendAuthorization($newTempToken, [ 'sms' => true ]));
                        
                        return $this->errorResponse(new ApiException('Authorisation code required', 2400), 400);
                    }
                    
                    return $this->updatePassword($currentUser, $request->input('password'));
               
                } catch (ApiException $aex){
                    
                    return $this->errorResponse($aex, 500);
                    
                } catch (Exception $ex){
                    
                    return $this->errorResponse(new ApiException('Could not activate', 4800, $request, $ex), 500);
                    
                }

               
            }
            return $this->errorResponse(new ApiException('Password isn\'t strong enough', 2220), 400);
            
        }
    
        //=====================================
        // 2. User/Chef own profile update 
        //=====================================
        if ($request->has('user')){
            try {
                
                $userPostData = $request->input('user', []);
                $this->clean($userPostData);
                $this->clean($chefPostData);
                
                if (count($userPostData) + count($chefPostData) == 0){
                    return $this->errorResponse(new ApiException('No valid user post data was provided', 4200), 400);
                }
                
                // Save it
                DB::transaction(function () use ($currentUser, $userPostData, $chefPostData) { 
                    $currentUser->update($userPostData);
                    $chef = Chef::find($currentUser->id);
                    if ($chef){
                        $chef->update($chefPostData);
                    }
                });
                return $this->successResponse($currentUser->id, 'updated', ['type' => 'user']);
                
            } catch (Exception $ex) {
                
                return $this->errorResponse(new ApiException('Cannot update user', 4000, $request, $ex), 500);
                
            }
        }
        
        //=====================================
        // 3. Otherwise assume bad format
        //=====================================
        return $this->errorResponse(new ApiException('Missing post data', 4000), 400);
        
    }

    /**
     * "Disable" their own account...
     * 
     * Note that when accounts are created, and email validation is required, then the
     * default account state would be "disabled"
     * 
     * @response JsonResponse
     */
    public function destroy(Request $request)
    {
        try {
            
            $currentUser = $request->user();
            if (empty($currentUser)){
                return $this->errorResponse(new ApiException('Not authenticated', 2010), 400);
            }
            
            DB::transaction(function () use ($currentUser) {
                
                // Set the password to some gibberish so they can't log in again
                // If they want to, they'll have to use the Account reactivation faciility
                
                $unguessablePassword = app('hash')->make(str_random(60));
                
                $currentUser->password = $unguessablePassword;
                $currentUser->save();
                $currentUser->delete();
                
                // $currentUser->update([
                    // 'password' => $unguessablePassword, 
                    // 'deleted_at' => date(DATETIME_FORMAT_MYSQL)
                // ]);
                
            });
            
            return $this->successResponse($currentUser->id, 'disabled', ['type' => 'user'], 200);

        } catch(Exception $ex){
            
            return $this->errorResponse(new ApiException('Cannot delete this user', 4000, $request, $ex), 500);
            
        }
        
        return $this->errorResponse(new ApiException('Not allowed to disable this profile', 4000), 401);
    }
   
    /**
     * Set a new password for the user
     *
     * @param int User ID
     * @param string New password
     * @return Response
     */
    protected function updatePassword($user, $newPassword)
    {
        try {
            $hashed = app('hash')->make($newPassword);
            DB::transaction(function () use ($user, $hashed) {

                $user->password = $hashed;
                $user->force_password_set = false;
                $user->save();
                
            });
            
            // Todo: Preference set to receive email updates?
            $emailWillSend = true;
            $this->dispatch(new SendPasswordSetSuccess($user, [ 'email' => $emailWillSend ]));
            
            return $this->successResponse($user->id, 'updated', ['type' => 'user', 'notification_type' => 'email', 'notification_sent' => $emailWillSend]); 
            
        } catch (Exception $ex) {
            
            return $this->errorResponse(new ApiException('Password could not be updated', 4000, null, $ex), 200);
            
        }
        
    }
   
    
}
