<?php

namespace App\Http\Controllers\User;

use DB;
use Exception;
use Schema;
use Illuminate\Http\Request;
use App\Exceptions\ApiException;
use App\Helpers\Formatter;
use App\Helpers\FriendlyPasswordGenerator as PassGen;
use App\Http\Controllers\TakeInController as BaseController;
use App\Http\Controllers\Traits\ManagesTempTokensTrait;
use App\Jobs\SendAuthorization;
use App\Jobs\SendPasswordSetSuccess;
use App\Models\User;
use App\Models\UserOwner;

class UserController extends BaseController
{
    use ManagesTempTokensTrait;
    
    public function __construct()
    {
        $this->massFillableFields = config('takeIn.user.massFillableFields');
        $this->userCanSee = config('takeIn.user.authedUserCanSee'); 
        parent::__construct();
    }
    
    /**
     * Gets a listing of users
     * Only way to access this functionality is when using a search term 
     * 
     * @response JsonResponse
     */
    public function index(Request $request)
    {
        try {
            if (!$request->has('term')){
                return $this->errorResponse(new ApiException('No search term received', 4000), 200);
            }
            
            $term = $request->input('term');
            $matchedUsersQuery = User::where(function ($query) use ($term) {
                $query->where('username', 'like', "%$term%")
                    ->orWhere('first_name', 'like', "%$term%")
                    ->orWhere('last_name', 'like', "%$term%");
                })->whereNull('deleted_at');
            if (empty($request->user())){
                $matchedUsersQuery = $matchedUsersQuery->lowDetail();
            }
            $matchedUsers = $matchedUsersQuery->get();
                            
            if ($matchedUsers->isEmpty()){
                return $this->errorResponse(new ApiException('No users found', 4000), 200);
            }
            
            return response()->json([
                'users' => $matchedUsers->toArray()
            ]);
            
        } catch (Exception $ex){
            
            return $this->errorResponse(new ApiException('API could not process your request', 4000, $request, $ex), 500);
            
        }
    }

    /**
     * Shows a detail level that an authenticated user would see
     * 
     * @param Request $request  Service-container injected request
     * @param mixed $id  User ID
     * @response JsonResponse
     */
    public function show(Request $request, $id)
    {
        try {
            
            $this->preventSystemUserDisplay($id);
            
            // If the requester is anonymous - select scopeLowDetail
            // If the requester is authenticated - select User (normal)
            if (empty($request->user())){
                $user = User::with('chef')->lowDetail()->findOrFail($id);
            }
            else {
                $user = User::with('chef')->findOrFail($id);
            }
            
            $user->member_since = $user->created_at;
            
            return response()->json([
                'user' => $user,
            ]);
            
        } catch (Exception $ex) {

            return $this->errorResponse(new ApiException('User does not exist', 4000, $request, $ex), 404);
            
        }
    }
    
   /**
     * Get the logged-in user's full details 
     * 
     * @return Response JsonResponse
     */
    public function showMe(Request $request)
    {
        try {
            
            $currentUser = $request->user();
            if (empty($currentUser)){
                return $this->errorResponse(new ApiException('Not authenticated', 2010), 404);
            }
            $user = UserOwner::with('chef')->findOrFail($currentUser->id);
            $user->member_since = $user->created_at;
            return response()->json([
                'user' => $user,
                // Return some other interesting stuff like stats
            ]);
            
        } catch (Exception $ex) {
            
            return $this->errorResponse(new ApiException('Cannot retrieve your profile', 4000, $request, $ex), 500);
            
        }
    }
    
   
    /**
     * Update their own profile
     * 
     * @response JsonResponse
     */
    public function update(Request $request)
    {
        $currentUser = $request->user();
        if (empty($currentUser)){
            return $this->errorResponse(new ApiException('Not authenticated', 2010), 400);
        }
        
        //=====================================
        // 1. Check if this is a password set or reset
        //=====================================
        if ($request->has('password')){ 
            if (PassGen::PasswordStrength($request->input('password'))){
                try {
                    
                    // 1.1 Validate an auth code, if it was provided
                    $mfaSettings = json_decode($currentUser->mfa);
                    $dontRequireMfa = is_object($mfaSettings) && property_exists($mfaSettings, 'password') && !$mfaSettings->password;
                    $dontHaveMobile = empty($currentUser->mobile);
                    if ($dontRequireMfa || $dontHaveMobile){
                        $authCodeValid = true;      // let them pass, they didn't want MFA on passwords
                    }
                    elseif ($request->has('auth_code')){
                        $tempToken = $this->retrieveTempToken($request->input('auth_code'), $currentUser->id);
                        $tempToken->delete();       // soft delete
                        $authCodeValid = true;
                    }
                    else {
                        $authCodeValid = false;
                    }
                
                    // 1.2 Check if the auth code would be required
                    // Not required if the user is in forced password set
                    if ( ! ($currentUser->force_password_set || $authCodeValid)){
                        
                        // Create a tempToken and send this out via SMS
                        list($newTempToken, $longCode, $manualCode) = $this->generateTempToken($currentUser->id, 3);
                        $this->dispatch(new SendAuthorization($newTempToken, [ 'sms' => true ]));
                        
                        return $this->errorResponse(new ApiException('Authorisation code required', 2400), 400);
                    }
                    
                    return $this->updatePassword($currentUser, $request->input('password'));
               
                } catch (ApiException $aex){
                    
                    return $this->errorResponse($aex, 500);
                    
                } catch (Exception $ex){
                    
                    return $this->errorResponse(new ApiException('Could not update password', 4700, $request, $ex), 500);                        

                }
            }
            return $this->errorResponse(new ApiException('Password isn\'t strong enough', 2220), 400);
            
        }
    
        //=====================================
        // 2. User/Chef own profile update 
        //=====================================
        if ($request->has('user')){
            try {
                
                $postData = $request->input('user', []);
                $updateTriggered = false;
                
                // Check if the user is a registered chef, if so, then update those details first
                if ($currentUser->chef && array_key_exists('chef', $postData)){
                    $chefPostData = array_only($postData['chef'], Schema::getColumnListing('chefs'));
                    if (count($chefPostData) > 0){
                        DB::transaction(function () use ($currentUser, $chefPostData) { 
                            $this->clean($chefPostData);
                            \App\Models\Chef::find($currentUser->id)->update($chefPostData);
                        });
                        $updateTriggered = true;
                    }
                    unset($postData['chef']);
                }
                
                $userPostData = array_only($postData, Schema::getColumnListing('users'));
                if (count($userPostData) > 0){
                    DB::transaction(function () use ($currentUser, $userPostData) { 
                        $this->clean($userPostData);
                        $currentUser->update($userPostData);
                    });
                    $updateTriggered = true;
                }
                
                if (! $updateTriggered){
                    return $this->errorResponse(new ApiException('No valid user post data was provided', 4200), 400);
                }
                
                return $this->successResponse($currentUser->id, 'updated', ['type' => 'user']);
                
            } catch (Exception $ex) {
                
                if (strpos($ex->getMessage(), 'users_username_unique') !== false){
                    return $this->errorResponse(new ApiException('Nothing updated, duplicate username', 4110, $request, $ex), 400);
                }
                elseif (strpos($ex->getMessage(), 'users_email_unique') !== false){
                    return $this->errorResponse(new ApiException('Nothing updated, duplicate email', 4115, $request, $ex), 400);
                }
                else {
                    return $this->errorResponse(new ApiException('Cannot update user', 4000, $request, $ex), 500);
                }
                
            }
        }
        
        //=====================================
        // 3. Otherwise assume bad format
        //=====================================
        return $this->errorResponse(new ApiException('Missing post data', 4000), 400);
        
    }

    /**
     * "Disable" their own account...
     * 
     * Note that when accounts are created, and email validation is required, then the
     * default account state would be "disabled"
     * 
     * @response JsonResponse
     */
    public function destroy(Request $request)
    {
        try {
            
            $currentUser = $request->user();
            if (empty($currentUser)){
                return $this->errorResponse(new ApiException('Not authenticated', 2010), 400);
            }
            
            DB::transaction(function () use ($currentUser) {
                
                // Set the password to some gibberish so they can't log in again
                // If they want to, they'll have to use the Account reactivation faciility
                
                $unguessablePassword = app('hash')->make(str_random(60));
                
                $currentUser->password = $unguessablePassword;
                $currentUser->save();
                $currentUser->delete();
                
                // $this->revokeApiTokens($currentUser->id);
                // Todo -- resolve the AuthController class and use this to perform a logout
                
            });
            
            return $this->successResponse($currentUser->id, 'disabled', ['type' => 'user'], 200);

        } catch(Exception $ex){
            
            return $this->errorResponse(new ApiException('Cannot delete this user', 4000, $request, $ex), 500);
            
        }
        
        return $this->errorResponse(new ApiException('Not allowed to disable this profile', 4000), 401);
    }
   
    /**
     * Set a new password for the user
     *
     * @param int User ID
     * @param string New password
     * @return Response
     */
    protected function updatePassword($user, $newPassword)
    {
        try {
            $hashed = app('hash')->make($newPassword);
            DB::transaction(function () use ($user, $hashed) {

                $user->password = $hashed;
                $user->force_password_set = false;
                $user->save();
                
                // Kill off all active API Tokens
                //$this->revokeApiTokens($user->id);
                // Todo -- resolve the AuthController class and use this to perform a logout
                
            });
            
            // Todo: Preference set to receive email updates?
            $emailWillSend = true;
            $this->dispatch(new SendPasswordSetSuccess($user, [ 'email' => $emailWillSend ]));
            
            return $this->successResponse($user->id, 'updated', ['type' => 'user', 'notification_type' => 'email', 'notification_sent' => $emailWillSend]); 
            
        } catch (Exception $ex) {
            
            return $this->errorResponse(new ApiException('Password could not be updated', 4000, null, $ex), 200);
            
        }
        
    } 
    
    /**
     * Prevent requests to access our defined system accounts i.e. #
     * Todo: build more hooks into this method, so that system account definitions can be
     * set through configuration
     * 
     * @param $id
     * @return null
     */
    protected function preventSystemUserDisplay($id)
    {
        if ($id < 10){
            throw new ApiException('Requested ID is defined as a system user and therefore restricted', 4310, $request, $ex);
        }
    }
    
}
