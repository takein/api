<?php

namespace App\Http\Controllers\User;

use DB;
use Exception;
use Illuminate\Http\Request;
use App\Exceptions\ApiException;
use App\Helpers\Formatter;
use App\Http\Controllers\Dish\DishController;
use App\Models\Listing;
use App\Models\User;

class UserDishController extends DishController
{
    public function __construct()
    {
        parent::__construct();
    }
 
    /**
     * Get lifetime dishes created by this requester
     * _Is paginated_
     * 
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $limit = $this->getPaginationLimit($request);
        
        try {
           
            $user = $request->user();
            $requestCursorInfo = Formatter::parseCursors($request->input('after', false), $request->input('before', false));
            
            $dishes = Listing::with('dish')->where('owner_id', $user->id)->get();

            $dishes->transform(function ($dish, $key) {

                // merge dish properties into the base listing
                foreach ( $dish->dish->toArray() as $key => $value){
                    if (! in_array($key, ['listing_id', 'owner_id', 'deleted_at' ])){
                        $dish->$key = $value;
                    }
                }
                
                unset($dish->dish);
                return $dish;
            });

            $chef = User::with('chef')->findOrFail($user->id);
                       
            // Set the metadata display
            $meta = [ 
                'total' => count($dishes), 
                'chef' => $chef,
            ];
            
            if (count($dishes) > 0){
                list($responseCursorBefore, $responseCursorAfter) = Formatter::encodeCursors($dishes->last(), $dishes->first(), 'tx_id');
                return response()->json([
                    'dishes' => $dishes->toArray(),
                    'meta' => $meta,
                    'cursor' => $this->formatPaginationCursors($request, $responseCursorBefore, $responseCursorAfter)

                ]);
            }
            
            return $this->errorResponse(new ApiException('No dishes found', 3410), 404);
            
        } catch (Exception $ex) {
            
            return $this->errorResponse(new ApiException('Something went wrong while retrieving dishes', 3000, $request, $ex), 500);
            
        }        
    }
    
}
