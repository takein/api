<?php

namespace App\Http\Controllers\User;

use DB;
use Exception;
use Illuminate\Http\Request;
use App\Exceptions\ApiException;
use App\Http\Controllers\TakeInImageController as BaseController;
use App\Models\User;

class UserImageController extends BaseController
{
    
    private $userImageUploadKey;
    
    public function __construct()
    {
        $this->imageUploadKeys = config('takeIn.user.imageUploadKeys');
    }
    
    /**
     * Get the images for a user's account
     * Will lookup authenticated requester if 'me' is provided as the User ID
     * 
     * _Todo:_ Provide more metadata such as when last updated, profile pic caption, etc.
     * 
     * @param Request $request 
     * @param string $userId
     * @return Response
     */    
    public function index(Request $request, $userId)
    {

        // Check if the image_url is formatted as a temp image i.e.
        // matches the scheme temp://542.jpg   
        // Where the ".jpg" extn or whatever is optional but handy if included
       
        if ($userId === 'me'){
            
            if (empty($request->user())){
                // Reject, must be authenticated
                return $this->errorResponse(new ApiException('Must be authenticated', 2012), 401);    
            }
            
            // Overwrite the userId with the appropriate value
            $userId = $request->user()->id;
        }
        
        $imageUrl = DB::table('users')->where('id', $userId)->value('profile_pic');
        $matches = [];
        
        if (preg_match('/^temp:\/\/(\d+)(?:\.[a-zA-Z]{1,4})?$/', $imageUrl, $matches)){
            $imageId = intval($matches[1]);
            $imageUrl = route('userImage', ['iid' => $imageId]);
            
            return response()->json([
                'image' => [
                    'id' => $imageId,
                    'url' => $imageUrl
                ]
            ]);
        }

        return $this->errorResponse(new ApiException('Image not found', 4200), 404);
    }
    
    /**
     * Retrieve and serve the image.
     * Do a quick check if allowed to serve, i.e. the image matches the requester's current profile one.
     * 
     * @param Request $request 
     * @param string $userId
     * @return Response
     */    
    public function show(Request $request, $imageId)
    {
        if (env('IMAGES_IN_DATABASE', 'false') == 'true'){
        
            $usersWithImageAssigned = DB::table('users')->where('profile_pic', 'like', "temp://$imageId%")->count();
            if ($usersWithImageAssigned){
                return $this->displayDatabaseImage($imageId);
            }
            
        }

        return $this->errorResponse(new ApiException('Image not found', 4200), 404);

    }

    /**
     * Store a new profile picture, which will overwrite any existing profile picture
     * Expected that this operation is always applied to the requester's own user account
     * 
     * @param Request $request 
     * @param string $userId -- Note, this is present for Interface implementation
     * @return Response
     */
    public function store(Request $request, $userId = null)
    {
        try {
            
            $user = $request->user();
            $imageUrl = DB::transaction(function () use ($request, $user) {

                $file = null;
                if (is_array($this->imageUploadKeys)){
                    foreach ($this->imageUploadKeys as $key){
                        if ($request->hasFile($key)){
                            $file = $request->file($key);
                            break;
                        }
                    }
                }
                else {
                    $file = $request->file($this->imageUploadKeys);
                }
                
                if (!empty($file) && $file->isValid() && $file->isReadable()){
                    
                    // Read the blob out
                    $fileObj = $file->openFile();
                    $blob = $fileObj->fread($fileObj->getSize());
                    $mimeType = $file->getMimeType();
                    $extension = $file->guessExtension();
                    
                    
                    // Are we storing it in database
                    if (env('IMAGES_IN_DATABASE', 'false') == 'true'){
                        $imageId = DB::table($this->tempImagesTable)->insertGetId([
                            'image_blob' => $blob,
                            'mime_type' => $mimeType,
                            'extension' => $extension
                        ]);
                    }
                    else {
                        throw new ApiException('No image storage method available', 1899);
                    }
                        
                        
                    // Construct a temp string to store into `users`
                    $imageUrl = "temp://$imageId.$extension";
                    $user = DB::table('users')
                                    ->where('id', $user->id)
                                    ->update(['profile_pic' => $imageUrl]);
                    
                    return $imageUrl;

                }
                throw new ApiException('Bad or invalid image upload', 1210);
            });

            return $this->successResponse($user->id, 'created', ['type' => 'user_image', 'profile_pic' => $imageUrl], 201);

        } catch (ApiException $ex) {
            
            return $this->errorResponse($ex, 400);
            
        } catch (Exception $ex) {
            
            return $this->errorResponse(new ApiException('User image failed to save', 4200, $request, $ex), 500);
            
        }            
    }

    /**
     * Removes the User profile image
     * Expected that this operation is always applied to the requester's own user account
     * 
     * @param Request $request 
     * @param string $userId -- Note, this is present for Interface implementation
     * @return Response
     */
    public function destroy(Request $request, $userId = null)
    {
        try {
            
            $user = $request->user();
            $imageUrl = DB::table('users')->where('id', $user->id)->value('profile_pic');
            
            if (empty($imageUrl)){
                return $this->errorResponse(new ApiException('Requested image does not exist', 3200), 404);
            }
            
            $deletion = DB::transaction(function () use ($imageUrl, $user) {
                                
                // TEMP IMAGE PROCESSING
                if (env('IMAGES_IN_DATABASE', 'false') == 'true'){
                    if (preg_match('/^temp:\/\/(\d+)(?:\.[a-zA-Z]{1,4})?$/', $imageUrl, $matches)){
                        DB::table($this->tempImagesTable)->where('id', $matches[1])->delete();
                    } 
                }
                // END TEMP IMAGE PROCESSING

                $user->update([ 'profile_pic' => null ]);
                
            });

            return $this->successResponse($user->id, 'deleted', ['type' => 'user_image']);

        } catch (Exception $ex) {
            
            return $this->errorResponse(new ApiException('Failed to delete user image', 4200, $request, $ex), 500);
            
        }

    }

}
