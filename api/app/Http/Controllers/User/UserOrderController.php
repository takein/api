<?php

namespace App\Http\Controllers\User;

use DB;
use Exception;
use Illuminate\Http\Request;
use App\Exceptions\ApiException;
use App\Helpers\Formatter;
use App\Http\Controllers\TakeInController as BaseController;
use App\Http\Controllers\Traits\PaginatesResults;
use App\Models\Order;
use App\Models\Transaction;

class UserOrderController extends BaseController
{
    use PaginatesResults;
 
    public function __construct()
    {
        parent::__construct();
    }
 
    /**
     * Get lifetime orders for the requester7
     * _Is paginated_
     * 
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        // Was there a specific max limit requested?
        $limit = $this->getPaginationLimit($request);
        
        try {
           
           $user = $request->user();
           
           $requestCursorInfo = Formatter::parseCursors($request->input('after', false), $request->input('before', false));
           
            // May have query string args to denote type
            // If neither was defined, then 'as buyer' cops the default
            $asBuyer = $request->input('as', 'buyer') == 'buyer';
            $asSeller = $request->input('as', '') == 'seller';
            
            $orders = $this->retrieveOrders($user, $requestCursorInfo, $asBuyer, $asSeller);
            $orders->transform(function ($order, $key) {
                $order->id = $order->tx_id;
                unset($order->tx->id);
                unset($order->tx_id);          
                unset($order->reverse_tx_id);   
                return $order;
            });
            
           // Display some metadata under the 'meta' key
            $meta = [ 'total' => count($orders) ];
            
            if (count($orders) > 0){
                list($responseCursorBefore, $responseCursorAfter) = Formatter::encodeCursors($orders->last(), $orders->first(), 'tx.id');
                return response()->json([
                    'orders' => $orders,
                    'meta' => $meta,
                    'cursor' => $this->formatPaginationCursors($request, $responseCursorBefore, $responseCursorAfter)
                ]);
            }
            
            return $this->errorResponse(new ApiException('No orders found', 5300), 404);
            
        } catch (Exception $ex) {
            
            return $this->errorResponse(new ApiException('Something went wrong while retrieving orders', 5000, $request, $ex), 500);
            
        }        
    }
    
    // Helper: Order retrieval 
    private function retrieveOrders($user, $cursorInfo, $asBuyer = true, $asSeller = true)
    {
        $ordersQuery = Order::whereHas('tx', function ($query) use ($user, $asBuyer, $asSeller) { 
        
            // if (!($asBuyer || $asSeller)){
            if ($asBuyer && $asSeller){
                $query->where('buyer_id', $user->id)->orWhere('seller_id', $user->id);
            }
            elseif ($asBuyer){
                $query->where('buyer_id', $user->id);
            }
            elseif ($asSeller){
                $query->where('seller_id', $user->id);
            }
            else {
                throw new InvalidArgumentException('Must retrieve orders as either buyer, seller or both');
            }
            
        });
        
        // $ordersQuery = $user->orders;
        // dd($ordersQuery);
        
        // dd($ordersQuery->get()->count());
        
        if (!empty($requestCursorInfo)){ 
            $orders = $ordersQuery->where('created_at', $requestCursorInfo->comp, $requestCursorInfo->datetime)->orderBy('created_at', $requestCursorInfo->dir < 0 ? 'desc' : 'asc')->get();
            if ($requestCursorInfo->dir > 0){
                $orders = $orders->reverse();       // To appear in reverse chrono order
            }
            return $orders;
        }
        
        return $ordersQuery->with('tx', 'dishSummary')->orderBy('created_at', 'desc')->get();
    }

}
