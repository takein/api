<?php

namespace App\Http\Controllers\User;

use DB;
use Exception;
use Illuminate\Http\Request;
use App\Exceptions\ApiException;
use App\Http\Controllers\TakeInController as BaseController;
use App\Models\Transaction as Tx;

class UserTransactionController extends BaseController
{
    
    /**
     * Pull up a history of a user's transactions
     * Todo: Checks if the transaction has a corresponding order
     *
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        try {
            
            $user = $request->user();
            
            $transactions = Tx::where('seller_id', $user->id)->orWhere('buyer_id', $user->id)->get();
            // $transactions = Tx::with()->where('seller_id', $user->id)->orWhere('buyer_id', $user->id)->get();
            // $transactions = $transactions->map(function () {   } );
            
            if (count($transactions) > 0){
                return response()->json([
                    'transactions' => $transactions,
                    'meta' => [
                        'total' => count($transactions),
                    ]
                ]);
            }
            
            return $this->errorResponse(new ApiException('No transactions found for this user', 5310, $request, $ex), 404);
            
        }
        catch (Exception $ex) {
            
            return $this->errorResponse(new ApiException('Could not retrieve transactions', 5300, $request, $ex), 500);
            
        }
    }
    
    
}
