<?php

namespace App\Http\Controllers;

use Exception;
use App\Exceptions\ApiException;
use App\Http\Requests\ApiRequest;


class ExceptionTestController
{    
    public function test(ApiRequest $request)
    {
        throw new ApiException('Testing only', 4000);
    }
}