<?php

namespace App\Http\Middleware\Auth;

use Closure;
use Illuminate\Http\Request;

class AccountRestrictionMiddleware 
{
    protected $forcedColumnName;
    protected $forcedAllowed;
    protected $disabledAllowed;
    
    public function __construct()
    {
        $forcedAllowedDefaults = ['AuthController@login', 'AuthController@logout', 'UserController@update'];
        $disabledAllowedDefaults = ['AuthController@login', 'AuthController@logout', 'UserController@update'];
        $this->forcedColumnName = config('takeIn.account.forcedPasswordResetColumn', 'force_password_set');
        $this->forcedAllowed = config('takeIn.account.forcedPasswordResetAllowedActions', $forcedAllowedDefaults);
        $this->disabledAllowed = config('takeIn.account.disabledAccountAllowedActions', $disabledAllowedDefaults);
    }
    
    /**
     * Filter the incoming request.
     *
     * Check if the user has an outstanding `force_password_reset` flag 
     * on their user record, or if they are disabled i.e. `deleted_at` != nulll;
     * if so, restrict their set of allowed actions
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return  mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if (!empty($request->user())){
            $user = $request->user();

            if (intval($user->{$this->forcedColumnName}) !== 0){
                
                $routeController = $request->route()[1]['uses'];

                // strip out the namespacing
                $routeController = last(explode('\\', $routeController));
                if (in_array($routeController, $this->forcedAllowed)){
                    return $next($request); 
                }

                return response()->json([
                    'error' => [
                        'code' => 2250,
                        'message' => 'Force set password is active, requester must set a new password before proceeding',
                    ]
                ], 401);
                
            }        
            
            if ($user->deleted_at != null){
                
                $routeController = $request->route()[1]['uses'];

                // strip out the namespacing
                $routeController = last(explode('\\', $routeController));
                // dd($routeController);
                if (in_array($routeController, $this->disabledAllowed)){
                    return $next($request); 
                }

                return response()->json([
                    'error' => [
                        'code' => 2260,
                        'message' => 'Account is disabled, re-activate your account before any other activity',
                    ]
                ], 401);
                
            }        
            
            
        }

        return $next($request); 
        
    }
    
}