<?php

namespace App\Http\Middleware\Auth;

use DB;
use Closure;
use Illuminate\Http\Request;

class AuthenticateMiddleware
{
    /**
     * Filter the incoming request.
     *
     * Check for the header X-API-Token
     * and use this to load the user's details
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // $canBeAnon = $anon == 'anon' || $anon == 'anonymous';
        
        $tokenHeader = config('takeIn.api.apiTokenHeaderKey');
        $tokenValue = $request->header($tokenHeader);
                
        // Lookup token value in the system_api_tokens table
        $matchedUserId = DB::table('system_api_tokens')
            ->where('token', $tokenValue)
            ->where('expires_at', '>', date(DATETIME_FORMAT_MYSQL))
            ->value('user_id');

        $user = \App\Models\User::find($matchedUserId);
        $request->setUserResolver(function () use ($user) {            // early binding
            return $user;
        });
        
        // Proceed...........
        return $next($request); 
        
    }
    

}