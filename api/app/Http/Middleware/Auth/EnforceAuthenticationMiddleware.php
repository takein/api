<?php

namespace App\Http\Middleware\Auth;

use DB;
use Closure;
use Illuminate\Http\Request;

class EnforceAuthenticationMiddleware
{
    /**
     * Filter the incoming request.
     *
     * Check for the header X-API-Token
     * and use this to load the user's details
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if ($request->user() == null){
            // Return an invalid API token response
            
            $debug = env('APP_DEBUG') == 'true';
            $errorResp = [
                'code' => 1000,
                'message' => 'A valid API token is required',
            ];
            if ($debug){
                $errorResp['requestMethod'] = $request->method();
                $errorResp['requestEndpoint'] = $request->url();
            }
            
            return response()->json([
                'error' => $errorResp
            ], 401);
            
        }
        
        return $next($request); 
        
    }
    

}