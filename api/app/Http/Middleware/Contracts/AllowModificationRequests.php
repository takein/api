<?php 

namespace App\Http\Middleware\Contracts;

interface AllowModificationRequests 
{
    // User-controller actions
    function allowModifyUserAction(\App\Models\User $user, array $pathParts);
    function allowModifyUserImageAction(\App\Models\User $user, array $pathParts);
    
    // Dish-controller actions
    function allowModifyDishAction(\App\Models\User $user, array $pathParts);
    function allowModifyDishImageAction(\App\Models\User $user, array $pathParts);
    
    // Order-controller actions
    function allowModifyOrderAction(\App\Models\User $user, array $pathParts);

}