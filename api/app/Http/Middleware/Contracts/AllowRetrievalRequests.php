<?php 

namespace App\Http\Middleware\Contracts;

interface AllowRetrievalRequests 
{
    // Order-controller actions
    function allowRetrieveOrderAction(\App\Models\User $user, array $pathParts);

}