<?php

namespace App\Http\Middleware;

use DB;
use Closure;
use Exception;
use App\Http\Middleware\Contracts\AllowModificationRequests as ModificationRequestsContract;
use App\Http\Middleware\Contracts\AllowRetrievalRequests as RetrieveRequestsContract;
use Illuminate\Http\Request;

class PolicyMiddleware implements ModificationRequestsContract, RetrieveRequestsContract
{
    
    private $handlerName = '[Not handled]';
    private $controllerName = '[Not handled]';
    private $methodName = '[Not handled]';
    private $segments = '[Not handled]';
    private $caughtException = null;
    
    /**
     * Filter the incoming request.
     *
     * Based on their $request->user(), pre-inspect their intended request by 
     * looking up the resource, checking that resource's user_id and
     * therefore determine authorisation.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return  mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // Do not allow anonymous users to continue
        if (empty($request->user())){
            
            return response()->json([
                'error' => [
                    'code' => 2022,
                    'message' => 'Request is must be authenticated, currently it is not.',
                ]
            ], 401);
            
        }
                    
        // Filter if requester is allowed to modify resource
        if (! $this->runPolicy($request)){
            
            $errorDetails = [
                'code' => 2010,
                'message' => 'You are not authorised to access or modify this item',
            ];
            
            $debug = env('APP_DEBUG') == 'true';
            if ($debug){
                $errorDetails = array_merge($errorDetails, get_object_vars($this));
                // $errorDetails['handlerName'] = $this->handlerName;
                // $errorDetails['controllerName'] = $this->controllerName;
                // $errorDetails['methodName'] = $this->methodName;
                // $errorDetails['segments'] = $this->segments;
                // $errorDetails['caughtException'] = $this->segments;
            }
            
            return response()->json([
                'error' => $errorDetails
            ], 403);
            
        }
        
        return $next($request); 
    }
    
    /**
     * Run the requester through authorisation filter to 
     * prevent unwanted modifications to resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    private function runPolicy(Request $request)
    {
        $method = $request->method();

        // Determine if their intention is just to view, or to actually modify
        // Assign a different handler methodname prefix based on the intention
        if (in_array($method, ['GET', 'HEAD', 'OPTIONS'])){
            $verb = 'allow_retrieve';
        };
        if (in_array($method, ['POST', 'PUT', 'PATCH', 'DELETE'])){
            $verb = 'allow_modify';
        };
                    
        $routeUses = $request->route()[1]['uses'];
        if (preg_match('/([a-zA-Z]+)Controller@([a-zA-Z]+)$/', $routeUses, $matches)){
            $controller = $this->controllerName = $matches[1];
            $method = $this->methodName =  $matches[2];
        }
        
        // Now pivot based on the controller name
        $handler = $this->handlerName = camel_case("{$verb}_{$controller}_action");
        if (method_exists($this, $handler)){
            $segments = $this->segments = $request->segments();
            array_splice($segments, 0, 2);     // assumption that first 2 segments will be 'api' and the version.
            return ( bool ) call_user_func_array(array($this, $handler), [$request->user(), $segments]);
        }

        // Default to true if not otherwise defined
        return true;
    }
    
    
    
    
    /**
     * Base method for testing access control
     *
     * @param  \App\Models\User  $user  The user object to compare to
     * @param  string[]  $pathParts  URL path segments
     * @param  string  $resourceName  The name to be detected in the URL path segments
     * @param  string  $tableName  The name of the database table
     * @param  string  $tablePrimaryKey  The PK field to query for ID
     * @return bool
     */
    private function verifyOwnership(\App\Models\User $user, $recordId, $tableName, $tableOwnerColumn = 'user_id', $tablePrimaryKey = 'id')
    {
        if (! (empty($recordId) || empty($tableName))){
            return DB::table($tableName)
                ->where($tablePrimaryKey, $recordId)
                ->value($tableOwnerColumn) == $user->id;
        }
        return false;
    }
    
    
    /**
     * Determine if the requester is authorised to edit this user
     *
     * @param  \App\Models\User  $user  The user object to compare to
     * @param  string[]  $pathParts  URL path segments
     * @return bool
     */
    public function allowModifyUserAction(\App\Models\User $user, array $pathParts)
    {
        // PATCH api/v1/user/1
        list(, $requestRecordId) = $pathParts;
        return ($requestRecordId === 'me') ? true : $this->verifyOwnership($user, $requestRecordId, 'users', 'id');
    }
    
    /**
     * Determine if the requester is authorised to post or delete a profile_pic
     *
     * @param  \App\Models\User  $user  The user object to compare to
     * @param  string[]  $pathParts  URL path segments
     * @return bool
     */
    public function allowModifyUserImageAction(\App\Models\User $user, array $pathParts)
    {
        // POST api/v1/user/1/image
        list(, $requestRecordId) = $pathParts;
        return ($requestRecordId === 'me') ? true : $this->verifyOwnership($user, $requestRecordId, 'users', 'id');
    }
      
    /**
     * Determine if the requester is authorised to edit this dish
     *
     * @param  \App\Models\User  $user  The user object to compare to
     * @param  string[]  $pathParts  URL path segments
     * @return bool
     */
    public function allowModifyDishAction(\App\Models\User $user, array $pathParts)
    {
        // POST api/v1/dish
        // If it's only got 1 pathPart it's probably create request
        if (count($pathParts) < 2){
            return true;
        }
        
        // PATCH api/v1/dish/1
        // Make sure it's LISTINGS not dishes
        list(, $requestRecordId) = $pathParts;
        return $this->verifyOwnership($user, $requestRecordId, 'listings', 'owner_id');
    }

    /**
     * Determine if the requester is authorised to upload or delete a dish image 
     *
     * @param  \App\Models\User  $user  The user object to compare to
     * @param  string[]  $pathParts  URL path segments
     * @return bool
     */
    public function allowModifyDishImageAction(\App\Models\User $user, array $pathParts)
    {
        // api/v1/dish/1/image  or api/v1/dish/image/57
        if (count($pathParts) > 2){
            
            list($one, $two, $three) = $pathParts;
            
            if (is_numeric($two)){
                return $this->allowModifyDishAction($user, $pathParts);       // shares same args as allowDishAction
            }
            
            if (is_numeric($three)){
                $listingId = DB::table('listings_images')->where('id', $three)->value('listing_id');
                return $this->allowModifyDishAction($user, ['dish', $listingId]);       // reconstruct to mimic a Dish action
            }
            
        }
        return false;
    }
    
    /**
     * Determine if the requester is authorised to retrieve an order. 
     * Only the buyer or seller should be able to do this.
     *
     * @param  \App\Models\User  $user  The user object to compare to
     * @param  string[]  $pathParts  URL path segments
     * @return bool
     */
    public function allowRetrieveOrderAction(\App\Models\User $user, array $pathParts)
    {
        try {
            
            list(, $requestRecordId) = $pathParts;
            $order = \App\Models\Order::findOrFail($requestRecordId);
            $isSeller = $this->verifyOwnership($user, $order->tx->id, 'transactions', 'seller_id');
            $isBuyer = $this->verifyOwnership($user, $order->tx->id, 'transactions', 'buyer_id');
            return $isSeller || $isBuyer;
            
        } catch (Exception $e){
            
            $this->caughtException = $e;
            return false;

        }
    }
   
    /**
     * Determine if the requester is authorised to reverse an order. 
     * Only the seller should be able to perform this.
     *
     * @param  \App\Models\User  $user  The user object to compare to
     * @param  string[]  $pathParts  URL path segments
     * @return bool
     */
    public function allowModifyOrderAction(\App\Models\User $user, array $pathParts)
    {
        // DELETE api/v1/order/23
        // Verify ownership of the requested order
        try {
            
            list(, $requestRecordId) = $pathParts;
            $order = \App\Models\Order::findOrFail($requestRecordId);
            return $this->verifyOwnership($user, $order->tx->id, 'transactions', 'seller_id');
            
        } catch (Exception $e){
            
            $this->caughtException = $e;
            return false;

        }
    }
}