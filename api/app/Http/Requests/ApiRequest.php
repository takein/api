<?php

namespace App\Http\Requests;

use DB;
use Closure;
use Illuminate\Http\Request;

class ApiRequest extends Request
{
    
    // =============================
    // Note: Currently unused due to difficulties in configuring
    // application to bind this ApiRequest rather than Illuminate\Http\Request
    // note it's hardcoded into Laravel\Lumen\Application at line 711
    // =============================
    
    protected $userId = null;     // null for anonymous
    protected $apiToken;
    
    /**
     * Use a database call to retrieve the user based 
     * on their authenticatedUserId
     *
     * @return ApiRequest
     */
    public function __construct()
    {
        // Change the way the user is resolved
        $this->setUserResolver(function () {
            return !empty($this->authenticatedUserId) ? DB::table('users')->where('id', $id)->first() : null;
        });
        
        parent::__construct();
    }
    
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
    
    public function setApiToken($token)
    {
        $this->apiToken = $token;
    }
    
    public function getApiToken()
    {
        return $this->apiToken;
    }
    
}