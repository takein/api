<?php

// =========================
// General v1 API
// =========================

$app->get('/', ['as' => 'cover', 'uses' => 'ApiMetadataController@cover' ]);
$app->get('/routes', ['as' => 'routes', 'uses' => 'ApiMetadataController@routes' ]);

// TEST REGION

// $app->get('extest', ['middleware' => ['tokenauth', 'policy'], 'uses' => 'ExceptionTestController@test']);        // ApiException testing
// $app->get('aclTest', ['middleware' => ['tokenauth', 'policy'], 'uses' => 'User\UserController@index']);
// $app->post('aclTest', ['middleware' => ['tokenauth', 'policy'], 'uses' => 'User\UserController@index']);
// $app->put('aclTest', ['middleware' => ['tokenauth', 'policy'], 'uses' => 'User\UserController@index']);
// $app->delete('aclTest', ['middleware' => ['tokenauth', 'policy'], 'uses' => 'User\UserController@index']);

// END TEST REGION
    

// =========================
// Authentication
// =========================
    
$app->post('login', 'Auth\AuthController@login');
$app->post('logout', 'Auth\AuthController@logout');
$app->post('auth/login', 'Auth\AuthController@login');
$app->post('auth/logout', 'Auth\AuthController@logout');
$app->post('auth/forgot', 'Auth\AccountController@forgotPassword');
$app->post('auth/resendcode', 'Auth\AccountController@resendCode');

$app->get('user/handshake', 'Auth\AccountController@handshake');
$app->post('user/signup', 'Auth\AccountController@signUp');
$app->post('user/activate', 'Auth\AccountController@processCode');       // activate with numeric code via app

// =========================
// My account
// =========================

$app->get('user/me', ['as' => 'currentUser', 'middleware' => 'tokenauth', 'uses' => 'User\UserController@showMe']);
$app->patch('user/me', ['middleware' => ['tokenauth', 'policy'], 'uses' => 'User\UserController@update']);
$app->put('user/me', ['middleware' => ['tokenauth', 'policy'], 'uses' => 'User\UserController@update']);
$app->delete('user/me', ['middleware' => ['tokenauth', 'policy'], 'uses' => 'User\UserController@destroy']);

$app->get('user/me/orders', ['middleware' => ['tokenauth'], 'uses' => 'User\UserOrderController@index']);      // Todo -- is this in the right namespace??
$app->get('user/me/transactions', ['middleware' => ['tokenauth'], 'uses' => 'User\UserTransactionController@index']);      // Todo -- is this in the right namespace??


$app->post('user/me/image', ['middleware' => ['tokenauth', 'policy'], 'uses' => 'User\UserImageController@store']);
$app->delete('user/me/image', ['middleware' => ['tokenauth', 'policy'], 'uses' => 'User\UserImageController@destroy']);  

$app->get('user/me/dishes', ['middleware' => ['tokenauth', 'policy'], 'uses' => 'User\UserDishController@index']);  


// =========================
// Users 
// =========================

$app->get('users', 'User\UserController@index');
$app->get('user/{id}', 'User\UserController@show');

$app->get('user/{id}/image', 'User\UserImageController@index');
$app->get('user/image/{iid}', ['as' => 'userImage', 'uses' => 'User\UserImageController@show']);    // temporary imaging solution

// =========================
// Dishes
// =========================

$app->get('dishes', 'Dish\DishController@index');
$app->get('dish/{id}', 'Dish\DishController@show');
$app->post('dish', ['middleware' => ['tokenauth', 'policy'], 'uses' => 'Dish\DishController@store']);
$app->patch('dish/{id}', ['middleware' => ['tokenauth', 'policy'], 'uses' => 'Dish\DishController@update']);
$app->put('dish/{id}', ['middleware' => ['tokenauth', 'policy'], 'uses' => 'Dish\DishController@update']);
$app->delete('dish/{id}', ['middleware' => ['tokenauth', 'policy'], 'uses' => 'Dish\DishController@destroy']);

$app->get('dish/{id}/images', 'Dish\DishImageController@index');
$app->get('dish/image/{iid}', ['as' => 'dishImage', 'uses' => 'Dish\DishImageController@displayDatabaseImage']);            // temporary imaging solution
$app->post('dish/{id}/image', ['middleware' => ['tokenauth', 'policy'], 'uses' => 'Dish\DishImageController@store']);
$app->delete('dish/image/{iid}', ['middleware' => ['tokenauth', 'policy'], 'uses' => 'Dish\DishImageController@destroy']);

$app->get('dish/{id}/orders', 'Dish\DishOrderController@index'); 
$app->post('dish/{id}/order', ['middleware' => ['tokenauth'], 'uses' => 'Dish\DishOrderController@store']);

// =========================
// Orders
// =========================

$app->get('order/{id}', ['middleware' => ['tokenauth', 'policy'], 'uses' => 'Order\OrderController@show']);
$app->put('order/{id}', ['middleware' => ['tokenauth', 'policy'], 'uses' => 'Order\OrderController@update']);
$app->patch('order/{id}', ['middleware' => ['tokenauth', 'policy'], 'uses' => 'Order\OrderController@update']);
$app->delete('order/{id}', ['middleware' => ['tokenauth', 'policy'], 'uses' => 'Order\OrderController@destroy']);



// =========================
// Reviews
// =========================

$app->get('user/{id}/reviews', 'Review\ChefReviewController@index');
$app->get('reviews', 'Review\ReviewController@index');
$app->post('review', 'Review\ReviewController@store');

$app->patch('review', ['middleware' => ['tokenauth', 'policy'], 'uses' => 'Review\ReviewController@update']);
$app->put('review', ['middleware' => ['tokenauth', 'policy'], 'uses' => 'Review\ReviewController@update']);
$app->delete('review', ['middleware' => ['tokenauth', 'policy'], 'uses' => 'Review\ReviewController@destroy']);

