<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// ===============================
// Activation/reset processors
// -- Note: These are NOT prepended with /api/v1
// ===============================

$app->get('account/activation', ['as' => 'activation', 'uses' => 'Auth\AccountController@processLinkToken']);     // activate with token in link0
$app->get('account/passwordreset', ['as' => 'passwordreset', 'uses' => 'Auth\AccountController@processLinkToken']);     // activate with token in link0 
