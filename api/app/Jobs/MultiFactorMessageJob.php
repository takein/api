<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\Traits\ThrottlesRetries;
use App\Jobs\Traits\SerializesSoftDeletedModels;

abstract class MultiFactorMessageJob extends Job
{
    // Vanilla SerializesModels does not account for soft deleted items
    use ThrottlesRetries, SerializesModels, SerializesSoftDeletedModels {
        SerializesSoftDeletedModels::getRestoredPropertyValue insteadof SerializesModels;
    }
    
    protected $willSendSms = true;
    protected $willSendEmail = true;
    
    public function __construct(array $sendOpts = null)
    {
        if (is_array($sendOpts)){
            $this->willSendSms = !array_key_exists('sms', $sendOpts) || $sendOpts['sms'] !== false;
            $this->willSendEmail = !array_key_exists('email', $sendOpts) || $sendOpts['email'] !== false;;
        } 
    }
    
}
