<?php

namespace App\Jobs;

use Log;
use Illuminate\Mail\Mailer;
use Illuminate\Http\Request;
use App\Sms\Contracts\SmsSender;
use App\Models\SystemTempToken;
use App\Models\User;

class SendActivation extends MultiFactorMessageJob
{    
    protected $user;
    protected $code;
    protected $urlCode;
    
    /**
     * Create a new job instance.
     *
     * @param  SystemTempToken  $token
     * @return void
     */
    public function __construct(SystemTempToken $token, array $sendOpts = null)
    {
        parent::__construct($sendOpts);
        
        $this->user = $token->user;
        $this->code = $token->manual_code;
        $this->urlCode = $token->long_code;
    }
    
    /**
     * Execute the job.
     *
     * @param  Illuminate\Mailer\Mailer  $mailer
     * @param  Illuminate\Http\Request     $request
     * @param  SmsSender  $sender
     * @return void
     */
    public function handle(SmsSender $sender)
    {  
        $this->throttleRetries();
        
        if ($this->reachedHardLimit()){
            return;
        }
    
        // Email
        if ($this->willSendEmail && !empty($this->user->email)){
            $mailer = app('mailer');        // Temp: resolve it out of app. Todo: Resolve it out of injected 
            
            // Lumen 5.2 doesn't recognise non-Request based URL roots for route() helper
            // $activationLink = route('activation', [ 'c' => base64_encode($this->urlCode) ]);     // route() helper appears broken in Lumen 5.2, no facility to change the rootUrl
            // Workaround: 
            $activationLink = config('app.url') . app()->namedRoutes['activation'] . '?' . http_build_query([ 'c' => base64_encode($this->urlCode) ]);
            $mailMerge = [
                'greetingName' => $this->user->first_name ?: $this->user->username,
                'activationLink' => $activationLink,
            ];
            $mailer->send('email.activation', $mailMerge, function ($m) {
                // NB: "From" address should automatically be set from the config file
                $m->subject('Activate your account');
                $m->to($this->user->email, "{$this->user->first_name} {$this->user->last_name} <{$this->user->email}>");
                
            });
        }

        // SMS
        if ($this->willSendSms && !empty($this->user->mobile)){
            $sender->sendActivationCode($this->code, $this->user->mobile);
        }
    }    
    
    /**
     * Handle a job failure.
     *
     * @return void
     */
    public function failed()
    {
        
        Log::warning('SendActivation job is failing', [
            'user' => $this->user,
            'code' => $this->code,
            'token' => $this->urlCode,
            'willSendSms' => $this->willSendSms,
            'willSendEmail' => $this->willSendEmail,
        ]);

    }
    
}
