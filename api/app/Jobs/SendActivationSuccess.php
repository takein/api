<?php

namespace App\Jobs;

use Illuminate\Mail\Mailer;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use App\Sms\Contracts\SmsSender;
use App\Jobs\SerializesSoftDeletedModels;
use App\Models\SystemTempToken;
use App\Models\User;

class SendActivationSuccess extends MultiFactorMessageJob
{    
    protected $user;
    
    /**
     * Create a new job instance.
     *
     * @param  SystemTempToken  $token
     * @return void
     */
    public function __construct(User $user, array $sendOpts = null)
    {
        parent::__construct($sendOpts);
        
        $this->user = $user;
    }
    
    /**
     * Execute the job.
     *
     * @param  Illuminate\Mailer\Mailer  $mailer            // Todo
     * @param  Illuminate\Http\Request     $request         // Todo
     * @param  SmsSender  $sender
     * @return void
     */
    public function handle(SmsSender $sender)
    {  
        $this->throttleRetries();
        
        if ($this->reachedHardLimit()){
            return;
        }
    
        // Email
        if ($this->willSendEmail && !empty($this->user->email)){
            $mailer = app('mailer');        // Temp: resolve it out of app. Todo: Resolve it out of injected 
            $mailMerge = [
                'greetingName' => $this->user->first_name ?: $this->user->username,
                'takeinSelfServiceUrl' => config('takeIn.activation.selfServiceUrl') . '/account',
            ];
            $mailer->send('email.activation-success', $mailMerge, function ($m) {
                // NB: "From" address should automatically be set from the config file
                
                $m->subject('Take In activation complete');
                $m->to($this->user->email, "{$this->user->first_name} {$this->user->last_name}");
                
            });
        }
    }    

}
