<?php

namespace App\Jobs;

use Illuminate\Mail\Mailer;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use App\Sms\Contracts\SmsSender;
use App\Models\SystemTempToken;
use App\Models\User;

class SendAuthorization extends MultiFactorMessageJob
{
    protected $user;
    protected $code;
    protected $urlCode;
    
    /**
     * Create a new job instance.
     * `SendAuthorization` will only send SMS codes
     *
     * @param  SystemTempToken  $token
     * @return void
     */
    public function __construct(SystemTempToken $token, array $sendOpts = null)
    {
        parent::__construct($sendOpts);
        
        $this->user = $token->user;
        $this->code = $token->manual_code;
        $this->urlCode = $token->long_code;
    }
    
    /**
     * Execute the job.
     *
     * @param  SmsSender  $sender
     * @return void
     */
    public function handle(SmsSender $sender)
    {  
        $this->throttleRetries();
        
        if ($this->reachedHardLimit()){
            return;
        }
    
        // SMS
        if (!empty($this->user->mobile)){
            $sender->sendAuthorizationCode($this->code, $this->user->mobile);
        }
    }    
    
}
