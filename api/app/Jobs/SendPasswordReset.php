<?php

namespace App\Jobs;

use Log;
use Illuminate\Mail\Mailer;
use Illuminate\Http\Request;
use App\Sms\Contracts\SmsSender;
use App\Models\SystemTempToken;
use App\Models\User;

class SendPasswordReset extends MultiFactorMessageJob
{
    protected $user;
    protected $code;
    protected $urlCode;

    /**
     * Create a new job instance.
     *
     * @param  SystemTempToken  $token
     * @return void
     */
    public function __construct(SystemTempToken $token, array $sendOpts = null)
    {
        parent::__construct($sendOpts);
        
        $this->user = $token->user;
        $this->code = $token->manual_code;
        $this->urlCode = $token->long_code; 
    }
    
    /**
     * Execute the job.
     *
     * @param  SmsSender  $sender
     * @return void
     */
    public function handle(SmsSender $sender)
    {  
        $this->throttleRetries();
        
        if ($this->reachedHardLimit()){
            return;
        }
    
        if ($this->willSendEmail && !empty($this->user->email)){
            try {
            
                $mailer = app('mailer');        // Temp: resolve it out of app. Todo: Resolve it out of injected 
                $resetLink = config('app.url') . app()->namedRoutes['passwordreset'] . '?' . http_build_query([ 'c' => base64_encode($this->urlCode) ]);
                $mailMerge = [
                    'greetingName' => $this->user->first_name ?: $this->user->username,
                    'resetLink' => $resetLink,
                ];
                $mailer->send('email.password-reset', $mailMerge, function ($m) {
                    // NB: "From" address should automatically be set from the config file
                    $m->subject('Your password reset link');
                    $m->to($this->user->email, "{$this->user->first_name} {$this->user->last_name} <{$this->user->email}>");
                });
                
            } catch (Exception $ex) {
                
                Log::error(get_class() . " could not send email to {$this->user->email}. Reason: ".$ex->getMessage()."\n");
                
            }            
        }

        if ($this->willSendSms && !empty($this->user->mobile)){
            try {
            
                $sender->sendPasswordResetCode($this->code, $this->user->mobile);
                
            } catch (Exception $ex) {
                
                Log::error(get_class() . " could not send SMS to {$this->user->mobile}. Reason: ".$ex->getMessage()."\n");
                
            }
        }

    }    
    
}
