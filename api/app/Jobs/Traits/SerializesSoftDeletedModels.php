<?php

namespace App\Jobs\Traits;

use Illuminate\Contracts\Database\ModelIdentifier;

trait SerializesSoftDeletedModels
{
    protected function getRestoredPropertyValue($value)
    {
        if ($value instanceof ModelIdentifier) {
            $model = new $value->class;

            if (method_exists($model, 'withTrashed')) {
                return $model->withTrashed()->findOrFail($value->id);
            }

            return $model->findOrFail($value->id);
        } else {
            return $value;
        }
    }
}