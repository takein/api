<?php

namespace App\Jobs\Traits;

trait ThrottlesRetries
{
    /**
     * Artificially throttle retries especially for jobs like SMS sending and emails
     *
     * @param int  Hard limit (optional, otherwise will lookup from config)
     * @return bool  Has it reached the limit?
     */
    protected function throttleRetries($every = null, $timeout = null)
    {
        $self = __CLASS__;
        $every = $every ?: config('queue.retries.throttleEvery', 3);
        $timeout = $timeout ?: config('queue.retries.timeout', 30);
        
        // Did something not work a lot?
        if ($this->attempts() > 0 && $this->attempts() % $every === 0) {
            $this->release($timeout);
        }
    }    
    
    /**
     * Check for the hard limit, to prevent insanity retries 
     * caused by badly written jobs
     *
     * @param int  Hard limit (optional, otherwise will lookup from config)
     * @return bool  Has it reached the limit?
     */
    protected function reachedHardLimit($hardLimit = null)
    {  
        // Since this is all about convenience, 
        // we'll directly call a config that we expect
        // But this can always be overriden with an argument
        
        $self = __CLASS__;  
        
        $hardLimit = $hardLimit ?: config("queue.retries.max" , 10);
    
        if ($this->attempts() > $hardLimit) {
            $this->delete();
            return true;
        }
        
        return false;    
    }
    
}