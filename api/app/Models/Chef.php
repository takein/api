<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\FormatsISO8601DatesTrait;

class Chef extends Model
{
    use SoftDeletes;
    use FormatsISO8601DatesTrait;
    
    protected $primaryKey = 'user_id';
    protected $guarded = [];
    protected $hidden = ['user_id', 'created_at', 'updated_at', 'deleted_at'];
    
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    
}
