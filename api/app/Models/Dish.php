<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\FormatsISO8601DatesTrait;

class Dish extends Model
{
    use FormatsISO8601DatesTrait;
    
    protected $primaryKey = 'listing_id';
    protected $guarded = [ 'listing_id', 'servings_remaining', 'price_per_serving' ];
    protected $hidden = [ 'listing_id', 'created_at', 'updated_at', 'deleted_at' ];
    
    protected $dates = ['start_pickup_time', 'end_pickup_time', 'start_delivery_time', 'end_delivery_time', 'cutoff_time'];
    // protected $appends = ['start_pickup_time', 'end_pickup_time', 'start_delivery_time', 'end_delivery_time', 'cutoff_time'];
    
    // Todo: research using serialization to achieve converted dates
    // https://laravel.com/docs/5.1/eloquent-serialization
    
    public function listing()
    {
        return $this->belongsTo('App\Models\Listing', 'listing_id');
    }
    
    public function reviews()
    {
        return $this->hasManyThrough('App\Models\Review', 'App\Models\Order', 'listing_id', 'order_id');
    }
    
}
