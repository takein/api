<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\FormatsISO8601DatesTrait;

class Listing extends Model
{
    use SoftDeletes;    
    use FormatsISO8601DatesTrait;
    
    protected $guarded = [ 'id', 'owner_id' ];
    protected $hidden = [ 'owner_id' ];
    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'owner_id');
    }
    
    public function chef()
    {
        return $this->belongsTo('App\Models\Chef', 'owner_id', 'user_id');
    }
    
    public function dish()
    {
        return $this->hasOne('App\Models\Dish', 'listing_id');
    }

    public function scopeLiteListing($query)
    {
        return $query->select(['id', 'name', 'owner_id']);
    }

}
