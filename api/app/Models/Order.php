<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\FormatsISO8601DatesTrait;

class Order extends Model
{
    use SoftDeletes;
    use FormatsISO8601DatesTrait;
    
    protected $primaryKey = 'tx_id';
    protected $guarded = ['approval_status', 'tx_id', 'reverse_tx_id'];
    protected $hidden = ['tx_id', 'listing_id', 'updated_at', 'deleted_at'];
    
    public function tx()
    {
        return $this->belongsTo('App\Models\Transaction', 'tx_id')->liteTx();
    }
    
    public function dishSummary()
    {
        return $this->belongsTo('App\Models\Listing', 'listing_id')->liteListing();
    }
    
    public function review()
    {
        return $this->hasOne('App\Models\Review');
    }
    
    public function scopeOfListing($query, $listingId)
    {
        return $query->where('listing_id', $listingId);
    }
    
}
