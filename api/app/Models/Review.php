<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\FormatsISO8601DatesTrait;

class Review extends Model
{
    use SoftDeletes;
    use FormatsISO8601DatesTrait;
    
    protected $guarded = ['id', 'order_id'];
    protected $hidden = ['listing_id', 'order_id', 'updated_at', 'deleted_at'];
    
    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

}
