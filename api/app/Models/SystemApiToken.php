<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\FormatsISO8601DatesTrait;

class SystemApiToken extends Model
{
    use FormatsISO8601DatesTrait;
    
    protected $guarded = [];
    protected $dates = ['expires_at', 'last_activity_at'];
    protected $casts = [
        'forced_expire' => 'boolean'
    ];
    
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id')->withTrashed();
    }
    
    public function scopeUnexpired($query)
    {
        return $query->where('expires_at', '>', date(DATETIME_FORMAT_MYSQL));
    }
    
}