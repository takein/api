<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\FormatsISO8601DatesTrait;

class SystemTempToken extends Model
{
    use SoftDeletes;
    use FormatsISO8601DatesTrait;
    
    protected $dates = ['deleted_at', 'expires_at'];
    protected $casts = [
        'require_user_inactive' => 'boolean'
    ];
    
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id')->withTrashed();
    }
    
}