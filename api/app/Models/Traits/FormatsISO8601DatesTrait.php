<?php

namespace App\Models\Traits;

use Carbon\Carbon;

trait FormatsISO8601DatesTrait 
{
    
    /** 
     * Convert a date to ISO8601, replacing with today's date if NULL
     *
     * @param $value
     * @return string date
     */
    private function convertISODate($value)
    {
        return (new \DateTime($value))->format(\DateTime::ISO8601);
    }
    
    /** 
     * Convert a date to ISO8601, honouring null dates
     *
     * @param $value
     * @return string date
     */
    private function convertISODateNullable($value)
    {
        return empty($value) ? null : $this->convertISODate($value);
    }
    
    /** 
     * Convert a date to Carbon, honouring null dates
     *
     * @param $value
     * @return Carbon date
     */
    private function convertCarbonDateNullable($attributeName, $value)
    {
        $this->attributes[$attributeName] = empty($value) ? null : new Carbon($value);
    }

    //======================
    // Common timestamps
    
    public function getCreatedAtAttribute($value)
    {
        return $this->convertISODate($value);
    }
    
    public function getDeletedAtAttribute($value)
    {
        return $this->convertISODateNullable($value);
    }
    
    public function getUpdatedAtAttribute($value)
    {
        return $this->convertISODate($value);
    }
    
    //================
    // For tokens

    public function getExpiresAtAttribute($value)
    {
        return $this->convertISODate($value);
    }

    public function getLastActivityAtAttribute($value)
    {
        return $this->convertISODate($value);
    }

    
    //==========================================
    // Get Dish pickup delivery times 
    // -- these require i/o, therefore need a setter (Eloquent mutator)
    
    public function getStartPickupTimeAttribute($value)
    {
        return $this->convertISODateNullable($value);
    }
    public function setStartPickupTimeAttribute($date) 
    {
        $this->convertCarbonDateNullable('start_pickup_time', $date);
    }
    
    public function getEndPickupTimeAttribute($value)
    {
        return $this->convertISODateNullable($value);
    }
    public function setEndPickupTimeAttribute($date) 
    {
        $this->convertCarbonDateNullable('end_pickup_time', $date);
    }
    
    public function getStartDeliveryTimeAttribute($value)
    {
        return $this->convertISODateNullable($value);
    }
    public function setStartDeliveryTimeAttribute($date) 
    {
        $this->convertCarbonDateNullable('start_delivery_time', $date);
    }

    public function getEndDeliveryTimeAttribute($value)
    {
        return $this->convertISODateNullable($value);
    }
    public function setEndDeliveryTimeAttribute($date) 
    {
        $this->convertCarbonDateNullable('end_delivery_time', $date);
    }
    
    public function getCutoffTimeAttribute($value)
    {
        return $this->convertISODateNullable($value);
    }
    public function setCutoffTimeAttribute($date) 
    {
        $this->convertCarbonDateNullable('cutoff_time', $date);
    }
    
    public function getPreferredPickupTimeAttribute($value)
    {
        return $this->convertISODateNullable($value);
    }
    public function setPreferredPickupTimeAttribute($date) 
    {
        $this->convertCarbonDateNullable('preferred_pickup_time', $date);
    }
 
    public function getPreferredDeliveryTimeAttribute($value)
    {
        return $this->convertISODateNullable($value);
    }
    public function setPreferredDeliveryTimeAttribute($date) 
    {
        $this->convertCarbonDateNullable('preferred_delivery_time', $date);
    }

}
