<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\FormatsISO8601DatesTrait;

class Transaction extends Model
{    
    use FormatsISO8601DatesTrait;
    
    protected $guarded = ['id'];
    protected $hidden = ['updated_at'];
    
    // A transaction may be related to an order
    public function order()
    {
        return $this->hasOne('App\Models\Order', 'tx_id');
    }
    
    // A transaction must have a seller
    public function seller()
    {
        return $this->belongsTo('App\Models\User', 'seller_id')->lowDetail();
    }
    
    // A transaction must have a buyer
    public function buyer()
    {
        return $this->belongsTo('App\Models\User', 'buyer_id')->lowDetail();
    }

    public function scopeLiteTx($query)
    {
        return $query;
        // return $query->select(['id', 'buyer_id', 'seller_id', 'subtotal_cost']);
    }

}
