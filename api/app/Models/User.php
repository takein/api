<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Lumen\Auth\Authorizable;
use App\Models\Traits\FormatsISO8601DatesTrait;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;
    use SoftDeletes;
    use FormatsISO8601DatesTrait;
    
    protected $visible = ['id', 'username', 'first_name', 'last_name', 'email', 'state', 'suburb', 'postcode', 'mobile', 'profile_desc', 'chef', 'member_since'];           // protected $hidden = [ 'password', 'created_at', 'updated_at', 'deleted_at' ];
    protected $fillable = ['username', 'first_name', 'last_name', 'email', 'street_address', 'house_number', 'street_name', 'suburb', 'postcode', 'state', 'mobile', 'profile_desc'];       // protected $guarded = [ 'id', 'password', 'is_system', 'force_password_set' ];
    protected $dates = ['deleted_at'];
    protected $casts = [
        'force_password_set' => 'boolean',
        'is_system' => 'boolean',
    ];
    
    public function orders()
    {
        return $this->hasManyThrough('App\Models\Order', 'App\Models\Transaction', 'buyer_id', 'tx_id');
    }
    
    public function chef()
    {
        return $this->hasOne('App\Models\Chef', 'user_id');
    }
    
    public function scopeLowDetail($query)
    {
        return $query->select(['id', 'username', 'first_name', 'last_name']);
    }

}
