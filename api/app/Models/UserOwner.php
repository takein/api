<?php

namespace App\Models;

use App\Models\User;

class UserOwner extends User
{
  
    protected $table = 'users';
  
    // Generally called if user is accessing their own data
    protected $visible = null;
    protected $hidden = [ 'password', 'created_at', 'updated_at', 'deleted_at' ];
  
}
