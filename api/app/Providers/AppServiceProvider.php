<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\BasicAuthenticator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadConstants();
        $this->catchOptionsRequests();
        
        // Configurations
        $this->app->configure('app');            //
        $this->app->configure('services');      // as per Laravel 5.2 way
        $this->app->configure('queue');         //
        $this->app->configure('sms');           // custom
        $this->app->configure('mail');           // for Illuminate\Mail    
        $this->app->configure('takeIn');        // custom

        // $app->configureMonologUsing(function($monolog) {
            // $monolog->pushHandler(...);
        // });        
        
        
        // Enable the Artisan command for TempTokens
        $this->commands('App\Console\Commands\TempTokensCommand');

    	// BasicAuthenticator helper
    	$this->app->singleton('BasicAuthenticator', function ($app) {
    		return new BasicAuthenticator($app['request']);
    	});
        
    }

    /**
     * If the incoming request is an OPTIONS request
     * we will register a handler for the requested route 
     * @return void
     */
    private function catchOptionsRequests()
    {
        $request = app('request');
        if ($request->isMethod('OPTIONS')){
            app()->options($request->path(), function() { return response('', 200); });
        }
    }
    
    /**
     * Load constants used by Take In API
     *
     * @return void
     */
    private function loadConstants()
    {
        if (!defined('DATETIME_FORMAT_MYSQL')){
            define('DATETIME_FORMAT_MYSQL', 'Y-m-d H:i:s');
        }
        
        if (!defined('DEV_ENV')){
            define('DEV_ENV', ( bool ) in_array(env('APP_ENV', 'local'), ['local', 'dev']));
        }
    }
    
}
