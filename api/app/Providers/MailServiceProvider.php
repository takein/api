<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Mail\MailServiceProvider as IlluminateMailServiceProvider;

class MailServiceProvider extends IlluminateMailServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        parent::register();
        
        $this->app->when('App\Jobs\Job')
            ->needs('Illuminate\Contracts\Mail\Mailer')
            ->give(function () {
                return $this->app['mailer'];
            });
        
    }
    
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array_merge(parent::provides(), ['Illuminate\Contracts\Mail\Mailer']);
    }
    
}
