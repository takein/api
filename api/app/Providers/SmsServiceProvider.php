<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Sms\SmsSender;

class SmsServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('App\Sms\Contracts\SmsSender', function ($app) {
            
            $driverName = env('SMS_DRIVER');
            $config = config("sms.drivers.$driverName", []);
            $driverClassName = $this->selectDriverClass($driverName);
            $driver = new $driverClassName($config);
            
            return new SmsSender($driver);
            // return new SmsSender();
            
        });
        
        // $this->app->when('App\Sms\SmsSender')
            // ->needs('App\Sms\Contracts\SmsDriver')
            // ->give(function () {
                // $driverName = env('SMS_DRIVER');
                // $config = config("sms.drivers.$driverName", []);
                // $driverClassName = $this->selectDriverClass($driverName);
                // return new $driverClassName($config);
            // });
        
    }
    
    /**
     * Check the environment and select driver accordingly
     *
     * @return null | string Classname
     */
    private function selectDriverClass($driverName)
    {
        switch ($driverName){
            case 'smsbroadcastcomau':
                return \App\Sms\Drivers\SmsBroadcast_Driver::class;
            default:
                return \App\Sms\Drivers\LogSms_Driver::class;
        }
    }
    
}
