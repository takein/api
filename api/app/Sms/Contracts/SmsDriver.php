<?php

namespace App\Sms\Contracts;

abstract class SmsDriver
{
    /**
     * Send a message
     * 
     * @param mixed $payload  Some data to send
     * @return bool  Sucess or failure
     */
    abstract public function sendMessage($message, $mobileNumber);
}