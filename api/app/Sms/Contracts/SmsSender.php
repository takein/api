<?php

namespace App\Sms\Contracts;

use App\User;
use App\SystemTempToken;

interface SmsSender 
{
    public function __construct(SmsDriver $driver);
    
    /**
     * Send an activation code
     *
     * @param string $code  The activation code text
     * @param string The mobile number to send it to
     * @return null | string Classname
     */
    public function sendActivationCode($code, $mobileNumber);
    
    /**
     * Send an authorization code
     *
     * @param string The authorization code text
     * @param string The mobile number to send it to
     * @return null | string Classname
     */
    public function sendAuthorizationCode($code, $mobileNumber);
    
}