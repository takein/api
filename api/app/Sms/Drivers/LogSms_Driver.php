<?php

namespace App\Sms\Drivers;

use Log;
use App\User;
use App\Helpers\Formatter;
use App\Sms\Contracts\SmsDriver as BaseSmsDriver;

class LogSms_Driver extends BaseSmsDriver
{

    /**
     * Send the SMS message
     */
    public function sendActivationCode($code, $mobileNumber)
    {
        Log::info(date('Y-m-d H:i:s') . " - [$code] was sent to [$mobileNumber]");
    }
    
}