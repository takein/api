<?php

namespace App\Sms\Drivers;

use Log;
use App\User;
use App\Helpers\Formatter;
use App\Sms\Contracts\SmsDriver as BaseSmsDriver;

class SmsBroadcast_Driver extends BaseSmsDriver
{
    protected $username;
    protected $password;
    protected $endpoint;
    protected $sourceNumber;
    protected $messageFormats;
    protected $refFormat;
    
    public function __construct($config)
    {
        $this->username = $config['username'];
        $this->password = $config['password'];
        $this->endpoint = $config['endpoint'];
        $this->sourceNumber = $config['sourceNumber'];
        $this->messageFormats = $config['messageFormats'];
        $this->refFormat = $config['refFormat'];
    }
    
    /**
     * Send the SMS message
     *
     * @param string $message  Text to send
     * @param string $mobileNumber  Number to send to
     * @param strnig $messageFormat  Key name of a config-loaded message format
     * @return bool  Success
     */
    public function sendMessage($message, $mobileNumber, $messageFormat = null)
    {
        if (!empty($messageFormat) && array_key_exists($messageFormat, $this->messageFormats)){
            $message = sprintf($this->messageFormats[$messageFormat], $message);
        }
        $ref = sprintf($this->refFormat, time(), md5($mobileNumber));      // todo: retrieve this from somewhere
            
        $querystring =  'username='.rawurlencode($this->username).
                    '&password='.rawurlencode($this->password).
                    '&to='.rawurlencode($mobileNumber).
                    '&from='.rawurlencode($this->sourceNumber).
                    '&message='.rawurlencode($message).
                    '&ref='.rawurlencode($ref);

        $resp = $this->send($querystring);
        return $resp;
    }
    
    /**
     * Underlying send mechanism
     *
     * @param string $payload  Query-string formatted arguments 
     * @return bool  Success
     */
    private function send($payload) {
        $ch = curl_init($this->endpoint);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec ($ch);
        curl_close ($ch);

        $response_lines = explode("\n", $output);
        
        $ok = true;
         foreach( $response_lines as $data_line){
            $message_data = "";
            $message_data = explode(':',$data_line);
            if($message_data[0] == "OK"){
                Log::info("The message to ".$message_data[1]." was successful, with reference ".$message_data[2]."\n");
            } elseif( $message_data[0] == "BAD" ){
                Log::error("The message to ".$message_data[1]." was NOT successful. Reason: ".$message_data[2]."\n");
                $ok = false;
            } elseif( $message_data[0] == "ERROR" ){
                Log::error("There was an error with this request. Reason: ".$message_data[1]."\n");
                $ok = false;
            }
        }
        
        return $ok;
        
    }

    
    
}