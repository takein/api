<?php

namespace App\Sms;

use App\Sms\Contracts\SmsSender as SmsSenderContract;

class SmsSender implements SmsSenderContract
{
    
    private $driver;
    
    public function __construct(Contracts\SmsDriver $driver)
    {
        $this->driver = $driver;
    }
    
    /**
     * Send an activation code
     *
     * @param string $code  The activation code text
     * @return null | string Classname
     */
    public function sendActivationCode($code, $mobileNumber)
    {
        return $this->driver->sendMessage($code, $mobileNumber, "activationCode");  
    }
    
    /**
     * Send an authorization code
     * Could be to change password, change user details, transaction related, etc.
     *
     * @param string $code  The activation code text
     * @return null | string Classname
     */
    public function sendAuthorizationCode($code, $mobileNumber)
    {
        return $this->driver->sendMessage($code, $mobileNumber, "authorizationCode");  
    }
    
    /**
     * Send a password reset code
     * ## Reuse the same method for Authorization
     *
     * @param string $code  The activation code text
     * @return null | string Classname
     */
    public function sendPasswordResetCode($code, $mobileNumber)
    {
        return $this->sendAuthorizationCode($code, $mobileNumber);  
    }

    
    
}