<?php

return [

    /*
    |--------------------------------------------------------------------------
    | PDO Fetch Style
    |--------------------------------------------------------------------------
    |
    | By default, database results will be returned as instances of the PHP
    | stdClass object; however, you may desire to retrieve records in an
    | array format for simplicity. Here you can tweak the fetch style.
    |
    */

    'fetch' => PDO::FETCH_CLASS,

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => isset($_SERVER['DB_CONNECTION']) ? $_SERVER['DB_CONNECTION'] : env('DB_CONNECTION', 'mysql'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        // 'testing' => [
            // 'driver' => 'sqlite',
            // 'database' => ':memory:',
        // ],

        // 'sqlite' => [
            // 'driver'   => 'sqlite',
            // 'database' => env('DB_DATABASE', base_path('database/database.sqlite')),
            // 'prefix'   => env('DB_PREFIX', ''),
        // ],

        'mysql' => [
            'driver'    => 'mysql',
            'host'      => isset($_SERVER['DB_HOST']) ? $_SERVER['DB_HOST'] : env('DB_HOST', 'localhost'),
            'port'      => isset($_SERVER['DB_PORT']) ? $_SERVER['DB_PORT'] : env('DB_PORT', 3306),
            'database'  => isset($_SERVER['DB_DATABASE']) ? $_SERVER['DB_DATABASE'] : env('DB_DATABASE', 'takein'),
            'username'  => isset($_SERVER['DB_USERNAME']) ? $_SERVER['DB_USERNAME'] : env('DB_USERNAME', 'admin'),
            'password'  => isset($_SERVER['DB_PASSWORD']) ? $_SERVER['DB_PASSWORD'] : env('DB_PASSWORD', ''),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => isset($_SERVER['DB_PREFIX']) ? $_SERVER['DB_PREFIX'] : env('DB_PREFIX', ''),
            'timezone'  => isset($_SERVER['DB_TIMEZONE']) ? $_SERVER['DB_TIMEZONE'] : env('DB_TIMEZONE', '+00:00'),
            'strict'    => false,
        ],

        // 'pgsql' => [
            // 'driver'   => 'pgsql',
            // 'host'     => env('DB_HOST', 'localhost'),
            // 'port'     => env('DB_PORT', 5432),
            // 'database' => env('DB_DATABASE', 'forge'),
            // 'username' => env('DB_USERNAME', 'forge'),
            // 'password' => env('DB_PASSWORD', ''),
            // 'charset'  => 'utf8',
            // 'prefix'   => env('DB_PREFIX', ''),
            // 'schema'   => 'public',
        // ],

        // 'sqlsrv' => [
            // 'driver'   => 'sqlsrv',
            // 'host'     => env('DB_HOST', 'localhost'),
            // 'database' => env('DB_DATABASE', 'forge'),
            // 'username' => env('DB_USERNAME', 'forge'),
            // 'password' => env('DB_PASSWORD', ''),
            // 'prefix'   => env('DB_PREFIX', ''),
        // ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'cluster' => isset($_SERVER['REDIS_CLUSTER']) ? $_SERVER['REDIS_CLUSTER'] : env('REDIS_CLUSTER', false),

        'default' => [
            'host'     => isset($_SERVER['REDIS_HOST']) ? $_SERVER['REDIS_HOST'] : env('REDIS_HOST', '127.0.0.1'),
            'port'     => isset($_SERVER['REDIS_PORT']) ? $_SERVER['REDIS_PORT'] : env('REDIS_PORT', 6379),
            'database' => isset($_SERVER['REDIS_DATABASE']) ? $_SERVER['REDIS_DATABASE'] : env('REDIS_DATABASE', 0),
            'password' => isset($_SERVER['REDIS_PASSWORD']) ? $_SERVER['REDIS_PASSWORD'] : env('REDIS_PASSWORD', null),
        ],

    ],

];
