<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Mail Driver
    |--------------------------------------------------------------------------
    |
    | Laravel supports both SMTP and PHP's "mail" function as drivers for the
    | sending of e-mail. You may specify which one you're using throughout
    | your application here. By default, Laravel is setup for SMTP mail.
    |
    | Supported: "smtp", "mail", "sendmail", "mailgun", "mandrill", "ses", "log"
    |
    */
    'driver' => env('MAIL_DRIVER', 'smtp'),
    
    /*
    |--------------------------------------------------------------------------
    | Global "From" Address
    |--------------------------------------------------------------------------
    |
    | You may wish for all e-mails sent by your application to be sent from
    | the same address. Here, you may specify a name and address that is
    | used globally for all e-mails that are sent by your application.
    |
    */
    
    'from' => [
        'address' => env('MAIL_FROM_ADDRESS', 'noreply@take-in.com.au'), 
        'name' => env('MAIL_FROM_NAME', 'Take in')
    ],

    
    // Universal "to" address -- For debugging
    
    'to' => [
        // 'address' => in_array(env('APP_ENV', 'local'), ['local', 'dev']) ? 'alan@take-in.com.au' : null,
        'address' => DEV_ENV ? 'alan@take-in.com.au' : null,
        'name' => DEV_ENV ? 'Take in' : null
    ],
    
];