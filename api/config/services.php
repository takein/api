<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */
    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],
    
    // I can't wait for the null coalescing operator
    'mandrill' => [
        'secret' => isset($_SERVER['MANDRILL_API_KEY']) ? $_SERVER['MANDRILL_API_KEY'] : env('MANDRILL_API_KEY'),
    ],
    
    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],
    
    'stripe' => [
        'model'  => App\User::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    
    'smsbroadcastcomau' => [
        'username' => isset($_SERVER['SMS_GATEWAY_USERNAME']) ? $_SERVER['SMS_GATEWAY_USERNAME'] : env('SMS_GATEWAY_USERNAME', 'admin'),
        'password'   => isset($_SERVER['SMS_GATEWAY_PASSWORD']) ? $_SERVER['SMS_GATEWAY_PASSWORD'] : env('SMS_GATEWAY_PASSWORD', 'admin'),
    ],

];