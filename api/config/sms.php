<?php

return [

    /*
    |--------------------------------------------------------------------------
    | SMS Drivers
    |--------------------------------------------------------------------------
    |
    */

    'drivers' => [

        'default' => [],

        'smsbroadcastcomau' => [
        
            'endpoint' => isset($_SERVER['SMS_GATEWAY_ENDPOINT']) ? $_SERVER['SMS_GATEWAY_ENDPOINT'] : env('SMS_GATEWAY_ENDPOINT', 'http://localhost'),
            'username' => isset($_SERVER['SMS_GATEWAY_USERNAME']) ? $_SERVER['SMS_GATEWAY_USERNAME'] : env('SMS_GATEWAY_USERNAME', 'admin'),                 // also moved to services config file
            'password'   => isset($_SERVER['SMS_GATEWAY_PASSWORD']) ? $_SERVER['SMS_GATEWAY_PASSWORD'] : env('SMS_GATEWAY_PASSWORD', 'admin'),               // also moved to services config file
            'sourceNumber'  => isset($_SERVER['SMS_GATEWAY_SOURCENUMBER']) ? $_SERVER['SMS_GATEWAY_SOURCENUMBER'] : env('SMS_GATEWAY_SOURCENUMBER', 'TakeIn'),
            
            'messageFormats' => [
                'sellerOrderNotification' => 'You have %s for your Take in listing. Open the app and get in touch with them!',
                'sellerOrderApprovalPending' => 'You have %s awaiting approval. Open the app to confirm the order!',
                'buyerOrderApproved' => 'Heads up: %s has approved your Take in order!',
                'buyerOrderRejected' => 'Sorry but %s has declined your order. Open the app for more details.',
                'authorizationCode' => 'Use %s as your Take in authorisation code',
                'activationCode' => 'Use %s as your Take in activation code',
            ],
            
            'refFormat' => '%s.%s',
            
        ],
    
    ]

];
