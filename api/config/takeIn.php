<?php

// Note: constants are loaded into "App\Providers\ConstantServiceProvider"

return [

    // ===================
    // General API
    
    'api' => [
        'apiTokenHeaderKey' => 'X-API-Token',       // the name of the http header      
        'apiTokenSalt' => 'AreyOUSaltY?!#',
    ],
    
    'activation' => [
        'selfServiceUrl' => env('TAKEIN_SELFSERVICE_URL', 'https://my.take-in.com.au'), 
        'failedMessage' => 'We encountered a problem whilst activating your account. Please try clicking your link again, or contact us on NNN NNNNNNNN.',
        'successMessage' => 'Your account has been activated.',
    ],
    
    'account' => [
        'maxResendRetries' => 4,
        'forcedPasswordResetColumn' => 'force_password_set',
        'forcedPasswordResetAllowedActions' => ['AuthController@login', 'AuthController@logout', 'AccountController@current', 'UserController@update'],
        'disabledAccountAllowedActions' => ['AuthController@login', 'AuthController@logout', 'AccountController@current', 'UserController@update', 'UserController@destroy'],
        'createChefAlongsideUser' => true
    ],
    
    // ===================
    // Data and formatting related
    
    'data' => [
        'knownDateColumns' => ['date', '_delivery_time', 'created_at', 'updated_at', 'deleted_at', 'expiry', 'pickup_time', 'delivery_time', 'cutoff_time'],
        // 'knownDateColumns' => [],
    ],
    
    // ===================
    // Dishes, reviews, categories, users

    'user' => [
    //    'massFillableFields' => ['first_name', 'last_name', 'username', 'email', 'mobile', 'house_number', 'street_name', 'street_address', 'suburb', 'postcode', 'state'],
    //    'anonUserCanSee' => ['id', 'username', 'first_name', 'last_name'],
    //    'authedUserCanSee' => ['id', 'username', 'first_name', 'last_name', 'email', 'state', 'suburb', 'postcode', 'mobile', 'desc', 'instagram_handle'],
    //    'userTableFields' => ['first_name', 'last_name', 'username', 'email', 'house_number', 'street_name', 'suburb', 'postcode', 'state', 'mobile'],
    //    'chefTableFields' => ['desc', 'instagram_handle', 'default_pickup_address'],
        'imageUploadKeys' => ['userImageFile', 'user_image_file']
    ],    
    
    'listing' => [
        'massFillableFields' => ['owner_id', 'name', 'description'],
    ],
    
    'dish' => [
        'defaultResultLimit' => 20,
        'massFillableFields' => ['listing_id', 'ingredients', 'price_per_serving', 'servings_total', 'servings_available', 'delivery_address_center', 'pickup_address', 'delivery_radius', 'related_dish_id', 'start_pickup_time', 'end_pickup_time', 'start_delivery_time', 'end_delivery_time', 'delivery_radius', 'pickup_delivery_options', 'calories', 'kilojoules'],
        'imageUploadKeys' => ['dishImageFile', 'dish_image_file']
    ],
    
    'tx' => [
        'nonOwnerCanSee' => ['seller_id', 'buyer_id', 'created_at' ]
    ],

    
    'order' => [
        'nonOwnerCanSee' => ['tx_id', 'listing_id' ]
    ],

];
