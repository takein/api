<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_types', function(Blueprint $table){
           $table->string('type');
           $table->primary('type');
           $table->text('description'); 
        });
        
        Schema::create('categories', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('icon_url', 256)->nullable();
            $table->string('type');     // e.g. CU=Cusine, DR=Dietary Requirement, AL=Allergy
            $table->foreign('type')->references('type')->on('category_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('categories')){
            Schema::table('categories', function ($table) {
                $table->dropForeign('categories_type_foreign');
            });
            Schema::drop('categories');        
        }
                
        Schema::dropIfExists('category_types');
    }
}
