<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function(Blueprint $table){
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('username')->unique();
            $table->string('password')->default('');             // hashed using bcrypt() helper
            $table->string('email')->unique();
            $table->string('street_address')->nullable();
            $table->string('house_number')->nullable();
            $table->string('street_name')->nullable();
            $table->string('suburb')->nullable();
            $table->string('postcode', 4)->nullable();
            $table->string('state', 3)->nullable();
            $table->string('mobile', 12)->nullable();
            $table->text('profile_desc')->nullable();      // Freetext description
            $table->string('profile_pic', 256)->nullable();      // url to AWS S3 
            $table->string('profile_pic_basecolor', 10)->nullable();      // e.g. #435231
            $table->text('profile_pic_thumbnail')->nullable();      // data URI
            $table->tinyInteger('force_password_set')->default(0);
            $table->tinyInteger('is_system')->default(0);
            $table->string('mfa')->default('{}');               // is a json object, Laravel migrations support json, but MySQL 5.6 doesn't... so....
            $table->timestamps();
            $table->softDeletes();
        });
        
        Schema::table('users', function(Blueprint $table){
            // placeholder for actual Eloquent timestamps
            DB::connection()->getPdo()->exec('ALTER TABLE `users` CHANGE `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
            DB::connection()->getPdo()->exec('ALTER TABLE `users` CHANGE `updated_at` `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
            DB::connection()->getPdo()->exec('ALTER TABLE `users` AUTO_INCREMENT = 10');        // ensure that ID's 1 - 9 are used for system purposes
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
