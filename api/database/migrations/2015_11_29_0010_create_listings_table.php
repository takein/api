<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::dropIfExists('listings');
        
        Schema::create('listings', function(Blueprint $table){
            $table->increments('id');
            $table->integer('owner_id')->unsigned();
            $table->string('name');
            $table->text('description')->nullable();
            // $table->string('image_url', 256)->nullable();
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('owner_id')->references('id')->on('users')->onDelete('cascade');
        });
        
        Schema::table('listings', function(Blueprint $table){
            // placeholder for actual Eloquent timestamps
            DB::connection()->getPdo()->exec('ALTER TABLE `listings` CHANGE `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
            DB::connection()->getPdo()->exec('ALTER TABLE `listings` CHANGE `updated_at` `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
        });
        
        Schema::create('listings_categories', function(Blueprint $table){
            $table->integer('listing_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->primary(['listing_id', 'category_id']);
            $table->foreign('listing_id')->references('id')->on('listings')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });
        
        // // Update the transactions table
        // if (Schema::hasTable('transactions')){
            // Schema::table('transactions', function ($table) {
                // $table->dropForeign('orders_dish_id_foreign');
                // $table->renameColumn('dish_id', 'listing_id');
                // $table->foreign('listing_id')->references('id')->on('listings')->onDelete('cascade');
            // });
        // };
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // if (Schema::hasTable('dishes')){
            // Schema::table('dishes', function ($table) {
                // $table->dropForeign('dishes_categories_dish_id_foreign');
            // });
            // Schema::drop('dishes');
        // }
        
        if (Schema::hasTable('listings_categories')){
            Schema::table('listings_categories', function ($table) {
                $table->dropForeign('listings_categories_listing_id_foreign');
                $table->dropForeign('listings_categories_category_id_foreign');
            });
            Schema::drop('listings_categories');
        }
        
        // // Have to do a dirty nested drop here
        // if (Schema::hasTable('transactions')){
            // if (Schema::hasTable('reviews')){
                // Schema::table('reviews', function ($table) {
                    // $table->dropForeign('reviews_transaction_id_foreign');
                // });
                // Schema::drop('reviews');    
            // }
            
            // Schema::table('transactions', function ($table) {
                // $table->dropForeign('transactions_seller_id_foreign');
                // $table->dropForeign('transactions_buyer_id_foreign');
                // $table->dropForeign('transactions_listing_id_foreign');
            // });
            // Schema::drop('transactions');    
        // }
                
        Schema::dropIfExists('dishes');
        Schema::dropIfExists('listings');
    }
}
