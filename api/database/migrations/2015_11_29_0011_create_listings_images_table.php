<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingsImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listings_images', function(Blueprint $table){
            $table->increments('id');
            $table->integer('listing_id')->unsigned();
            $table->text('caption');
            $table->string('image_url', 256)->nullable();
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('listing_id')->references('id')->on('listings')->onDelete('cascade');
        });
        
        Schema::table('listings_images', function(Blueprint $table){
            // placeholder for actual Eloquent timestamps
            DB::connection()->getPdo()->exec('ALTER TABLE `listings_images` CHANGE `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
            DB::connection()->getPdo()->exec('ALTER TABLE `listings_images` CHANGE `updated_at` `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listings_images');
    }
}
