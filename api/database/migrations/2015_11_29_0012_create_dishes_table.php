<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {  
        // cf. 2016_01_19_121258_create_listings_table.php 
        // The dishes schema is altered in that migration
        Schema::create('dishes', function(Blueprint $table){
            $table->integer('listing_id')->unsigned();
            $table->text('ingredients')->nullable();
            $table->decimal('price_per_serving', 5, 2);
            $table->integer('servings_total')->default(1);
            $table->integer('servings_remaining')->default(1);
            $table->text('delivery_address_center')->nullable();
            $table->text('pickup_address')->nullable();
            $table->integer('delivery_radius')->nullable();
            $table->dateTime('start_pickup_time')->nullable();
            $table->dateTime('end_pickup_time')->nullable();
            $table->dateTime('start_delivery_time')->nullable();
            $table->dateTime('end_delivery_time')->nullable();
            $table->dateTime('cutoff_time')->nullable();
            $table->decimal('delivery_cost', 5, 2)->default(0.0);
            $table->string('pickup_delivery_options', 2)->default('P');
            $table->integer('calories')->default(0);
            $table->integer('kilojoules')->default(0);
            $table->timestamps();
            
            $table->primary('listing_id');
            $table->foreign('listing_id')->references('id')->on('listings')->onDelete('cascade');
        });
        
        Schema::table('dishes', function(Blueprint $table){
            // placeholder for actual Eloquent timestamps
            DB::connection()->getPdo()->exec('ALTER TABLE `dishes` CHANGE `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
            DB::connection()->getPdo()->exec('ALTER TABLE `dishes` CHANGE `updated_at` `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
        });

        // Schema::create('dishes', function(Blueprint $table){
        //     $table->increments('id');
        //     $table->text('name');
        //     $table->text('description')->nullable();
        //     $table->integer('chef_id')->unsigned();
        //     $table->integer('dish_base_id')->unsigned();
            
        //     $table->foreign('chef_id')->references('id')->on('user_id');
        //     $table->foreign('dish_base_id')->references('id')->on('dish_bases');
        // });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // if (Schema::hasTable('dishes')){
        //     Schema::table('dishes', function ($table) {
        //         $table->dropForeign('dishes_dish_id_foreign');
        //         $table->dropForeign('dishes_category_id_foreign');
        //     });
        //     Schema::drop('dishes');    
        // }
        
         Schema::dropIfExists('dishes');       
    }
}
