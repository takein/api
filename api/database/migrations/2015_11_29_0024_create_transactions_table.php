<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function(Blueprint $table){
           $table->increments('id');
           $table->integer('seller_id')->unsigned();
           $table->integer('buyer_id')->unsigned();
           $table->decimal('subtotal_cost', 5, 2);                      // What the dish wouldve cost
           $table->decimal('delivery_cost', 5, 2)->default(0);      // delivery-specific cost overhead
           $table->decimal('surcharge', 5, 2)->default(0);        // Surcharge by seller
           $table->decimal('fees', 5, 2)->default(0);        // Fees to Take in
           $table->timestamps();
           
           // both sellerID and buyerID are users
           $table->foreign('seller_id')->references('id')->on('users');
           $table->foreign('buyer_id')->references('id')->on('users');
           
        });

        Schema::table('transactions', function(Blueprint $table){
            // placeholder for actual Eloquent timestamps
            DB::connection()->getPdo()->exec('ALTER TABLE `transactions` CHANGE `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
            DB::connection()->getPdo()->exec('ALTER TABLE `transactions` CHANGE `updated_at` `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('transactions')){
            Schema::table('transactions', function ($table) {
                $table->dropForeign('transactions_seller_id_foreign');
                $table->dropForeign('transactions_buyer_id_foreign');
            });
            Schema::drop('transactions');    
        }
    }
}
