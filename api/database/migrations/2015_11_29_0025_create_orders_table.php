<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function(Blueprint $table){
            $table->integer('tx_id')->unsigned();
            $table->integer('listing_id')->unsigned();
            $table->string('approval_status', 1)->nullable();       // either A=Accepted, R=Rejected, P=Pending
            $table->integer('servings_purchased')->unsigned();
            $table->dateTime('preferred_pickup_time')->nullable();
            $table->dateTime('preferred_delivery_time')->nullable();
            $table->string('delivery_address')->nullable();
            $table->string('pickup_delivery', 2)->default('P');        // Convention: P=Pickup D=Delivery
            $table->string('notes')->nullable();
            $table->integer('reverse_tx_id')->unsigned()->nullable();       // Link to a tx that reverses this order, if any
            $table->timestamps();
            $table->softDeletes();

            // References transaction IDs (money)
            $table->primary('tx_id');
            $table->foreign('tx_id')->references('id')->on('transactions')->onDelete('cascade');
            $table->foreign('reverse_tx_id')->references('id')->on('transactions')->onDelete('cascade');

            // dishID obviously comes from dishes
            $table->foreign('listing_id')->references('id')->on('listings')->onDelete('cascade');
        });
                
        Schema::table('transactions', function(Blueprint $table){
            // placeholder for actual Eloquent timestamps
            DB::connection()->getPdo()->exec('ALTER TABLE `orders` CHANGE `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
            DB::connection()->getPdo()->exec('ALTER TABLE `orders` CHANGE `updated_at` `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('orders')){
            Schema::table('orders', function ($table) {
                $table->dropForeign('orders_tx_id_foreign');
                $table->dropForeign('orders_listing_id_foreign');
            });
            Schema::drop('orders');    
        }
    }
}
