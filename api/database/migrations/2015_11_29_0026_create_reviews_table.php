<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function(Blueprint $table){
           $table->increments('id');
           $table->integer('order_id')->unsigned();
           $table->integer('value_score');
           $table->integer('taste_score');
           $table->integer('service_score');
           $table->text('comment');
           $table->timestamps();
           $table->softDeletes();
           
           // References orders, NOT transactions - because a review must know which listing_id
           // and looking purely at a tx cannot tell you that info
           $table->foreign('order_id')->references('tx_id')->on('orders');
        });
        
        Schema::table('reviews', function(Blueprint $table){
            // placeholder for actual Eloquent timestamps
            DB::connection()->getPdo()->exec('ALTER TABLE `reviews` CHANGE `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
            DB::connection()->getPdo()->exec('ALTER TABLE `reviews` CHANGE `updated_at` `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('reviews')){
            Schema::table('reviews', function ($table) {
                $table->dropForeign('reviews_order_id_foreign');
            });
            Schema::drop('reviews');    
        }
    }
}
