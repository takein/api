<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_images', function(Blueprint $table){
            $table->increments('id');
            $table->binary('image_blob');
            $table->string('extension')->nullable();
            $table->string('mime_type')->nullable();
        });
        
        Schema::table('temp_images', function(Blueprint $table){
            DB::connection()->getPdo()->exec('ALTER TABLE `temp_images` CHANGE `image_blob` `image_blob` MEDIUMBLOB NOT NULL');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('temp_images');
    }
}
