<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChefAverageRatingStoredProc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // To calculate chefs' average ratings based off ALL their reviews
        DB::unprepared(<<<'STORED_PROC_DEF_CHEF'
CREATE PROCEDURE `CalculateChefAverageRating`(IN `ChefID` INT)
SELECT 		AVG(value_score) avg_value_score, 
			AVG(taste_score) avg_taste_score,
            AVG(service_score) avg_service_score
FROM 		reviews r 
JOIN		transactions t
ON			r.order_id = t.id
WHERE		t.seller_id = ChefID
STORED_PROC_DEF_CHEF
        );
        
        // TO calculate a dish's average ratings based off all its reviews
        DB::unprepared(<<<'STORED_PROC_DEF_DISH'
CREATE PROCEDURE `CalculateDishAverageRating`(IN `DishID` INT)
SELECT 		AVG(value_score) avg_value_score, 
			AVG(taste_score) avg_taste_score,
            AVG(service_score) avg_service_score
FROM 		reviews r 
JOIN		orders o
ON			r.order_id = o.tx_id
JOIN		transactions t
ON			r.order_id = t.id
WHERE		o.listing_id = DishID
STORED_PROC_DEF_DISH
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS `CalculateChefAverageRating`');
        DB::unprepared('DROP PROCEDURE IF EXISTS `CalculateDishAverageRating`');
    }
}
