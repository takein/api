<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemApiTokenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_api_tokens', function ($table) {
            $table->increments('id');
            $table->string('user_id');          // No need to create FK constraint.. too much trouble
            $table->string('token');
            $table->string('ip_address');
            $table->string('user_agent');
            $table->dateTime('expires_at');
            $table->smallInteger('forced_expire')->default(0);
            $table->dateTime('last_activity_at');
            $table->timestamps();
        });
                         
        Schema::table('system_api_tokens', function(Blueprint $table){
            // placeholder for actual Eloquent timestamps
            DB::connection()->getPdo()->exec('ALTER TABLE `system_api_tokens` CHANGE `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
            DB::connection()->getPdo()->exec('ALTER TABLE `system_api_tokens` CHANGE `updated_at` `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
        });
        
        Schema::create('system_temp_tokens', function ($table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();           // No need to create FK constraint though
            $table->string('long_code');             // an auth token
            $table->string('manual_code', 20);   // small digit code they can type in
            $table->smallInteger('require_user_inactive')->default(1);     // user active/inactive state must match this flag
            $table->dateTime('expires_at');
            $table->integer('retries')->default(0);           // How many times it's been re-used
            $table->string('purpose')->default('Unspecified');          // short desc code of what it's for
            $table->timestamps();
            $table->softDeletes();        // When was this used
        });
        
        Schema::table('system_temp_tokens', function(Blueprint $table){
            // placeholder for actual Eloquent timestamps
            DB::connection()->getPdo()->exec('ALTER TABLE `system_temp_tokens` CHANGE `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
            DB::connection()->getPdo()->exec('ALTER TABLE `system_temp_tokens` CHANGE `updated_at` `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('system_api_tokens');
        Schema::drop('system_temp_tokens');
    }
}
