<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(ReferenceDataSeeder::class);
        $this->call(UserDataSeeder::class);
        $this->call(DishDataSeeder::class);
        $this->call(TransactionDataSeeder::class);
        $this->call(ReviewDataSeeder::class);
        $this->call(TokenSeeder::class);

        Model::reguard();
    }
}
