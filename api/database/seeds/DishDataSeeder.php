<?php

use Illuminate\Database\Seeder;

class DishDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            $this->createListings();        
            $this->createDishes();
        });
    }
    
    private function truncateEverything()
    {
        DB::table('listings')->truncate();
        DB::table('dishes')->truncate();
    }
        
    private function deleteEverything()
    {
        DB::table('dishes')->delete();
        DB::table('listings')->delete();
    }
    
    private function createListings()
    {
        DB::table('listings')->insert([
            "id" => 1,
            "owner_id" => 10,
            "name" => "Tacos",
            "description" => "I Live to Cook!",
            "created_at" => "2015-12-15 10:23:11",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        
        DB::table('listings')->insert([
            "id" => 2,
            "owner_id" => 11,
            "name" => "Fried Rice",
            "description" => "I need a dollar!",
            "created_at" => "2015-12-25 12:44:11",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        
        DB::table('listings')->insert([
            "id" => 3,
            "owner_id" => 14,
            "name" => "Curry Chicken",
            "description" => "Aspiring chef!",
            "created_at" => "2015-12-25 22:18:11",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]); 
        
        DB::table('listings')->insert([
            "id" => 4,
            "owner_id" => 10,
            "name" => "Delicious Gourmet Burger",
            "description" => "A home-ground beef patty with pickles, chunky tomato sauce and mustard",
            "created_at" => "2015-12-18 15:08:13",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        
        DB::table('listings')->insert([
            "id" => 5,
            "owner_id" => 14,
            "name" => "Chicken Rice",
            "description" => "Hainanese style chicken breast served with fragrant rice and soup",
            "created_at" => "2016-02-01 04:39:02",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        
        DB::table('listings')->insert([
            "id" => 6,
            "owner_id" => 14,
            "name" => "Hot doges",
            "description" => "A tangy sausage",
            "created_at" => "2016-01-19 12:08:13",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
    }
    
    private function createDishes()
    {
        DB::table('dishes')->insert([
            "listing_id" => "1",
            "price_per_serving" => "3.50",
            "servings_total" => 50,
            "servings_remaining" => 50,
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        
        DB::table('dishes')->insert([
            "listing_id" => "2",
            "price_per_serving" => "4.99",
            "servings_total" => 10,
            "servings_remaining" => 10,
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);        
        
        DB::table('dishes')->insert([
            "listing_id" => "3",
            "servings_total" => 5,
            "servings_remaining" => 5,
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]); 
        
        DB::table('dishes')->insert([
            "listing_id" => "4",
            "price_per_serving" => "8.50",
            "servings_total" => 30,
            "servings_remaining" => 30,
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        
        DB::table('dishes')->insert([
            "listing_id" => "5",
            "price_per_serving" => "9.55",
            "servings_total" => 5,
            "servings_remaining" => 5,
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        
        DB::table('dishes')->insert([
            "listing_id" => "6",
            "price_per_serving" => "13.15",
            "servings_total" => 4,
            "servings_remaining" => 4,
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
    }

}
