<?php

use Illuminate\Database\Seeder;

class ReferenceDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {

            DB::table('categories')->delete();
            DB::table('category_types')->delete();
            
        });
        DB::transaction(function () {
            
            DB::table('category_types')->insert([
                ['type' => 'CU', 'description' => 'Cuisine'],
                ['type' => 'DR', 'description' => 'Dietary Requirements'],
                ['type' => 'AL', 'description' => 'Allergy']
            ]);
            
            DB::table('categories')->insert([
                ['type' => 'CU', 'name' => 'Asian'],
                ['type' => 'CU', 'name' => 'Indian'],
                ['type' => 'CU', 'name' => 'Western'],
                ['type' => 'DR', 'name' => 'Gluten free'],
                ['type' => 'DR', 'name' => 'Dairy free'],
                ['type' => 'DR', 'name' => 'Kosher'],
                ['type' => 'DR', 'name' => 'Nut free'],
                ['type' => 'DR', 'name' => 'Paleo'],
                ['type' => 'DR', 'name' => 'Vegan'],
                ['type' => 'DR', 'name' => 'Vegetarian'],
                ['type' => 'DR', 'name' => 'Halal'],
                ['type' => 'AL', 'name' => 'Shellfish'],
                ['type' => 'AL', 'name' => 'Soy'],       
                ['type' => 'AL', 'name' => 'Peanuts'],       
                ['type' => 'AL', 'name' => 'Tree nuts'],       
                ['type' => 'AL', 'name' => 'Gluten'],       
                ['type' => 'AL', 'name' => 'Egg'],       
                ['type' => 'AL', 'name' => 'Wheat']       
            ]);
            
        });
    }
}
