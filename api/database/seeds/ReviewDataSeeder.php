<?php

use Illuminate\Database\Seeder;

class ReviewDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            $this->createReviews();        
        });
    }
    
    private function createReviews()
    {
        DB::table('reviews')->insert([
            "order_id" => 1,
            "value_score" => 5,
            "taste_score" => 5,
            "service_score" => 3,
            "comment" => 'Absolutely fantastic!!',
            "created_at" => "2015-12-16 06:08:13",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        
        DB::table('reviews')->insert([
            "order_id" => 2,
            "value_score" => 4,
            "taste_score" => 4,
            "service_score" => 3,
            "comment" => 'Great',
            "created_at" => "2015-12-15 23:08:13",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        
        DB::table('reviews')->insert([
            "order_id" => 3,
            "value_score" => 5,
            "taste_score" => 5,
            "service_score" => 4,
            "comment" => 'Wow I like',
            "created_at" => "2015-12-14 22:08:13",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        DB::table('reviews')->insert([
            "order_id" => 3,
            "value_score" => 5,
            "taste_score" => 5,
            "service_score" => 4,
            "comment" => 'v v nice would eat again',
            "created_at" => "2015-12-13 09:08:13",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        
        DB::table('reviews')->insert([
            "order_id" => 5,
            "value_score" => 5,
            "taste_score" => 5,
            "service_score" => 4,
            "comment" => 'ten out of ten',
            "created_at" => "2015-12-12 17:08:13",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        
        DB::table('reviews')->insert([
            "order_id" => 6,
            "value_score" => 5,
            "taste_score" => 3,
            "service_score" => 4,
            "comment" => 'v v nice would eat again',
            "created_at" => "2015-12-11 12:08:13",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
    }
}
