<?php

use Illuminate\Database\Seeder;

class TokenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            $this->createSystemApiTokens();        
        });
    }
    
    private function createSystemApiTokens()
    {
        DB::table('system_api_tokens')->insert([
            "user_id" => 14,
            "token" => '$2y$10$qHJBvx5YrV3mFebKEqWfpexZjaiYLKSWRzWTv6qDIt700lchRM8j.',
            "ip_address" => "TEST_ONLY",
            "user_agent" => "TEST_ONLY",
            "expires_at" => "2050-01-01 00:00:00"
        ]);
        
        // For our testing convenience
        DB::table('system_api_tokens')->insert([
            "user_id" => 14,
            "token" => '1',
            "ip_address" => "TEST_ONLY",
            "user_agent" => "TEST_ONLY",
            "expires_at" => "2050-01-01 00:00:00"
        ]);
        
        // For jasmine/frisby test runner
        DB::table('system_api_tokens')->insert([
            "user_id" => 10,
            "token" => '10',
            "ip_address" => "AUTOMATED_TEST_ONLY",
            "user_agent" => "AUTOMATED_TEST_ONLY",
            "expires_at" => "2050-01-01 00:00:00"
        ]);
        
        // For jasmine/frisby test runner
        DB::table('system_api_tokens')->insert([
            "user_id" => 12,
            "token" => '12',
            "ip_address" => "AUTOMATED_TEST_ONLY",
            "user_agent" => "AUTOMATED_TEST_ONLY",
            "expires_at" => "2050-01-01 00:00:00"
        ]);
        
    }   
}
