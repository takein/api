<?php

use Illuminate\Database\Seeder;

class TransactionDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            $this->createTransactions();        
            $this->createOrders();                         
        });
    }
    
    private function createTransactions()
    {
        DB::table('transactions')->insert([     // 1
            "id" => 1,
            "seller_id" => 10,
            "buyer_id" => 12,
            "subtotal_cost" => 13.50,
            "delivery_cost" => 0.0,
            "surcharge" => 0.0,
            "fees" => 0.50,
            "created_at" => "2015-12-18 15:08:13",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        DB::table('transactions')->insert([     // 2
            "id" => 2,
            "seller_id" => 10,
            "buyer_id" => 11,
            "subtotal_cost" => 9.00,
            "delivery_cost" => 0.0,
            "surcharge" => 0.0,
            "fees" => 0.50,
            "created_at" => "2015-12-18 15:08:13",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        DB::table('transactions')->insert([     // 3
            "id" => 3,
            "seller_id" => 11,
            "buyer_id" => 12,
            "subtotal_cost" => 7.55,
            "delivery_cost" => 0.0,
            "surcharge" => 0.0,
            "fees" => 0.25,
            "created_at" => "2015-12-18 15:08:13",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        DB::table('transactions')->insert([     // 4
            "id" => 4,
            "seller_id" => 11,
            "buyer_id" => 10,
            "subtotal_cost" => 29.99,
            "delivery_cost" => 0.0,
            "surcharge" => 0.0,
            "fees" => 0.50,
            "created_at" => "2015-12-18 15:08:13",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        DB::table('transactions')->insert([     // 5
            "id" => 5,
            "seller_id" => 14,
            "buyer_id" => 10,
            "subtotal_cost" => 10.00,
            "delivery_cost" => 0.0,
            "surcharge" => 0.0,
            "fees" => 0.20,
            "created_at" => "2015-12-18 15:08:13",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        
        DB::table('transactions')->insert([     // 6
            "id" => 6,
            "seller_id" => 10,
            "buyer_id" => 12,
            "subtotal_cost" => 5.50,
            "delivery_cost" => 0.0,
            "surcharge" => 0.0,
            "fees" => 0.50,
            "created_at" => "2015-12-18 15:08:13",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        
        DB::table('transactions')->insert([     // 6
            "id" => 7,
            "seller_id" => 10,
            "buyer_id" => 12,
            "subtotal_cost" => 5.50,
            "delivery_cost" => 0.0,
            "surcharge" => 0.0,
            "fees" => 0.50,
            "created_at" => "2015-12-18 15:08:13",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
    }
    
    private function createOrders()
    {
        DB::table('orders')->insert([     // 1
            "tx_id" => 1,
            "listing_id" => 1,
            "servings_purchased" => 1,
            "preferred_pickup_time" => '2016-02-15 00:00:00',
            "pickup_delivery" => 'P',
            "notes" => 'No topping please',
            "created_at" => "2015-12-18 15:08:13",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        DB::table('orders')->insert([     // 2
            "tx_id" => 2,
            "listing_id" => 1,
            "servings_purchased" => 2,
            "pickup_delivery" => 'P',
            "preferred_pickup_time" => '2016-02-15 00:00:00',
            "notes" => '',
            "created_at" => "2015-12-18 15:08:13",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        
        DB::table('orders')->insert([     // 3
            "tx_id" => 3,
            "listing_id" => 2,
            "servings_purchased" => 1,
            "pickup_delivery" => 'P',
            "preferred_pickup_time" => '2016-02-15 00:00:00',
            "notes" => 'Not spicy please',
            "created_at" => "2015-12-18 15:08:13",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        DB::table('orders')->insert([     // 4
            "tx_id" => 4,
            "listing_id" => 2,
            "servings_purchased" => 1,
            "pickup_delivery" => 'P',
            "preferred_pickup_time" => '2016-02-15 00:00:00',
            "notes" => '',
            "created_at" => "2015-12-18 15:08:13",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        
        DB::table('orders')->insert([     // 5
            "tx_id" => 5,
            "listing_id" => 6,
            "servings_purchased" => 1,
            "pickup_delivery" => 'P',
            "preferred_pickup_time" => '2016-02-15 00:00:00',
            "notes" => 'May be 5-10 mins late',
            "created_at" => "2015-12-18 15:08:13",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        
        DB::table('orders')->insert([     // 6
            "tx_id" => 6,
            "listing_id" => 1,
            "servings_purchased" => 1,
            "pickup_delivery" => 'P',
            "preferred_pickup_time" => '2016-02-15 00:00:00',
            "notes" => '',
            "created_at" => "2015-12-18 15:08:13",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        
        DB::table('orders')->insert([     // 6
            "tx_id" => 7,
            "listing_id" => 1,
            "servings_purchased" => 1,
            "pickup_delivery" => 'P',
            "preferred_pickup_time" => '2016-02-15 00:00:00',
            "notes" => 'Your tacos had better b good',
            "created_at" => "2015-12-18 15:08:13",
            "updated_at" => date(DATETIME_FORMAT_MYSQL),
        ]);
        
    }
      
    private function truncateEverything()
    {
        DB::table('transactions')->truncate();
        DB::table('order')->truncate();
    }
        
    private function deleteEverything()
    {
        DB::table('order')->delete();
        DB::table('transactions')->delete();
    }  
}
