<?php

use Illuminate\Database\Seeder;

class TruncationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateEverything();
    }
    
    private function truncateEverything()
    {
        DB::select(DB::raw('SET FOREIGN_KEY_CHECKS = 0;'));
        DB::table('reviews')->truncate();
        DB::table('orders')->truncate();
        DB::table('transactions')->truncate();
        DB::table('chefs')->truncate();
        DB::table('users')->truncate();
        DB::table('dishes')->truncate();
        DB::table('listings_categories')->truncate();
        DB::table('system_api_tokens')->truncate();
        DB::table('system_temp_tokens')->truncate();
        DB::table('listings_images')->truncate();
        DB::table('listings')->truncate();
        DB::table('categories')->truncate();
        DB::table('category_types')->truncate();
        DB::table('temp_images')->truncate();
    }

        
}
