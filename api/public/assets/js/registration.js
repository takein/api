var tia = {
    data: {},
    config: {
       signupEndpoint : '/api/v1/user/signup',
       handshakeEndpoint: 'api/v1/user/handshake',
       activationEndpoint : '/api/v1/user/activate',
       forgotEndpoint : '/api/v1/auth/forgot',
       resendEndpoint : '/api/v1/auth/resendcode',
       loginEndpoint : '/api/v1/login',
       meEndpoint: '/api/v1/user/me',
       resetEndpoint: function(id){
           return '/api/v1/user/' + id;
       }
    }
};

$('.form-container').find('select').on('change', flyingSelectLabels);
$('.form-container').find('input, textarea').on('keyup blur focus change', flyingLabels);

$('.tab a').on('click', function(e) {

    e.preventDefault();

    $(this).parent('li').addClass('active');
    $(this).parent('li').siblings().removeClass('active');

    target = $(this).attr('href');
    toggleTarget(target);
    
});

// ===============
//  Registration
// ===============
$('#form-register').submit(function(e){
    
    e.preventDefault();

    var $form = $(this);
    var originalButtonText = $form.find('.button').text();
    $form.find('.button').attr('disabled', true).text('Submitting...');
    
    // Preliminarily bind; these may change
    tia.data.username = $form.find('[name=username]').val().trim();
    tia.data.firstName = $form.find('[name=first_name]').val().trim();
    tia.data.lastName = $form.find('[name=last_name]').val().trim();
    
    var payload = {
        user: {
            username: $form.find('[name=username]').val().trim(),
            first_name: $form.find('[name=first_name]').val().trim(),
            last_name: $form.find('[name=last_name]').val().trim(),
            mobile: $form.find('[name=mobile]').val().trim(),
            email: $form.find('[name=email]').val().trim(),
            postcode: $form.find('[name=postcode]').val().trim(),
            state: $form.find('[name=state]').val().trim()
        }
    };
    
    $.ajax(tia.config.signupEndpoint, {
        data: payload, 
        method: 'POST'
    }).done(function(response){
        
        runPreActivationChecks();
        toggleTarget($('#activate'));
        
    }).fail(function(xhr){

        if (xhr.responseJSON.error.code === 4115){
            alert('That email has already been used, have you already signed up before? If so, you can recover your password.');
        }
        else if (xhr.responseJSON.error.code === 4110){
            alert('That username has already been used, have you already signed up before? If so, you can recover your password.');
        }
        else if (xhr.responseJSON.error.code === 4116){
            alert('Your email address appears to be in the wrong format, please check');
        }
        else {
            alert('Ooops... we couldn\'t submit your registration! Are you connected to the internet? If so, it may be something on our end. Sorry.');
        }
        
    }).always(function(){
       
        $form.find('.button').attr('disabled', false).text(originalButtonText);
        
    });
    
    return false;
});

// ===============
//  Activation
// ===============
$('#form-activate').submit(function(e){
    
    e.preventDefault();
    
    var $form = $(this);
    var originalButtonText = $form.find('.button').text();
    $form.find('.button').attr('disabled', true).text('Submitting...');
    
    // Username can be used for later
    tia.data.username = $form.find('[name=username]').val().trim();
    
    var payload = {
        username: $form.find('[name=username]').val().trim(),
        activation_code: $form.find('[name=activation_code]').val().trim()
    };
    
    $.ajax(tia.config.activationEndpoint, {
        data: payload, 
        method: 'POST'
    }).done(function(response){
        
        if (response.temp_password){
            toggleTarget($('#reset'));
        
            // IMPORTANT: SAVE THE TEMP PASSWORD
            tia.data.tempPassword = response.temp_password;
            $('#temp-password').text(tia.data.tempPassword);
        }
        else {
            $form.parent().find('h1').addClass('error-text').text('Sorry, that\'s not quite right');    // sneaky hint
        }
        
    }).fail(function(xhr){
        
        $form.parent().find('h1').addClass('error-text').text('Sorry, that\'s not the right code');        
        
    }).always(function(){
        
        $form.find('.button').attr('disabled', false).text(originalButtonText);

    });
    
    return false;
});

// ===============
//  Password reset
// ===============
$('#form-reset').submit(function(e){
    
    e.preventDefault();
    
    // Maybe this is a bit convoluted but...
    // 1. Enact a login using the temp password, retrieve the API token
    // 2. Use this API token to call a /me
    // 3. Use /me data to issue a user password reset request
    // 4. ???
    // 5. Profit
    
    var $form = $(this);
    
    // Check both passwords match
    var pass1 = $form.find('[name=password]').val();
    var pass2 = $form.find('[name=password2]').val();
    if (pass1 !== pass2){
        $form.parent().find('h1').addClass('error-text').text('Both passwords need to match');    // sneaky hint
        return false;
    }
    
    var originalButtonText = $form.find('.button').text();
    $form.find('.button').attr('disabled', true).text('Submitting...');

    new Promise(function(resolve, reject){
        // Part 1a. Resolve the username
        $.ajax(tia.config.handshakeEndpoint, {
            method: 'GET',
            data: { 
                name: tia.data.username
            }
        }).done(function(response){
           
            if (response.username){
                tia.data.username = response.username;
                resolve(response.username);
            }
           
        }).fail(function(){
            
            reject('Could not ascertain username');
            
        });

    }).then(function(username){
        // Part 1b. Perform the login to obtain an auth token
        return new Promise(function(resolve, reject){
            $.ajax(tia.config.loginEndpoint, {
                method: 'POST',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader ("Authorization", "Basic " + btoa(tia.data.username + ":" + tia.data.tempPassword));
                }
             }).done(function(response){
                 
                 if (response.api_token){
                    tia.data.apiToken = response.api_token;
                    return resolve(response.api_token);
                }
                reject('No API token returned from login');
                 
             }).fail(function(xhr){
                 
                    reject('Login not successful');
                 
             });
        });
        
    }).then(function(apiToken){
        
        // Part 2.
        return new Promise(function(resolve, reject){
            $.ajax(tia.config.meEndpoint, {
                method: 'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader ("X-API-TOKEN", apiToken);
                }
             }).done(function(response){
                 
                 if (response.user){
                    tia.data.firstName = response.user.first_name;
                    tia.data.lastName = response.user.last_name;
                    tia.data.userId = response.user.id;
                    return resolve(apiToken);
                }
                reject('Failed to retrieve this users details');
                 
             }).fail(function(xhr){
                 
                 reject('Failed to retrieve this users details');

             });
        });
        
    }).then(function(apiToken){
            
        // Part 3
        return new Promise(function(resolve, reject){
                
            var payload = {
                password: pass1     // just choose either of the matched passwords
            };
            
            $.ajax(tia.config.resetEndpoint(tia.data.userId), {
                method: 'PATCH',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader ("X-API-TOKEN", apiToken);
                },
                data: payload
             }).done(function(response){
                    
                if (response.success){
                    resolve(response.success.id);
                    return;
                }
                reject('Did not set password successfully');
                    
             }).fail(function(xhr){
                 
                if (xhr.responseJSON.error && xhr.responseJSON.error.code === 2220){
                    alert('Your password doesn\'t meet the requirements, please choose a password that includes letters, numbers and symbols.'); 
                }
                reject('Did not set password successfully');
                 
             });
                
         });
         
    }).then(function(userId){
                
        // Set their names on the target
        $('#done h1').text('Thanks' + ( tia.data.firstName ? (' ' + tia.data.firstName) : '' ) + '!');
        toggleTarget($('#done'));
        return userId;
        
    }).catch(function(reason){
	
        console.warn(reason);
        $form.find('.button').attr('disabled', false).text(originalButtonText);
        
    });
    
    return false;
});


// ===============
//  Forgot
// ===============
$('#form-forgot').submit(function(e){
    
    e.preventDefault();
    
    var $form = $(this);
    var originalButtonText = $form.find('.button').text();
    $form.find('.button').attr('disabled', true).text('Submitting...');
    
    // Username can be used for later
    tia.data.username = $form.find('[name=username]').val().trim();
    
    var payload = {
        username: $form.find('[name=username]').val().trim(),
        mobile: $form.find('[name=mobile]').val().trim()
    };
    
    $.ajax(tia.config.forgotEndpoint, {
        data: payload, 
        method: 'POST'
    }).done(function(response){
        
        if (response.sms_sent || response.email_sent){
            
            runPreActivationChecks();
            toggleTarget($('#activate'));
            
        }
        else {
            
            $form.parent().find('h1').addClass('error-text').text('Sorry, those details don\'t match');    // sneaky hint
            
        }
        
    }).fail(function(xhr){
        
        $form.parent().find('h1').addClass('error-text').text('Sorry, those details weren\'t found');  
        
    }).always(function(){

        $form.find('.button').attr('disabled', false).text(originalButtonText);
        
    });
    
    return false;
});

// ====================
// Resend code
// ====================
$('#resend-code').click(function(e){
    e.preventDefault();
    
    // No point in completing if username not known
    var attemptUsername;
    if (tia.data.username){
        attemptUsername = tia.data.username;
    }
    else {
        var $usernameField = $(this).closest('form').find('[name=username]');
        if ($.trim($usernameField.val())){
            attemptUsername = $.trim($usernameField.val());
        }
        else {
            alert("Please fill in the 'Username or Email Address' field, then try again.");
            $usernameField.select();
            return;
        }
    }

    var $submitBtn = $(this).closest('form').find('[type=submit]');
    var originalButtonText = $submitBtn.text();
    $submitBtn.attr('disabled', true).text('Resending...');
    
    $.ajax(tia.config.resendEndpoint, {
        data: { username: attemptUsername }, 
        method: 'POST'
    }).done(function(response){
    
        alert('Done: Your SMS code and validation email have been sent again');
        
    }).fail(function(xhr){
        
        alert('Hmm... Sorry something didn\'t work, and we weren\'t able to resend the code to you.')
        
    }).always(function(){
        
        $submitBtn.attr('disabled', false).text(originalButtonText);

    });

    return false;
});


// ====================
// Functions
// ====================

function flyingLabels(e) {

    var $this = $(this), label = $this.prev('label');

    if (e.type === 'keyup' || e.type === 'change') {
        if ($this.val() === '') {
            label.removeClass('active highlight');
        } else {
            label.addClass('active highlight');
        }
    } else if (e.type === 'blur') {
        if ($this.val() === '') {
            label.removeClass('active highlight');
        } else {
            label.removeClass('highlight');
        }
    } else if (e.type === 'focus') {
        if ($this.val() === '') {
            label.removeClass('highlight');
        } else if ($this.val() !== '') {
            label.addClass('highlight');
        }
    }

};

function flyingSelectLabels(e) {

    var $this = $(this), label = $this.prev('label');

    if (e.type === 'change') {
        if ($this.val() === '') {
            label.removeClass('active highlight');
        } else {
            label.addClass('active highlight');
        }
    }

};

function toggleTarget(target){
    $('.tab-content > div').not(target).hide();
    $(target).fadeIn(600);
};

function runPreActivationChecks(){
    if (tia.data.username){     
        // Username was prefilled probably from subscription form
        $usernameField = $('#form-activate [name=username]');
        $usernameField.val(tia.data.username);
        $usernameField.closest('.field-wrap').hide();
    }
}
