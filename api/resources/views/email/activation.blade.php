@extends('email.templates.confirmation')

@section('title', 'Activation your account')

@section('templateBody')
<!-- // Begin Template Body \\ -->
<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">
    <tr>
        <td valign="top">

            <!-- // Begin Module: Standard Content \\ -->
            <table border="0" cellpadding="20" cellspacing="0" width="100%">
                <tr>
                    <td valign="top" class="bodyContent">
                        <div mc:edit="std_content00">
                            <br />
                            <h1 class="h1" style="">Activate your account</h1>
                            <br />
                            <h3 class="h3">Are you ready to see what's cooking?</h3>
                            <br />
                            <strong>Hello {{ $greetingName or 'there' }},</strong>
                            <br />
                            <br />
                            You're a <em>few steps away</em> from launching Take in. Just click on the Activate button below, set your own password, then get started!
                            <br />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="padding-top:0;">
                        <table border="0" cellpadding="15" cellspacing="0" class="templateButton">
                            <tr>
                                <td valign="middle" class="templateButtonContent">
                                    <div mc:edit="std_content01">
                                        <a href="{{ $activationLink or 'xxxxxxx' }}" target="_blank">Activate my account</a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" class="footerContent">
                        <div mc:edit="std_content00">
                            Alternatively, copy and paste this URL into your browser: 
                            <br />
                            <code>{{ $activationLink or 'xxxxxxx' }}</code>
                        </div>
                    </td>
                </tr>
            </table>
            <!-- // End Module: Standard Content \\ -->
            
        </td>
    </tr>
</table>
<!-- // End Template Body \\ -->
@endsection