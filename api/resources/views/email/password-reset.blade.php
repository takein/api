@extends('email.templates.confirmation')

@section('title', 'Password reset')

@section('templateBody')
<!-- // Begin Template Body \\ -->
<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">
    <tr>
        <td valign="top">

            <!-- // Begin Module: Standard Content \\ -->
            <table border="0" cellpadding="20" cellspacing="0" width="100%">
                <tr>
                    <td valign="top" class="bodyContent">
                        <div mc:edit="std_content00">
                            <br />
                            <h1 class="h1">Here's your password reset</h1>
                            <br />
                            <strong>Hello {{ $greetingName or 'there' }},</strong>
                            <br />
                            <br />
                            Did you recently request a password reset? If so, you can do so by clicking the button below. If not, you can ignore this email.
                            <br />
                            <br />
                            If you didn't request a password reset but you've received this email a few times, someone may be trying to hack your account. If so, <a href="mailto:{{ $contactUsEmail or 'admin@take-in.com.au' }}" target="_blank">please contact us</a>.
                            <br />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="padding-top:0;">
                        <table border="0" cellpadding="15" cellspacing="0" class="templateButton">
                            <tr>
                                <td valign="middle" class="templateButtonContent">
                                    <div mc:edit="std_content01">
                                        <a href="{{ $resetLink or 'xxxxxxx' }}" target="_blank">Reset my password</a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" class="footerContent">
                        <div mc:edit="std_content00">
                            Alternatively, copy and paste this URL into your browser: 
                            <br />
                            <code>{{ $resetLink or 'xxxxxxx' }}</code>
                        </div>
                    </td>
                </tr>
            </table>
            <!-- // End Module: Standard Content \\ -->
            
        </td>
    </tr>
</table>
<!-- // End Template Body \\ -->
@endsection