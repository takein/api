<!DOCTYPE html>
<html>
<head>
	<title><?php echo strtoupper($env); ?> | API v1: Take In</title>
	<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
	<style>
		body {
			background: #222;
            color: whitesmoke;
            text-align: center;
            font-family: monospace;
        }
	</style>
</head>
<body>
    <pre>
                          
 /$$$$$$$$        /$$                       /$$                  /$$$$$$  /$$$$$$$  /$$$$$$
|__  $$__/       | $$                      |__/                 /$$__  $$| $$__  $$|_  $$_/
   | $$  /$$$$$$ | $$   /$$  /$$$$$$        /$$ /$$$$$$$       | $$  \ $$| $$  \ $$  | $$  
   | $$ |____  $$| $$  /$$/ /$$__  $$      | $$| $$__  $$      | $$$$$$$$| $$$$$$$/  | $$  
   | $$  /$$$$$$$| $$$$$$/ | $$$$$$$$      | $$| $$  \ $$      | $$__  $$| $$____/   | $$  
   | $$ /$$__  $$| $$_  $$ | $$_____/      | $$| $$  | $$      | $$  | $$| $$        | $$  
   | $$|  $$$$$$$| $$ \  $$|  $$$$$$$      | $$| $$  | $$      | $$  | $$| $$       /$$$$$$
   |__/ \_______/|__/  \__/ \_______/      |__/|__/  |__/      |__/  |__/|__/      |______/
                                                                                                                              
                                                                                           
Routes in <?php echo strtoupper($env); ?> environment:

<?php echo $listedRoutes; ?>

********

Last 10 temp tokens for <?php echo strtoupper($env); ?> environment:

<?php echo $lastTokens; ?>

********
    </pre>	
</body>
</html>