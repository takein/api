<?php

use App\Providers\SmsServiceProvider;
use App\Jobs\SendActivation;
use App\Jobs\SendPasswordReset;
use App\Models\SystemTempToken;
//use Laravel\Lumen\Routing\DispatchesJobs;     // was Lumen 5.1

class TransactionalMessageTest extends TestCase
{
    
    public function testSendsEmail()
    {
        $app = $this->createApplication();
        
        $tt = new SystemTempToken();
        $tt->user_id = 14;      // should lookup the Alan test user
        $tt->activation_code = '555555';
        $tt->token = 'abcdef54321';
        
        // $mailTest = app('mailer');
        // dd($mailTest);
        
        // dispatch(new SendPasswordReset($tt));
        
        $mailer = app('mailer');
        
        $mailMerge = [
            'greetingName' => $tt->user->first_name,
            'resetLink' => 'https://aaaaaaaaaaaaaa.com/activate?cred=' . base64_encode($tt->token),
        ];

        $mailer->send('emails.password-reset', $mailMerge, function ($m) use ($tt) {
            // NB: "From" address should automatically be set from the config file
            
            $m->subject('Take in password reset');
            $m->to($tt->user->email, "{$tt->user->first_name} {$tt->user->last_name}");
            
        });
        
    }
    
    public function testSendsActivation()
    {
        $app = $this->createApplication();
        
        $tt = new SystemTempToken();
        $tt->user_id = 14;      // should lookup the Alan test user
        $tt->manual_code = date('Ymd.His');
        $tt->token = 'abcdef54321';
        
        dispatch(new SendActivation($tt, ['sms' => true, 'email' => true]));
    }
    
    
}
