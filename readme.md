# Welcome to Take in API

You should find documentation at https://my.<env>.take-in.net.au/docs/v1

The self-service portal code, etc has been moved to another repository `takein-public-website/laravel`

Branch: master 
[ ![Codeship Status for takein/api](https://codeship.com/projects/3b3c8c20-b2ec-0133-aa07-02105821d9b0/status?branch=master)](https://codeship.com/projects/133508)


Branch: dev-api  
[ ![Codeship Status for takein/api](https://codeship.com/projects/3b3c8c20-b2ec-0133-aa07-02105821d9b0/status?branch=dev-api)](https://codeship.com/projects/133508)
